﻿namespace UTMC_Report_from_Oracle
{
    partial class UTMC_Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOracleConnection = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.DateSelection = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnAdjustment = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnUTMC18 = new System.Windows.Forms.Button();
            this.btnUTMC16 = new System.Windows.Forms.Button();
            this.btnUTMC17 = new System.Windows.Forms.Button();
            this.btnUTMC12 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.Test = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnUTMC13 = new System.Windows.Forms.Button();
            this.btnUTMC15 = new System.Windows.Forms.Button();
            this.btnUTMC10 = new System.Windows.Forms.Button();
            this.btnUTMC11 = new System.Windows.Forms.Button();
            this.btnUTMC_03 = new System.Windows.Forms.Button();
            this.btnUTMC14 = new System.Windows.Forms.Button();
            this.btn_export = new System.Windows.Forms.Button();
            this.btn_import = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.EndDate = new System.Windows.Forms.DateTimePicker();
            this.StartDate = new System.Windows.Forms.DateTimePicker();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOracleConnection
            // 
            this.btnOracleConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOracleConnection.Location = new System.Drawing.Point(427, 10);
            this.btnOracleConnection.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOracleConnection.Name = "btnOracleConnection";
            this.btnOracleConnection.Size = new System.Drawing.Size(107, 62);
            this.btnOracleConnection.TabIndex = 9;
            this.btnOracleConnection.Text = "DB_Status";
            this.btnOracleConnection.UseVisualStyleBackColor = true;
            this.btnOracleConnection.Click += new System.EventHandler(this.btnOracleConnection_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.checkBox2);
            this.panel1.Controls.Add(this.button10);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.DateSelection);
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.btn_export);
            this.panel1.Controls.Add(this.btn_import);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.EndDate);
            this.panel1.Controls.Add(this.StartDate);
            this.panel1.Controls.Add(this.btnOracleConnection);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(663, 685);
            this.panel1.TabIndex = 10;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(465, 87);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(182, 21);
            this.checkBox2.TabIndex = 54;
            this.checkBox2.Text = "Use for historical report.";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.Red;
            this.button10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button10.Location = new System.Drawing.Point(539, 14);
            this.button10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(109, 62);
            this.button10.TabIndex = 37;
            this.button10.Text = "Refresh DB";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button6.Location = new System.Drawing.Point(235, 519);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(112, 158);
            this.button6.TabIndex = 36;
            this.button6.Text = "STEP 1 - FINAL MEMBER LEVEL IMPORT ";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button4.Location = new System.Drawing.Point(499, 519);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(125, 158);
            this.button4.TabIndex = 31;
            this.button4.Text = "STEP 3 - FINAL EXPORT ";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // DateSelection
            // 
            this.DateSelection.AutoSize = true;
            this.DateSelection.ForeColor = System.Drawing.Color.Red;
            this.DateSelection.Location = new System.Drawing.Point(67, 94);
            this.DateSelection.Name = "DateSelection";
            this.DateSelection.Size = new System.Drawing.Size(0, 17);
            this.DateSelection.TabIndex = 34;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button2);
            this.groupBox5.Controls.Add(this.btnAdjustment);
            this.groupBox5.Location = new System.Drawing.Point(11, 519);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(208, 158);
            this.groupBox5.TabIndex = 30;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Adjustment from Rpt 11 (to cover 0 unit closing)";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(40, 102);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(136, 42);
            this.button2.TabIndex = 25;
            this.button2.Text = "Adjustment 15";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnAdjustment
            // 
            this.btnAdjustment.Location = new System.Drawing.Point(40, 43);
            this.btnAdjustment.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAdjustment.Name = "btnAdjustment";
            this.btnAdjustment.Size = new System.Drawing.Size(136, 42);
            this.btnAdjustment.TabIndex = 24;
            this.btnAdjustment.Text = "Adjustment 3";
            this.btnAdjustment.UseVisualStyleBackColor = true;
            this.btnAdjustment.Click += new System.EventHandler(this.btnAdjustment_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button15);
            this.groupBox1.Controls.Add(this.button14);
            this.groupBox1.Controls.Add(this.button13);
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.Test);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(11, 114);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(636, 389);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Import from UTS (Oracle)";
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(341, 329);
            this.button15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(96, 43);
            this.button15.TabIndex = 40;
            this.button15.Text = "button15";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(340, 286);
            this.button14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(97, 43);
            this.button14.TabIndex = 39;
            this.button14.Text = "btntest0unit";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(237, 329);
            this.button13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(97, 43);
            this.button13.TabIndex = 38;
            this.button13.Text = "btntestCost";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(443, 330);
            this.button9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(136, 42);
            this.button9.TabIndex = 37;
            this.button9.Text = "Adjustment 03 for decease";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(443, 282);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(136, 42);
            this.button8.TabIndex = 36;
            this.button8.Text = "Adjustment 18";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnUTMC18);
            this.groupBox3.Controls.Add(this.btnUTMC16);
            this.groupBox3.Controls.Add(this.btnUTMC17);
            this.groupBox3.Controls.Add(this.btnUTMC12);
            this.groupBox3.Location = new System.Drawing.Point(237, 39);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(183, 241);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Fund Level Report";
            // 
            // btnUTMC18
            // 
            this.btnUTMC18.Location = new System.Drawing.Point(23, 74);
            this.btnUTMC18.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUTMC18.Name = "btnUTMC18";
            this.btnUTMC18.Size = new System.Drawing.Size(136, 43);
            this.btnUTMC18.TabIndex = 12;
            this.btnUTMC18.Text = "UTMC_18";
            this.btnUTMC18.UseVisualStyleBackColor = true;
            this.btnUTMC18.Click += new System.EventHandler(this.btnUTMC18_Click);
            // 
            // btnUTMC16
            // 
            this.btnUTMC16.Location = new System.Drawing.Point(23, 25);
            this.btnUTMC16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUTMC16.Name = "btnUTMC16";
            this.btnUTMC16.Size = new System.Drawing.Size(136, 43);
            this.btnUTMC16.TabIndex = 10;
            this.btnUTMC16.Text = "UTMC_16";
            this.btnUTMC16.UseVisualStyleBackColor = true;
            this.btnUTMC16.Click += new System.EventHandler(this.btnUTMC16_Click);
            // 
            // btnUTMC17
            // 
            this.btnUTMC17.Location = new System.Drawing.Point(23, 172);
            this.btnUTMC17.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUTMC17.Name = "btnUTMC17";
            this.btnUTMC17.Size = new System.Drawing.Size(136, 43);
            this.btnUTMC17.TabIndex = 11;
            this.btnUTMC17.Text = "UTMC_17";
            this.btnUTMC17.UseVisualStyleBackColor = true;
            this.btnUTMC17.Click += new System.EventHandler(this.btnUTMC17_Click);
            // 
            // btnUTMC12
            // 
            this.btnUTMC12.Location = new System.Drawing.Point(23, 123);
            this.btnUTMC12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUTMC12.Name = "btnUTMC12";
            this.btnUTMC12.Size = new System.Drawing.Size(136, 43);
            this.btnUTMC12.TabIndex = 9;
            this.btnUTMC12.Text = "UTMC_12";
            this.btnUTMC12.UseVisualStyleBackColor = true;
            this.btnUTMC12.Click += new System.EventHandler(this.btnUTMC12_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button5.Location = new System.Drawing.Point(429, 175);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(183, 79);
            this.button5.TabIndex = 35;
            this.button5.Text = "STEP 2 - FINAL FUND LEVEL IMPORT ";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button3);
            this.groupBox4.Location = new System.Drawing.Point(429, 39);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(183, 129);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Import From PMS (ImportFolder)";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(13, 47);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(136, 43);
            this.button3.TabIndex = 24;
            this.button3.Text = "Import_data 5-9";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Test
            // 
            this.Test.Location = new System.Drawing.Point(237, 286);
            this.Test.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Test.Name = "Test";
            this.Test.Size = new System.Drawing.Size(97, 43);
            this.Test.TabIndex = 22;
            this.Test.Text = "btntest";
            this.Test.UseVisualStyleBackColor = true;
            this.Test.Click += new System.EventHandler(this.Test_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnUTMC13);
            this.groupBox2.Controls.Add(this.btnUTMC15);
            this.groupBox2.Controls.Add(this.btnUTMC10);
            this.groupBox2.Controls.Add(this.btnUTMC11);
            this.groupBox2.Controls.Add(this.btnUTMC_03);
            this.groupBox2.Controls.Add(this.btnUTMC14);
            this.groupBox2.Location = new System.Drawing.Point(20, 39);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(188, 334);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Member Level Report";
            // 
            // btnUTMC13
            // 
            this.btnUTMC13.Location = new System.Drawing.Point(20, 228);
            this.btnUTMC13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUTMC13.Name = "btnUTMC13";
            this.btnUTMC13.Size = new System.Drawing.Size(136, 43);
            this.btnUTMC13.TabIndex = 30;
            this.btnUTMC13.Text = "UTMC_13";
            this.btnUTMC13.UseVisualStyleBackColor = true;
            this.btnUTMC13.Click += new System.EventHandler(this.btnUTMC13_Click);
            // 
            // btnUTMC15
            // 
            this.btnUTMC15.Location = new System.Drawing.Point(20, 30);
            this.btnUTMC15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUTMC15.Name = "btnUTMC15";
            this.btnUTMC15.Size = new System.Drawing.Size(136, 43);
            this.btnUTMC15.TabIndex = 32;
            this.btnUTMC15.Text = "UTMC_15";
            this.btnUTMC15.UseVisualStyleBackColor = true;
            this.btnUTMC15.Click += new System.EventHandler(this.btnUTMC15_Click);
            // 
            // btnUTMC10
            // 
            this.btnUTMC10.Location = new System.Drawing.Point(20, 128);
            this.btnUTMC10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUTMC10.Name = "btnUTMC10";
            this.btnUTMC10.Size = new System.Drawing.Size(136, 43);
            this.btnUTMC10.TabIndex = 28;
            this.btnUTMC10.Text = "UTMC_10";
            this.btnUTMC10.UseVisualStyleBackColor = true;
            this.btnUTMC10.Click += new System.EventHandler(this.btnUTMC10_Click);
            // 
            // btnUTMC11
            // 
            this.btnUTMC11.Location = new System.Drawing.Point(20, 79);
            this.btnUTMC11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUTMC11.Name = "btnUTMC11";
            this.btnUTMC11.Size = new System.Drawing.Size(136, 43);
            this.btnUTMC11.TabIndex = 29;
            this.btnUTMC11.Text = "UTMC_11";
            this.btnUTMC11.UseVisualStyleBackColor = true;
            this.btnUTMC11.Click += new System.EventHandler(this.btnUTMC11_Click);
            // 
            // btnUTMC_03
            // 
            this.btnUTMC_03.Location = new System.Drawing.Point(20, 277);
            this.btnUTMC_03.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUTMC_03.Name = "btnUTMC_03";
            this.btnUTMC_03.Size = new System.Drawing.Size(136, 43);
            this.btnUTMC_03.TabIndex = 33;
            this.btnUTMC_03.Text = "UTMC_03";
            this.btnUTMC_03.UseVisualStyleBackColor = true;
            this.btnUTMC_03.Click += new System.EventHandler(this.btnUTMC_03_Click);
            // 
            // btnUTMC14
            // 
            this.btnUTMC14.Location = new System.Drawing.Point(20, 178);
            this.btnUTMC14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUTMC14.Name = "btnUTMC14";
            this.btnUTMC14.Size = new System.Drawing.Size(136, 43);
            this.btnUTMC14.TabIndex = 31;
            this.btnUTMC14.Text = "UTMC_14";
            this.btnUTMC14.UseVisualStyleBackColor = true;
            this.btnUTMC14.Click += new System.EventHandler(this.btnUTMC14_Click);
            // 
            // btn_export
            // 
            this.btn_export.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_export.Location = new System.Drawing.Point(368, 519);
            this.btn_export.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_export.Name = "btn_export";
            this.btn_export.Size = new System.Drawing.Size(125, 69);
            this.btn_export.TabIndex = 20;
            this.btn_export.Text = "EXPORT (UTMC 03 - UTMC 18, UTMC 01)";
            this.btn_export.UseVisualStyleBackColor = false;
            this.btn_export.Click += new System.EventHandler(this.btn_export_Click);
            // 
            // btn_import
            // 
            this.btn_import.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_import.Location = new System.Drawing.Point(368, 607);
            this.btn_import.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_import.Name = "btn_import";
            this.btn_import.Size = new System.Drawing.Size(125, 69);
            this.btn_import.TabIndex = 19;
            this.btn_import.Text = "UTMC_02 (FINAL PROCESS)";
            this.btn_import.UseVisualStyleBackColor = false;
            this.btn_import.Click += new System.EventHandler(this.btn_import_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(313, 10);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 62);
            this.button1.TabIndex = 15;
            this.button1.Text = "Select";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "End_Date :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "Start_Date :";
            // 
            // EndDate
            // 
            this.EndDate.CustomFormat = "yyyy/MM/dd";
            this.EndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.EndDate.Location = new System.Drawing.Point(93, 48);
            this.EndDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.EndDate.Name = "EndDate";
            this.EndDate.Size = new System.Drawing.Size(200, 22);
            this.EndDate.TabIndex = 12;
            // 
            // StartDate
            // 
            this.StartDate.CustomFormat = "yyyy/MM/dd";
            this.StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.StartDate.Location = new System.Drawing.Point(93, 10);
            this.StartDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.StartDate.Name = "StartDate";
            this.StartDate.Size = new System.Drawing.Size(200, 22);
            this.StartDate.TabIndex = 11;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.DimGray;
            this.button12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button12.Location = new System.Drawing.Point(692, 362);
            this.button12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(233, 30);
            this.button12.TabIndex = 41;
            this.button12.Text = "Find & Replace All";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(819, 71);
            this.button11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(107, 25);
            this.button11.TabIndex = 40;
            this.button11.Text = "Browse";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(692, 101);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(233, 22);
            this.textBox1.TabIndex = 39;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.SpringGreen;
            this.button7.Location = new System.Drawing.Point(12, 703);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(377, 41);
            this.button7.TabIndex = 37;
            this.button7.Text = "FINAL PROCESS - MONTHLY UTMC REPORT";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(693, 194);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(232, 22);
            this.textBox2.TabIndex = 42;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(692, 255);
            this.textBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(232, 22);
            this.textBox3.TabIndex = 43;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(689, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(251, 18);
            this.label4.TabIndex = 44;
            this.label4.Text = "Folder Level - Find and Replace ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(689, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 18);
            this.label5.TabIndex = 45;
            this.label5.Text = "Step 1:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(691, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 17);
            this.label6.TabIndex = 46;
            this.label6.Text = "Select Folder";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(691, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 18);
            this.label7.TabIndex = 47;
            this.label7.Text = "Step 2:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(691, 166);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 17);
            this.label8.TabIndex = 48;
            this.label8.Text = "Find What";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(691, 229);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 17);
            this.label9.TabIndex = 49;
            this.label9.Text = "Replace With";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(691, 305);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 18);
            this.label10.TabIndex = 50;
            this.label10.Text = "Step 3:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(691, 332);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(250, 17);
            this.label3.TabIndex = 51;
            this.label3.Text = "Look in parameter and click the button";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(421, 714);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(198, 21);
            this.checkBox1.TabIndex = 53;
            this.checkBox1.Text = "DISPLAY 0 UNIT MEMBER";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Location = new System.Drawing.Point(661, 714);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(240, 21);
            this.checkBox3.TabIndex = 54;
            this.checkBox3.Text = "UPDATE_LAST_WORKING_DAY";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // UTMC_Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(961, 754);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "UTMC_Report";
            this.Text = "UTMC_Report_from_Oracle V1.16";
            this.Load += new System.EventHandler(this.UTMC_Report_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOracleConnection;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker StartDate;
        private System.Windows.Forms.DateTimePicker EndDate;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_import;
        private System.Windows.Forms.Button btn_export;
        private System.Windows.Forms.Button Test;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnUTMC13;
        private System.Windows.Forms.Label DateSelection;
        private System.Windows.Forms.Button btnUTMC15;
        private System.Windows.Forms.Button btnUTMC10;
        private System.Windows.Forms.Button btnUTMC11;
        private System.Windows.Forms.Button btnUTMC_03;
        private System.Windows.Forms.Button btnUTMC14;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnUTMC18;
        private System.Windows.Forms.Button btnUTMC16;
        private System.Windows.Forms.Button btnUTMC17;
        private System.Windows.Forms.Button btnUTMC12;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnAdjustment;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
    }
}

