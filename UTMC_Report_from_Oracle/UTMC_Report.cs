﻿using System;
using System.Text;
using System.Windows.Forms;
using Oracle.DataAccess.Client;
using MySql.Data.MySqlClient;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections.Generic;


namespace UTMC_Report_from_Oracle
{
    public partial class UTMC_Report : Form
    {
        string Start_date = "";
        string End_date = "";

        string TransDateUpdate = "";
        string BirthDateupdate = "";
        Timer timer1 = new Timer();  

        private void UTMC_Report_Load(object sender, EventArgs e)
        {
         InitTimer();
        }
        public void InitTimer()
        {


            string SchedulerDate = ConfigurationManager.AppSettings["SchedulerDate"];
            DateTime dt = DateTime.Now;
            string CheckSchedulerDate = dt.ToString("yyyy-MM-dd");       // TimerStart.
            if (SchedulerDate == CheckSchedulerDate)
            {
                timer1.Tick += new EventHandler(timer1_Tick);
                timer1.Interval = 1000; 
                timer1.Start();             
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            string CheckSchedulerTime = dt.ToString("hh:mm:tt");
            string SchedulerTime = ConfigurationManager.AppSettings["SchedulerTime"];

            if (CheckSchedulerTime == SchedulerTime)
            {
                Log.Write("STOP" + DateTime.Now.ToString(), End_date);

                //function 

                timer1.Stop();
            }

            Log.Write("start"+ DateTime.Now.ToString(), End_date);
            
        }
        public UTMC_Report()
        {
            InitializeComponent();
        }

        public string GetOracleConnection()
        {

            string Host = ConfigurationManager.AppSettings["Oracle_Host"];
            string Port = ConfigurationManager.AppSettings["Oracle_Port"];
            string DatabaseName = ConfigurationManager.AppSettings["Oracle_DatabaseName"];
            string Useraame = ConfigurationManager.AppSettings["Oracle_UserName"];
            string Password = ConfigurationManager.AppSettings["Oracle_Password"];




            /*string ConnectionString = "Data Source=(DESCRIPTION="
                                     + "(ADDRESS=(PROTOCOL=TCP)(HOST=202.40.107.98)(PORT=1521))"
                                     + "(CONNECT_DATA=(SERVICE_NAME=TestDB)));"
                                     + "User Id=System;Password=abcd1234;";
             * */
            //return ConnectionString;

            ////// Apex Oracle Connection  Oracle 9i version  10.10.10.141  //202.40.107.3  apex server  //202.40.107.98 // 192.168.0.05  // 192.168.0.05

          //  string ConnectionString = "Data Source = (DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.10.10.141)(PORT = 1521)))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = TestDB))); User Id = SYSTEM; Password = abcd1234;";

            string ConnectionString = "Data Source = (DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = "+Host+")(PORT = "+Port+")))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = "+DatabaseName+"))); User Id = "+Useraame+"; Password = "+Password+";";    
            return ConnectionString;

        }

        public string GetSqlConnection()
        {
            string ServerName = ConfigurationManager.AppSettings["MySql_ServerName"];
            string MyDatabase = ConfigurationManager.AppSettings["MySql_Database"];
            string Myusername = ConfigurationManager.AppSettings["MySql_Username"];
            string Mypassword = ConfigurationManager.AppSettings["MySql_Password"]; 
       
            string conStr = "server="+ServerName+";user id="+Myusername+"; password="+Mypassword+";database="+MyDatabase+"";
            return conStr;
        }

        private void btnOracleConnection_Click(object sender, EventArgs e)
        {

            //   ReadHolderNoforutmc15();

            string name = StartDate.Text;
            string name1 = EndDate.Text;
            string ConnectionString = GetOracleConnection();
            string mysqlConnection = GetSqlConnection();
            MessageBox.Show(ConnectionString);
            try
            {
                OracleConnection connection;
                connection = new OracleConnection(ConnectionString);
                MessageBox.Show("Wait for Open Connection");
                connection.Open();
                MessageBox.Show("Connection Oracle Ready...");
                connection.Close();
                MySqlConnection con;
                con = new MySqlConnection(mysqlConnection);
                con.Open();
                MessageBox.Show("Connection Mysql Ready...");
                con.Close();
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
            }
        }

        private void Process_UTMC18()
        {

        try
            {
                string ConnectionString = GetOracleConnection();
                string Sqlconnstring = GetSqlConnection();
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                   conn.Open();
                    //string mysql = "SELECT  TRANS_DT,UP_SELL, DLY_CAP_VAL,UNIT_IN_ISSUE, FUND_ID"
                    //                + " FROM UTS.PRICE_HISTORY "
                    //                + " WHERE "
                    //                + " FUND_ID IN ('05', '01', '02', '03', '04', '06', '07')"
                    //               + " AND (trans_dt between to_date('" + Start_date + "', 'yyyy/mm/dd')"
                    //                + "  and to_date('" + End_date + "', 'yyyy/mm/dd'))  AND PR_STATUS = 'C'   and rownum <2 " //ADJUSTMENT
                    //                + " ORDER BY TRANS_DT,FUND_ID ASC, seq_no desc";


                   string mysql = "SELECT A.TRANS_DT, A.UP_SELL, A.DLY_CAP_VAL, A.UNIT_IN_ISSUE, A.FUND_ID, A.SEQ_NO FROM UTS.PRICE_HISTORY A INNER JOIN "
                                    + " (SELECT FUND_ID,TRANS_DT, MAX(SEQ_NO) AS MaxRecord FROM UTS.PRICE_HISTORY GROUP BY FUND_ID, TRANS_DT) B  "
                                    + " ON A.FUND_ID = B.FUND_ID AND A.TRANS_DT = B.TRANS_DT AND A.SEQ_NO = B.MaxRecord "
                                    + " WHERE "
                                    + " A.FUND_ID IN ('05', '01', '02', '03', '04', '06', '07')"
                                   + " AND (A.trans_dt between to_date('" + Start_date + "', 'yyyy/mm/dd')"
                                    + "  and to_date('" + End_date + "', 'yyyy/mm/dd'))  AND A.PR_STATUS = 'C'" //ADJUSTMENT
                                    + " ORDER BY A.TRANS_DT, A.FUND_ID ASC";





                    string[] FundIds = null;
                    string EPF_IPD_Code = Read_IPD_info_for_utmc01();

                    using (OracleCommand comm = new OracleCommand(mysql, conn))
                    {
                        using (OracleDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();
                                string ProcessDate = "";
                                while (rdr.Read())
                                {

                                   // MessageBox.Show("Oracle read raedy...");

                                    string IPD_Fund_Code = rdr[4].ToString();
                                    DateTime dt = Convert.ToDateTime(rdr[0].ToString());
                                    string Daily_NAV_Date = dt.ToString("yyyy-MM-dd");
                                    string Daily_NAV = rdr[2].ToString();
                                    string Daily_Unit_created = rdr[3].ToString();
                                    string Daily_Unit_Price = rdr[1].ToString();

                                    decimal daily_Unit_Created_EPF = 0.4M;
                                    decimal Daily_NAV_EPF = 0.2M;

                                  //  if (ProcessDate != Daily_NAV_Date)
                                    {
                                        string ProcessDate1 = dt.AddDays(1).ToString("yyyy-MM-dd");

                                        string ReturnValue = UpdateUTMC18(ProcessDate1,IPD_Fund_Code);
                                        if (ReturnValue == "")
                                        {                                        
                                            ReturnValue = IPD_Fund_Code + ",0.00@";
                                        }
                                        FundIds = ReturnValue.Split('@');

                                        ProcessDate = Daily_NAV_Date;
                                    }

                                    Log.Write("UTMC_018 Final Value : " + FundIds, End_date);

                                    Int16 i = 0;
                                    foreach (var fund in FundIds)
                                    {
                                        if (fund == "")
                                        {
                                            daily_Unit_Created_EPF = 0.0M;
                                            Daily_NAV_EPF = Convert.ToDecimal(daily_Unit_Created_EPF) * Convert.ToDecimal(Daily_Unit_Price);
                                            daily_Unit_Created_EPF = decimal.Round(daily_Unit_Created_EPF, 4);
                                            Daily_NAV_EPF = decimal.Round(Daily_NAV_EPF, 2);
                                            break;
                                        }

                                        string fundcode = fund.Substring(0, 2);
                                        if (fundcode == IPD_Fund_Code)
                                        {
                                            daily_Unit_Created_EPF = Convert.ToDecimal(fund.Substring(3, (fund.Length - 3)));
                                            Daily_NAV_EPF = Convert.ToDecimal(daily_Unit_Created_EPF) * Convert.ToDecimal(Daily_Unit_Price);
                                            daily_Unit_Created_EPF = decimal.Round(daily_Unit_Created_EPF, 4);
                                            Daily_NAV_EPF = decimal.Round(Daily_NAV_EPF, 2);

                                            Log.Write("UTMC_018 Final insert Value : " + Daily_NAV_EPF +"--"+IPD_Fund_Code +"--"+Daily_NAV_Date, End_date);
                                            break;
                                        }

                                        i++;

                                    }

                                    string Query = "Insert into utmc_daily_nav_fund("
                                        + "EPF_IPD_Code, IPD_Fund_Code, Daily_NAV_Date, Daily_NAV, Report_Date,"
                                           + "Daily_Unit_Created, Daily_NAV_EPF, Daily_Unit_Created_EPF, Daily_Unit_Price)"
                                             + " values('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Daily_NAV_Date + "','" + Daily_NAV + "','" + End_date + "','"
                                             + Daily_Unit_created + "', " + " '" + Daily_NAV_EPF + "','" + daily_Unit_Created_EPF + "','" + Daily_Unit_Price + "');";

                                    

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                      //  MessageBox.Show("Insert Ready"); 
                                        mysql_comm.ExecuteNonQuery();
                                        Log.Write("UTMC_018 : " + IPD_Fund_Code + Daily_NAV_Date + Daily_NAV_EPF + "Inserted", End_date);
                                    }
                                }
                                

                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
            }
        }

        protected string UpdateUTMC18(string ProcessDate,string IPDFundCode)
        {
            string Return = "";
            try
            {
                string ConnectionString = GetOracleConnection();            
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    string mysql = "SELECT  S1.FUND_ID, SUM(ROUND(s1.CUR_UNIT_HLDG, 4)) AS TOTAL_UNIT"
                                           + " from UTS.HOLDER_LEDGER s1"
                                            + " inner join"
                                            + " UTS.HOLDER_REG s3 on"
                                            + " s1.HOLDER_NO = s3.HOLDER_NO "
                                           + " INNER JOIN"
                                           + " ("
                                           + "   select max(SORT_SEQ) SORT_SEQ, FUND_ID,"
                                            + "    HOLDER_NO"
                                            + "  from UTS.HOLDER_LEDGER"
                                             + " WHERE TRANS_DT < to_date('" + ProcessDate + "', 'yyyy/mm/dd')"
                                            + "  group by HOLDER_NO,FUND_ID"
                                           + " ) s2"
                                           + "   on s1.HOLDER_NO = s2.HOLDER_NO"
                                            + "  and s1.SORT_SEQ = s2.SORT_SEQ "
                                            + "  where "
                                            + "  s1.CUR_UNIT_HLDG > 0"
                                           + "   AND s3.HOLDER_CLS IN ('EB','EN','ER','ES','EZ','EP')"
                                        //    + "  AND S1.FUND_ID IN ('01','02','03','04','05','06','07') "
                                         + "  AND S1.FUND_ID IN ('" + IPDFundCode + "') "
                                            + "  AND s1.TRANS_DT < to_date('" + ProcessDate + "', 'yyyy/mm/dd')"
                                           + "  GROUP BY S1.FUND_ID"
                                            + "  ORDER BY S1.FUND_ID";


                    using (OracleCommand comm = new OracleCommand(mysql, conn))
                    {
                        using (OracleDataReader rdr = comm.ExecuteReader())
                        {
                           

                                int row = 0;
                                while (rdr.Read())
                                {
                                    row++;
                                    string Fund_Id = rdr[0].ToString();
                                    string Unit_created = rdr[1].ToString();
                                    Return = Return + Fund_Id + "," + Unit_created + "@";

                                }
                                rdr.Close();
                               
                            }
                        }
                    conn.Close();
                    }
                
                    return Return;
                }
            
            catch (Exception ex)
            {
                ex.ToString();
                return "fail";
            }

        }

        private void Process_UTMC17()
        {
            try
            {
                string ConnectionString = GetOracleConnection();
                string Sqlconnstring = GetSqlConnection();

                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    string mysql = "SELECT FUND_ID,(div_tax_exem+div_tax+ div_equalsn_rt),REINV_DT,NET_DIV_RT FROM UTS.NEW_DIV_DISTN"
                                    + " WHERE (distn_dt between to_date('" + Start_date + "', 'yyyy/mm/dd')"
                                    + " and to_date('" + End_date + "', 'yyyy/mm/dd')) and "
                                    + " FUND_ID IN ('01','02','03','04','05','06','07')"
                                    + " ORDER BY distn_dt";
                    string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                    using (OracleCommand comm = new OracleCommand(mysql, conn))
                    {
                        using (OracleDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();

                                while (rdr.Read())
                                {

                                    string IPD_Fund_Code = rdr[0].ToString();
                                    DateTime dt = Convert.ToDateTime(rdr[2].ToString());
                                    string Corporate_Action_Date = dt.ToString("yyyy-MM-dd");
                                    decimal Distributions = Convert.ToDecimal(rdr[1].ToString());
                                    Distributions = decimal.Round(Distributions, 4);
                                    //  decimal Unit_Splits = Convert.ToDecimal(rdr[3].ToString());
                                    string Unit_Splits = "0.0000";
                                    decimal Net_Distrubutions = Convert.ToDecimal(rdr[3].ToString());
                                    Net_Distrubutions = decimal.Round(Net_Distrubutions, 4);

                                    string Query = "Insert into utmc_fund_corporate_actions(EPF_IPD_Code,IPD_Fund_Code,Corporate_Action_Date,Distributions,Unit_Splits,Report_Date,Net_Distributions) "
                                   + " values('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Corporate_Action_Date + "','" + Distributions + "','" + Unit_Splits + "','" + End_date + "','"+Net_Distrubutions+"')";

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        Log.Write("UTMC_017 : " +IPD_Fund_Code + IPD_Fund_Code +  Corporate_Action_Date + "Inserted", End_date);
                                        mysql_comm.ExecuteNonQuery();
                                    }
                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }

        private void Process_UTMC16()
        {
            try
            {
                string ConnectionString = GetOracleConnection();
                string Sqlconnstring = GetSqlConnection();

                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    //string mysql = "SELECT FUND_ID, TRANS_DT, UP_SELL, UNIT_IN_ISSUE, UP_SELL * UNIT_IN_ISSUE "
                    //                  + " FROM UTS.PRICE_HISTORY "
                    //                  + " WHERE FUND_ID IN ('05', '01', '02', '03', '04', '06', '07') AND "
                    //                   + " (trans_dt between to_date('" + Start_date + "', 'yyyy/mm/dd')"
                    //                + "  and to_date('" + End_date + "', 'yyyy/mm/dd'))  AND PR_STATUS = 'C'"    //ADJUSTMENT
                    //                  + " ORDER BY FUND_ID, TRANS_DT ASC";


                    string mysql = "SELECT A.FUND_ID, A.TRANS_DT, A.UP_SELL, A.UNIT_IN_ISSUE, A.UP_SELL * A.UNIT_IN_ISSUE "
                                + " FROM UTS.PRICE_HISTORY A INNER JOIN "
                                + " (SELECT FUND_ID,TRANS_DT, MAX(SEQ_NO) AS MaxRecord FROM UTS.PRICE_HISTORY GROUP BY FUND_ID, TRANS_DT) B "
                                + " ON A.FUND_ID = B.FUND_ID AND A.TRANS_DT = B.TRANS_DT AND A.SEQ_NO = B.MaxRecord "
                                + " WHERE A.FUND_ID IN ('05', '01', '02', '03', '04', '06', '07') AND "
                                + " (A.trans_dt between to_date('" + Start_date + "', 'yyyy/mm/dd')"
                                + "  and to_date('" + End_date + "', 'yyyy/mm/dd'))  AND A.PR_STATUS = 'C'"    //ADJUSTMENT
                                + " ORDER BY A.FUND_ID, A.TRANS_DT ASC";







                    string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                    using (OracleCommand comm = new OracleCommand(mysql, conn))
                    {
                        using (OracleDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();
                                while (rdr.Read())
                                {

                                    string IPD_Fund_Code = rdr[0].ToString();
                                    DateTime dt = Convert.ToDateTime(rdr[1].ToString());
                                    string NAV_Date_Daily = dt.ToString("yyyy-MM-dd");
                                    string Nav_Per_Unit = rdr[2].ToString();
                                    string Total_Units_Circulation = rdr[3].ToString();

                                    DateTime now = dt;
                                    var startDate = new DateTime(now.Year, now.Month, 1);
                                    var endDate = startDate.AddMonths(1).AddDays(-1);

                                    decimal Units_TT = Convert.ToDecimal(Total_Units_Circulation.ToString());
                                    Units_TT = decimal.Round(Units_TT, 4);
                                    Total_Units_Circulation = Convert.ToString(Total_Units_Circulation.ToString());


                                    string Query = "Insert into utmc_fund_daily_nav(EPF_IPD_Code,IPD_Fund_Code,NAV_Date_Daily,Nav_Per_Unit,Total_Units_Circulation,report_date) "
                                    + " values('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + NAV_Date_Daily + "','" + Nav_Per_Unit + "','" + Total_Units_Circulation + "','" + End_date + "')";
                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        Log.Write("UTMC_016 : " + IPD_Fund_Code + IPD_Fund_Code + NAV_Date_Daily + "Inserted", End_date);
                                        mysql_comm.ExecuteNonQuery();
                                    }
                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void Process_UTMC15()
        {
            string IPD_Fund_Code = "";
            string IPD_Member_Acc_No = "";

            try
            {
                string ConnectionString = GetOracleConnection();
                string Sqlconnstring = GetSqlConnection();

                
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    string NAV_PricePU_Dt = End_date;
                    NAV_PricePU_Dt = NAV_PricePU_Dt.Replace("/", "-");
                    BirthDateupdate = BirthDateupdate.Replace("/", "-");

                    //NAV_PricePU_Dt = "2016-08-30";
                    
                    string mysql = "select S3.ID_NO_2 AS EPF_NO, S1.FUND_ID,s1.HOLDER_NO,"
                                     + " S1.TRANS_UNITS,S1.TRANS_AMT, s1.TRANS_DT, s1.CUR_UNIT_HLDG, S3.ID_NO,"
                                     + " s3.BIRTH_DT, S1.TRANS_TYPE,S1.TRANS_PR, (S4.UP_SELL * s1.CUR_UNIT_HLDG) AS MARKET_VALUE"
                                        + " from UTS.HOLDER_LEDGER s1 inner join  UTS.HOLDER_REG s3 on "
                                      + "  s1.HOLDER_NO = s3.HOLDER_NO "
                                      + " INNER JOIN UTS.PRICE_HISTORY S4 ON S1.FUND_ID = S4.FUND_ID AND S4.TRANS_DT =  to_date('" + NAV_PricePU_Dt + "', 'yyyy/mm/dd')"
                                     + " AND S4.PR_STATUS = 'C' INNER JOIN "    //ADJUSTMENT ONLY COMPLETE STATUS FOR PRICE HISTORY
                                     + " (   select max(SORT_SEQ) SORT_SEQ, MAX(TRANS_DT) TRANS_DT, FUND_ID,    HOLDER_NO "
                                     + "  from UTS.HOLDER_LEDGER "
                                     + "  WHERE TRANS_DT < to_date('" + TransDateUpdate + "', 'yyyy/mm/dd')"
                                     + "  AND NOT HOLDER_NO IN ('9504', '19765')  "  //adjusment
                                     + "  group by HOLDER_NO,FUND_ID  "
                                     + "  ) s2    "
                                     + " on s1.HOLDER_NO = s2.HOLDER_NO  and s1.SORT_SEQ = s2.SORT_SEQ "  // adjustment .. AND s1.TRANS_DT = s2.TRANS_DT"
                                     + " where   s1.CUR_UNIT_HLDG > 0 AND s3.HOLDER_CLS IN ('EB','EN','ER','ES','EZ','EP')  "
                                     + " AND S3.BIRTH_DT > to_date('" + BirthDateupdate + "', 'yyyy/mm/dd') "
                                     + " AND S1.FUND_ID IN ('01','02','03','04','05','06','07') AND s1.TRANS_DT < to_date('" + TransDateUpdate + "', 'yyyy/mm/dd')  "
                                     + " ORDER BY S1.FUND_ID, S1.HOLDER_NO";
                    string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                    using (OracleCommand comm = new OracleCommand(mysql, conn))
                    {
                        using (OracleDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();

                                while (rdr.Read())
                                {
                                    string Member_EPF_No = rdr[0].ToString();

                                    IPD_Fund_Code = rdr[1].ToString();
                                    IPD_Member_Acc_No = rdr[2].ToString();
                                    string Actual_Transferred_From_EPF_RM = rdr[4].ToString();
                                    string Units = rdr[6].ToString();
                                    string Book_Value = rdr[4].ToString();
                                    string Market_Value = rdr[11].ToString();


                                    // adjustment 2016 09

                                    if (End_date == "2016/07/31")
                                    {
                                        if (IPD_Fund_Code == "06")
                                        {

                                            decimal Market_Value_TT = 0.00M;
                                            decimal Units_TT = Convert.ToDecimal(Units.ToString());
                                            Market_Value_TT = (Units_TT * Convert.ToDecimal("0.2021".ToString()));
                                            Market_Value_TT = decimal.Round(Market_Value_TT, 4);
                                            Market_Value = Convert.ToString(Market_Value_TT.ToString());

                                        }
                                    }

                                    // adjustment 2016 09

                                    DateTime dt = Convert.ToDateTime(rdr[5].ToString());
                                    string TransDate = dt.ToString("yyyy-MM-dd");

                                    DateTime now = Convert.ToDateTime(rdr[5].ToString());
                                    var startDate = new DateTime(now.Year, now.Month, 1);
                                    var endDate = startDate.AddMonths(1).AddDays(-1);
                                    string Effective_Date = End_date;

                                    string Query = "Insert into utmc_member_investment(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Actual_Transferred_From_EPF_RM,Units,Book_Value,Market_Value,Effective_Date,Report_Date) "
                                   + " values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "', '" + IPD_Member_Acc_No + "','" + Actual_Transferred_From_EPF_RM + "','" + Units + "','" + Book_Value + "','" + Market_Value + "','" + End_date + "','" + End_date + "')";

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        Log.Write("UTMC_015 : " + IPD_Fund_Code + IPD_Fund_Code + IPD_Member_Acc_No + "Inserted", End_date);
                                        mysql_comm.ExecuteNonQuery();

                                        //System.Threading.Thread.Sleep(20);
                                    }
                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }
                
                System.Threading.Thread.Sleep(2000);
                
                ReadHolderNoforutmc15();
            }
            catch (Exception ex)
            {
                Log.Write("UTMC_015 : " + IPD_Fund_Code  + IPD_Member_Acc_No + " - Insert Failed", End_date);
                                       
                ex.ToString();
            }
        }

        protected void ReadActualCostUTMC15()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "SELECT * FROM eppa_wbr1.utmc_member_investment where Report_Date = '" + End_date + "' and Actual_Transferred_From_EPF_RM = 0;";

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {

                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr);

                            rdr.Close();
                            mysql_conn.Close();

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                string Holder_No = row[3].ToString();
                                string IPD_fund_Code = row[2].ToString();
                                Get_ActualCost(Holder_No, IPD_fund_Code);

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "---Get Final Read 0 unit Actual Cost Holder No From utmc_member_investment failed--- timeout", End_date);
            }

        }



        protected void ReadHolderNoforutmc15()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "SELECT IPD_Member_Acc_No,IPD_Fund_Code FROM utmc_member_investment where Report_date=' " + End_date + "'";  //
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();


                    

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {

                    
                            
                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr);

                            rdr.Close();
                            mysql_conn.Close();

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                string EPF_No = row[0].ToString();
                                string IPD_fund_Code = row[1].ToString();
                                Get_Redumptiondate(EPF_No, IPD_fund_Code);
                            
                            }

                            
                            //while (rdr.Read())
                            //{
                            //    string EPF_No = rdr[0].ToString();
                            //    string IPD_fund_Code = rdr[1].ToString();
                            //    Get_Redumptiondate(EPF_No, IPD_fund_Code);
                            //}
                            //rdr.Close();
                            //mysql_conn.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "---Get Final Read Holder No From utmc_member_investment failed--- timeout", End_date);
            }

        }


        protected void Get_ActualCost(string Holder_No, string Fund_ID)
        {

            string Sqlconnstring = GetOracleConnection();
            
            try
            {

                decimal Actual_transfer_value = 0.0000M;
                decimal Book_value = 0.0000M;
                decimal Unit_Holding_value = 0.0000M;
                string Trans_Type = "";
                decimal Total_Redemption_cost = 0.0000M;
                decimal UnitCostRD = 0.0000M;
                decimal UnitCostRDfinal = 0.0000M;
                string AccountType = "";
                decimal Actual_Cost = 0.0000M;



                string Mysql = "SELECT TRANS_NO, TRANS_TYPE, ACC_TYPE, ENTRY_DT, TRANS_DT,"
                                 + " TRANS_UNITS, TRANS_AMT AS BIG,"
                                 + " (TRANS_UNITS * TRANS_PR) AS SMALL, CUR_UNIT_HLDG, CUM_VALUE_HLDG "
                                 + " FROM UTS.HOLDER_LEDGER "
                                 + " WHERE "
                                 + " HOLDER_NO='" + Holder_No + "' AND "
                                 + " FUND_ID='" + Fund_ID + "' AND "
                                 + " trans_type IN ('SA','RD','DD','BI','UC', 'TR') "
                                 + " AND  TRANS_DT <= to_date('" + End_date + "', 'yyyy/mm/dd') "
                                 + " ORDER BY TRANS_DT ASC,SORT_SEQ ASC, TRANS_TYPE DESC ";

                using (OracleConnection mysql_conn = new OracleConnection(Sqlconnstring))
                {
                    mysql_conn.Open();



                    using (OracleCommand comm = new OracleCommand(Mysql, mysql_conn))
                    {
                        int Count = 0; string[] numb;
                        using (OracleDataReader rdr1 = comm.ExecuteReader())
                        {

                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr1);

                            numb = new string[dataTable.Rows.Count];

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                numb[Count++] = row[0].ToString();
                            }




                            //while (rdr1.Read())
                            //{
                            //    numb[Count++] = rdr1[0].ToString();

                            //}
                            Array.Resize(ref numb, Count);
                            //}

                            // using (OracleDataReader rdr = comm.ExecuteReader())
                            //   {


                            Count = 0;
                            string Trans_String;


                            // while (rdr.Read())
                            //{

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                try
                                {

                                    Trans_String = row[0].ToString();
                                    AccountType = row[2].ToString();
                                    bool TTT = false;


                                    // check reversal
                                    for (int i = Count; i < numb.Length; i++)
                                    {
                                        if (Trans_String + "X" == numb[i].ToString())
                                        {
                                            // rdr.NextResult();
                                            TTT = true;
                                            break;

                                        }

                                        char Check_reversed = Trans_String[Trans_String.Length - 1];

                                        if (Check_reversed == 'X')
                                        {
                                            TTT = true;

                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                                            break;
                                        }

                                    }

                                    Count++;

                                    if (TTT == false)
                                    {


                                        Trans_Type = row[1].ToString().Trim();

                                        if (Trans_Type == "SA")
                                        {
                                            string Trans_no = row[0].ToString();
                                            char Check_reversed = Trans_no[Trans_no.Length - 1];

                                            if (Check_reversed == 'X')
                                            {
                                                Trans_no = Trans_no.TrimEnd(Trans_no[Trans_no.Length - 1]);
                                                Actual_transfer_value -= Convert.ToDecimal(row[6].ToString());
                                                Book_value -= Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                                // new actual transfer
                                                if (!(AccountType == "SW"))
                                                {
                                                    Actual_Cost -= Convert.ToDecimal(row[6].ToString());
                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                }


                                            }
                                            else
                                            {
                                                Actual_transfer_value += Convert.ToDecimal(row[6].ToString());
                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                                // new actual transfer
                                                if (!(AccountType == "SW"))
                                                {
                                                    Actual_Cost += Convert.ToDecimal(row[6].ToString());
                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                }



                                            }

                                        }
                                        if ((Trans_Type == "DD") || (Trans_Type == "BI") || (Trans_Type == "SA") || (Trans_Type == "UC"))
                                        {
                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);


                                            if ((Trans_Type == "DD") || (Trans_Type == "BI"))
                                            {
                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                // rounding
                                                Book_value = decimal.Round(Book_value, 2);

                                            }

                                        }
                                        if (Trans_Type == "RD" || Trans_Type == "TR")
                                        {
                                            string Trans_noRD = row[0].ToString();
                                            char Check_reversedRD = Trans_noRD[Trans_noRD.Length - 1];

                                            // Total_Redemption_cost_forRD = Convert.ToDecimal(row[5].ToString());
                                            // Total_Redemption_cost_forRD = decimal.Round(Total_Redemption_cost, 4);

                                            //if (!(Trans_Type == "TR"))
                                            //{
                                            Total_Redemption_cost = Convert.ToDecimal(row[8].ToString());
                                            Total_Redemption_cost = decimal.Round(Total_Redemption_cost, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);
                                            // }


                                            UnitCostRDfinal = (Book_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redemption_cost);
                                                                * UnitCostRD);
                                            UnitCostRDfinal = decimal.Round(UnitCostRDfinal, 2);

                                            if (Convert.ToDecimal(Total_Redemption_cost) > 0)
                                            {
                                                if (!(Check_reversedRD == 'X'))                             // Check reversal
                                                {


                                                    Actual_transfer_value = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                                               * Total_Redemption_cost);
                                                    Book_value = (Book_value / Convert.ToDecimal(Unit_Holding_value)
                                                                              * Total_Redemption_cost);



                                                    Book_value = decimal.Round(Book_value, 2);
                                                    Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                                    // new actual transfer
                                                    if (!(AccountType == "SW"))
                                                    {
                                                        Actual_Cost -= Convert.ToDecimal(row[6].ToString());
                                                        Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                    }



                                                    // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Actual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                                                }
                                                else  // reversal for RD and TR
                                                {
                                                    // new actual transfer
                                                    if (!(AccountType == "SW"))
                                                    {
                                                        Actual_Cost += Convert.ToDecimal(row[6].ToString());
                                                        Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                    }

                                                }

                                            }
                                            else
                                            {
                                                Actual_transfer_value = 0.0000M;
                                                Book_value = 0.0000M;
                                                //UnitCostRDfinal = 0.0000M;
                                                Unit_Holding_value = 0.0000M;

                                                // new actual transfer
                                                if (!(AccountType == "SW"))
                                                {
                                                    Actual_Cost -= Convert.ToDecimal(row[6].ToString());
                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                }
                                            }

                                        }





                                    }

                                }
                                catch (Exception ex)
                                {
                                    Log.Write(ex.ToString() + "---Get Actual Cost Update failed--- NAV", End_date);
                                }




                            }
                            Actual_Transfer_Cost_Update(Holder_No, Fund_ID, Actual_Cost);
                            // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Actual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                            //rdr.Close();

                            mysql_conn.Close();


                        }
                    }
                }
            }



            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "---Get Actual Cost Update failed--- NAV", End_date);
            }
        }



        protected void Get_Redumptiondate(string Holder_No, string Fund_ID, bool IsInsert = false)
        {

            string Sqlconnstring = GetOracleConnection();
            try
            {

                decimal Actual_transfer_value = 0.0000M;
                decimal Book_value = 0.0000M;
                decimal Unit_Holding_value = 0.0000M;
                string Trans_Type = "";
                decimal Total_Redemption_cost = 0.0000M;
                decimal UnitCostRD = 0.0000M;
                decimal UnitCostRDfinal = 0.0000M;
                string AccountType = "";
                decimal Actual_Cost = 0.0000M;                                            



                string Mysql = "SELECT TRANS_NO, TRANS_TYPE, ACC_TYPE, ENTRY_DT, TRANS_DT,"
                                 + " TRANS_UNITS, TRANS_AMT AS BIG,"
                                 + " (TRANS_UNITS * TRANS_PR) AS SMALL, CUR_UNIT_HLDG, CUM_VALUE_HLDG "
                                 + " FROM UTS.HOLDER_LEDGER "
                                 + " WHERE "
                                 + " HOLDER_NO='" + Holder_No + "' AND "
                                 + " FUND_ID='" + Fund_ID + "' AND "
                                 + " trans_type IN ('SA','RD','DD','BI','UC', 'TR') "
                                 + " AND  TRANS_DT <= to_date('" + End_date + "', 'yyyy/mm/dd') "
                                 + " ORDER BY TRANS_DT ASC,SORT_SEQ ASC, TRANS_TYPE DESC ";

                using (OracleConnection mysql_conn = new OracleConnection(Sqlconnstring))
                {
                    mysql_conn.Open();



                    using (OracleCommand comm = new OracleCommand(Mysql, mysql_conn))
                    {
                        int Count = 0; string[] numb;
                        using (OracleDataReader rdr1 = comm.ExecuteReader())
                        {

                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr1);

                            numb = new string[dataTable.Rows.Count];

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                numb[Count++] = row[0].ToString();
                            }




                            //while (rdr1.Read())
                            //{
                            //    numb[Count++] = rdr1[0].ToString();

                            //}
                            Array.Resize(ref numb, Count);
                            //}

                            // using (OracleDataReader rdr = comm.ExecuteReader())
                            //   {


                            Count = 0;
                            string Trans_String;


                            // while (rdr.Read())
                            //{

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                try
                                {

                                    Trans_String = row[0].ToString();
                                    AccountType = row[2].ToString();
                                    bool TTT = false;


                                    // check reversal
                                    for (int i = Count; i < numb.Length; i++)
                                    {
                                        if (Trans_String + "X" == numb[i].ToString())
                                        {
                                            // rdr.NextResult();
                                            TTT = true;
                                            break;

                                        }

                                        char Check_reversed = Trans_String[Trans_String.Length - 1];

                                        if (Check_reversed == 'X')
                                        {
                                            TTT = true;

                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                                            break;
                                        }

                                    }

                                    Count++;

                                    if (TTT == false)
                                    {


                                        Trans_Type = row[1].ToString().Trim();

                                        if (Trans_Type == "SA")
                                        {
                                            string Trans_no = row[0].ToString();
                                            char Check_reversed = Trans_no[Trans_no.Length - 1];

                                            if (Check_reversed == 'X')
                                            {
                                                Trans_no = Trans_no.TrimEnd(Trans_no[Trans_no.Length - 1]);
                                                Actual_transfer_value -= Convert.ToDecimal(row[6].ToString());
                                                Book_value -= Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                                // new actual transfer
                                                if (!(AccountType == "SW"))
                                                {
                                                    Actual_Cost -= Convert.ToDecimal(row[6].ToString());
                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                }
                                                

                                            }
                                            else
                                            {
                                                Actual_transfer_value += Convert.ToDecimal(row[6].ToString());
                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                                // new actual transfer
                                                if (!(AccountType == "SW"))
                                                {
                                                    Actual_Cost += Convert.ToDecimal(row[6].ToString());
                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                }
                                                


                                            }

                                        }
                                        if ((Trans_Type == "DD") || (Trans_Type == "BI") || (Trans_Type == "SA") || (Trans_Type == "UC"))
                                        {
                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);


                                            if ((Trans_Type == "DD") || (Trans_Type == "BI"))
                                            {
                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                // rounding
                                                Book_value = decimal.Round(Book_value, 2);

                                            }

                                        }
                                        if (Trans_Type == "RD" || Trans_Type == "TR" )
                                        {
                                            string Trans_noRD = row[0].ToString();
                                            char Check_reversedRD = Trans_noRD[Trans_noRD.Length - 1];

                                            // Total_Redemption_cost_forRD = Convert.ToDecimal(row[5].ToString());
                                            // Total_Redemption_cost_forRD = decimal.Round(Total_Redemption_cost, 4);

                                            //if (!(Trans_Type == "TR"))
                                            //{
                                            Total_Redemption_cost = Convert.ToDecimal(row[8].ToString());
                                            Total_Redemption_cost = decimal.Round(Total_Redemption_cost, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);
                                            // }


                                            UnitCostRDfinal = (Book_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redemption_cost);
                                                                * UnitCostRD);
                                            UnitCostRDfinal = decimal.Round(UnitCostRDfinal, 2);

                                            if (Convert.ToDecimal(Total_Redemption_cost) > 0)
                                            {
                                                if (!(Check_reversedRD == 'X'))                             // Check reversal
                                                {


                                                    Actual_transfer_value = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                                               * Total_Redemption_cost);
                                                    Book_value = (Book_value / Convert.ToDecimal(Unit_Holding_value)
                                                                              * Total_Redemption_cost);



                                                    Book_value = decimal.Round(Book_value, 2);
                                                    Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                                    // new actual transfer
                                                    if (!(AccountType == "SW"))
                                                    {
                                                        Actual_Cost -= Convert.ToDecimal(row[6].ToString());
                                                        Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                    }



                                                    // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Actual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                                                }
                                                else  // reversal for RD and TR
                                                {
                                                    // new actual transfer
                                                    if (!(AccountType == "SW"))
                                                    {
                                                        Actual_Cost += Convert.ToDecimal(row[6].ToString());
                                                        Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                    }
                                                
                                                }

                                            }
                                            else
                                            {
                                                Actual_transfer_value = 0.0000M;
                                                Book_value = 0.0000M;
                                                //UnitCostRDfinal = 0.0000M;
                                                Unit_Holding_value = 0.0000M;

                                                // new actual transfer
                                                if (!(AccountType == "SW"))
                                                {
                                                    Actual_Cost -= Convert.ToDecimal(row[6].ToString());
                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                }
                                            }

                                        }





                                    }

                                }
                                catch (Exception ex)
                                {
                                    Log.Write(ex.ToString() + "---Get Redemption Update failed--- NAV" , End_date);
                                }




                            }
                            Actual_transfer_BookValue_update(Holder_No, Fund_ID, Actual_Cost, Actual_transfer_value, Book_value, UnitCostRDfinal, IsInsert, Unit_Holding_value.ToString());
                            // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Actual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                            //rdr.Close();
                            mysql_conn.Close();


                        }
                    }
                }
            }



            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "---Get Final Redemption Update failed--- NAV", End_date);
            }
        }

        protected void Actual_transfer_BookValue_update(string HolderNo, string IPD_fund_Code, decimal Actual_Cost, decimal Actual_Transfer, decimal Book_value, decimal Redumption_cost, bool IsInsert = false, string Units = "0.00")
        {

            
                    string Query = "";
            //string ConnectionString = GetOracleConnection();
            string Sqlconnstring = GetSqlConnection();
            try
            {
                //    using (OracleConnection conn = new OracleConnection(ConnectionString))
                //  {

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    if (IsInsert)
                    {

                        string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                        string Member_EPF_No = "";

                        // string Units = "0.00";

                        string Market_Value = "0.00";


                        string OracleSql = " SELECT A1.HOLDER_NO,  A1.ID_NO AS NRIC,  A1.ID_NO_2 AS EPF_NO, "
                                                         + " to_date(A1.BIRTH_DT),   A1.SEX,    A1.HOLDER_CLS,   A1.HOLDER_STATUS FROM  "
                                                         + " UTS.HOLDER_REG A1 WHERE A1.HOLDER_NO='" + HolderNo + "'";
                        //  + " ( "
                        //  + "SELECT s3.HOLDER_NO   "
                        //  + "from UTS.HOLDER_LEDGER s1 inner join UTS.HOLDER_REG s3 on s1.HOLDER_NO = s3.HOLDER_NO   "
                        //  + "INNER JOIN  "
                        //  + "  (   select max(SORT_SEQ) SORT_SEQ, MAX(TRANS_DT) TRANS_DT, FUND_ID,    HOLDER_NO   "
                        // + "  from UTS.HOLDER_LEDGER  "
                        //  + "  WHERE TRANS_DT < to_date('" + TransDateUpdate + "', 'yyyy/mm/dd') "
                        //  + "  group by HOLDER_NO,FUND_ID   "
                        //  + "  ) s2  "
                        //  + "on s1.HOLDER_NO = s2.HOLDER_NO  and s1.SORT_SEQ = s2.SORT_SEQ AND s1.TRANS_DT = s2.TRANS_DT  "
                        //  + "where   s1.CUR_UNIT_HLDG > 0 AND s3.HOLDER_CLS IN ('EB','EN','ER','ES','EZ','EP')  "
                        //+ "AND S3.BIRTH_DT > to_date('" + BirthDateupdate + "', 'yyyy/mm/dd')  "
                        // + "AND S1.FUND_ID IN ('01','02','03','04','05','06','07') AND s1.TRANS_DT < to_date('" + TransDateUpdate + "', 'yyyy/mm/dd') "
                        // + "GROUP BY S3.HOLDER_NO) ";


                        string ConnectionString = GetOracleConnection();


                        using (OracleConnection conn = new OracleConnection(ConnectionString))
                        {
                            conn.Open();

                            using (OracleCommand comm = new OracleCommand(OracleSql, conn))
                            {
                                using (OracleDataReader rdr = comm.ExecuteReader())
                                {
                                    while (rdr.Read())
                                    {
                                        string Passport_No = rdr[1].ToString();
                                        string EPF_No = rdr[2].ToString();
                                        Member_EPF_No = EPF_No;
                                        string NIC = "";
                                        // string NIC_Date = "";
                                        DateTime dt = Convert.ToDateTime(rdr[3].ToString());
                                        string BirthDate = dt.ToString("yyyy-MM-dd");
                                        string Gender = rdr[4].ToString();

                                        DateTime now = DateTime.Now;
                                        var startDate = new DateTime(now.Year, now.Month, 1);
                                        var endDate = startDate.AddMonths(1).AddDays(-1);
                                        string EndofMonth = endDate.ToString("yyyy-MM-dd");
                                        // Effective

                                        DateTime dt1 = Convert.ToDateTime(rdr[3].ToString());
                                        string Release_Date = dt1.ToString("yyyy-MM-dd");
                                        string year = DateTime.Now.Year.ToString();
                                        Release_Date = Release_Date.Remove(0, 4).Insert(0, year);



                                        Query =
                                          "Insert into eppa_wbr1.utmc_member_information(Code,Passport_No,EPF_No,NIC,Birthdate,Gender, Effective_Date_Of_Report,report_date) "
                                         + " values('" + EPF_IPD_Code + "', '" + Passport_No + "','" + EPF_No + "','" + NIC + "','" + BirthDate + "','" + Gender + "','" + End_date + "','" + End_date + "');";

                                    }
                                }
                            }
                        }

                        Query += "\n" + "Insert into eppa_wbr1.utmc_member_investment(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Actual_Transferred_From_EPF_RM,Units,Book_Value,Market_Value,Effective_Date,Report_Date) "
                     + " values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_fund_Code + "', '" + HolderNo + "','" + Actual_Transfer + "','" + Units + "','" + Book_value + "','" + Market_Value + "','" + End_date + "','" + End_date + "')";


                    }

                    else
                    {
                        Query = "UPDATE eppa_wbr1.utmc_member_investment SET Actual_Cost = '" + Actual_Cost + "', Actual_Transferred_From_EPF_RM='" + Actual_Transfer + "', Book_Value='" + Book_value + "', Redemption_Cost='" + Redumption_cost + "' where IPD_Member_Acc_No='" + HolderNo + "' AND IPD_Fund_Code= '" + IPD_fund_Code + "' AND Report_Date='" + End_date + "' ";


                    }

                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {



                        mysql_comm.ExecuteNonQuery();

                        //System.Threading.Thread.Sleep(20);
                        Log.Write(Query, End_date);
                    }
                    mysql_conn.Close();
                }
                //  }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "/n" + Query, End_date);
            }

        }



        protected void Actual_Transfer_Cost_Update(string HolderNo, string IPD_fund_Code, decimal Actual_Cost)
        {


            string Query = "";
            //string ConnectionString = GetOracleConnection();
            string Sqlconnstring = GetSqlConnection();
            try
            {
                //    using (OracleConnection conn = new OracleConnection(ConnectionString))
                //  {

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();
                    Query = "UPDATE eppa_wbr1.utmc_member_investment SET Actual_Cost = '" + Actual_Cost + "' where IPD_Member_Acc_No='" + HolderNo + "' AND IPD_Fund_Code= '" + IPD_fund_Code + "' AND Report_Date='" + End_date + "' ";


                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {



                        mysql_comm.ExecuteNonQuery();

                        //System.Threading.Thread.Sleep(20);
                        Log.Write(Query, End_date);
                    }
                    mysql_conn.Close();
                }
                //  }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "/n" + Query, End_date);
            }

        }



        private void Process_UTMC14()
        {

            try
            {
                //string ConnectionString = GetOracleConnection();
                string Sqlconnstring = GetSqlConnection();

                using (MySqlConnection conn = new MySqlConnection(Sqlconnstring))
                {
                    conn.Open();
                                   
                    string mysql = "SELECT Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No, "
                                     + " Date_Of_Transaction,Transaction_Code,IPD_Unique_Transaction_ID,Reversed_Transaction_ID,Units,Cost_RM"
                                      + " FROM eppa_wbr1.utmc_compositional_transactions "
                                     + " where Transaction_Code in ('TO','RO') and Report_Date='"+End_date+"'";


                    string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                    using (MySqlCommand comm = new MySqlCommand(mysql, conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();

                                while (rdr.Read())
                                {
                                    string Member_EPF_No = rdr[0].ToString();

                                    string IPD_Fund_Code = rdr[2].ToString();
                                    string IPD_Member_Acc_No = rdr[3].ToString();

                                    DateTime dt = Convert.ToDateTime(rdr[4].ToString());
                                    string Release_Date = dt.ToString("yyyy-MM-dd");                                                                   
                                    string IPD_Unique_Transaction_ID = rdr[6].ToString();
                                    DateTime now = Convert.ToDateTime(rdr[4].ToString());
                                    var startDate = new DateTime(now.Year, now.Month, 1);
                                    var endDate = startDate.AddMonths(1).AddDays(-1);
                                    string Effective_Date = endDate.ToString("yyyy-MM-dd");
                                   
                                     decimal Units = Convert.ToDecimal(rdr[8].ToString());                                   
                                    decimal Cost = Convert.ToDecimal(rdr[9].ToString());                                 
                                    string Transaction_Code = rdr[5].ToString();
                                    string Reversed_Transaction_ID = rdr[7].ToString();

                                    Cost = decimal.Round(Cost, 2);

                                    string Query =
                                      "Insert into utmc_release_control(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Release_Date,Transaction_Code,IPD_Unique_Transaction_ID,Reversed_Transaction_ID,Units,Cost_RM,Effective_Date,report_date)"
                                    + " values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + IPD_Member_Acc_No + "','" + Release_Date + "','" + Transaction_Code + "','" + IPD_Unique_Transaction_ID + "','" + Reversed_Transaction_ID + "','" + Units + "','" + Cost + "','" + Effective_Date + "','" + Effective_Date + "');";

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        Log.Write("UTMC_014 : " + IPD_Fund_Code + IPD_Fund_Code + IPD_Member_Acc_No + "Inserted", End_date);
                                        mysql_comm.ExecuteNonQuery();
                                    }
                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }

        private void Process_UTMC13()
        {

            try
            {
                string ConnectionString = GetOracleConnection();
                string Sqlconnstring = GetSqlConnection();

                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();


                    string mysql = "select  A.FUND_ID,"
                                        + " B.ID_NO_2 AS EPF_NO,"
                                        + " A.HOLDER_NO,"
                                        + "  A.TRANS_DT,"
                                        + " A.TRANS_NO,"
                                        + " A.TRANS_TYPE,"
                                        + "  A.ACC_TYPE,"
                                        + "  A.TRANS_UNITS,"
                                        + "  A.TRANS_AMT,"
                                        + "  A.FEE_AMT,"
                                        + "  A.GST_AMT, "
                                       + "  A.UNIT_VALUE,"
                                       + "  A.TRANS_PR "
                                       + "  from UTS.HOLDER_LEDGER A "
                                + " INNER JOIN UTS.HOLDER_REG B "
                                + " ON A.HOLDER_NO = B.HOLDER_NO "
                                + " where "
                                + " (A.trans_dt between to_date('" + Start_date + "', 'yyyy/mm/dd') "
                                + " and to_date('" + End_date + "', 'yyyy/mm/dd')) "
                                + " AND A.FUND_ID IN ('05', '01', '02', '03', '04', '06', '07') AND B.HOLDER_CLS IN ('EB','EN','ER','ES','EZ','EP')"
                                + " AND NOT A.ACC_TYPE = 'SW'"
                                + " AND  A.TRANS_TYPE = 'SA'"
                                + " AND B.BIRTH_DT > to_date('" + BirthDateupdate + "', 'yyyy/mm/dd') " //adjust only display active member for historical 
                                + " ORDER BY A.FUND_ID, A.TRANS_DT,A.HOLDER_NO ASC";
                    string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                    using (OracleCommand comm = new OracleCommand(mysql, conn))
                    {
                        using (OracleDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();

                                while (rdr.Read())
                                {
                                    string Member_EPF_No = rdr[1].ToString();

                                    string IPD_Fund_Code = rdr[0].ToString();
                                    string IPD_Member_Acc_No = rdr[2].ToString();
                                    // DateTime dt = Convert.ToDateTime(rdr[3].ToString());
                                    // string Transfer_Date = dt.ToString("yyyy-MM-dd");

                                    string Transaction_Code = rdr[5].ToString();
                                    string IPD_Unique_Transaction_ID = rdr[4].ToString();
                                    string Reversed_Transaction_ID = "";
                                    string Actual_Transferred_From_EPF_RM = rdr[8].ToString();
                                    string Service_Charge_RM = rdr[9].ToString();

                                    string GST_RM = rdr[10].ToString();
                                    string Actual_Amount_Invested_RM = rdr[11].ToString();
                                    string Transaction_Price_RM = rdr[12].ToString();
                                    string Units_Created = rdr[7].ToString();
                                    DateTime now = Convert.ToDateTime(rdr[3].ToString());
                                    //var startDate = new DateTime(now.Year, now.Month, 1);
                                    // var endDate = startDate.AddMonths(1).AddDays(-1);
                                    string Transfer_Date = now.ToString("yyyy-MM-dd");

                                    DateTime dt1 = Convert.ToDateTime(rdr[3].ToString());
                                    DateTime now1 = dt1;
                                    var startDate1 = new DateTime(now1.Year, now1.Month, 1);
                                    var endDate1 = startDate1.AddMonths(1).AddDays(-1);
                                    string Effective_Date = endDate1.ToString("yyyy-MM-dd");

                                    // for Checking 
                                    char Check_reversed = IPD_Unique_Transaction_ID[IPD_Unique_Transaction_ID.Length - 1];
                                    if (Check_reversed == 'X')
                                    {
                                        if (Transaction_Code == "SA")
                                        {
                                            Transaction_Code = "XS";
                                            Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                            // IPD_Unique_Transaction_ID = "";
                                            Actual_Transferred_From_EPF_RM = "-" + (Actual_Transferred_From_EPF_RM);
                                            Service_Charge_RM = "-" + Service_Charge_RM;
                                            Actual_Amount_Invested_RM = "-" + Actual_Amount_Invested_RM;
                                            Transaction_Price_RM = "-" + Transaction_Price_RM;
                                            Units_Created = "-" + Units_Created;
                                            GST_RM = "-" + GST_RM;
                                        }
                                    }

                                    if (Transaction_Code == "SA")
                                    {
                                        Transaction_Code = "NS";
                                    }





                                    string Query =
                                       "Insert into utmc_transferred(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Transfer_Date,Transaction_Code,IPD_Unique_Transaction_ID,Reversed_Transaction_ID,Actual_Transferred_From_EPF_RM,Service_Charge_RM,GST_RM,Actual_Amount_Invested_RM,Transaction_Price_RM,Units_Created,Effective_Date,report_date) "
                                     + "values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + IPD_Member_Acc_No + "','" + Transfer_Date + "','" + Transaction_Code + "','" + IPD_Unique_Transaction_ID + "','" + Reversed_Transaction_ID + "','" + Actual_Transferred_From_EPF_RM + "','" + Service_Charge_RM + "','" + GST_RM + "','" + Actual_Amount_Invested_RM + "','" + Transaction_Price_RM + "','" + Units_Created + "','" + Effective_Date + "','" + Effective_Date + "')";

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        Log.Write("UTMC_013 : " + IPD_Fund_Code + IPD_Fund_Code + IPD_Member_Acc_No + "Inserted", End_date);
                                        mysql_comm.ExecuteNonQuery();
                                    }
                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void Process_UTMC12()
        {
            DateTime da = DateTime.Now;
            string Last_NAV_Working_Date = "";

            try
            {
                string ConnectionString = GetOracleConnection();
                string Sqlconnstring = GetSqlConnection();
                //string Working_Day_Count = "0";
                Int16 WorkingCount = 0;
                decimal Sumof_Daily_NAV = 0.00M;
                decimal Daily_NAV_RPT11 = 0.00M;
                decimal Daily_NAV = 0.00M;
                string Daily_NAV_Date = "";

                using (MySqlConnection conn = new MySqlConnection(Sqlconnstring))
                {
                    conn.Open();
                    string mysql = "select sum(Daily_NAV_epf),Daily_NAV_Date from utmc_daily_nav_fund "
                                    + "where Daily_NAV_Date between '" + Start_date + "' and '" + End_date + "'"
                                   + " and IPD_Fund_Code !='02' "        // 02 update                                    
                                    + " group by Daily_NAV_Date";
                    string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                    using (MySqlCommand comm = new MySqlCommand(mysql, conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();

                                while (rdr.Read())
                                {

                                    Daily_NAV = Convert.ToDecimal(rdr[0].ToString());
                                    Daily_NAV = decimal.Round(Daily_NAV, 2);
                                    string Service_Charge_Percent = "0.00";
                                    string Service_Charge_RM = "0.00";
                                    DateTime dt = Convert.ToDateTime(rdr[1].ToString());
                                    Daily_NAV_Date = dt.ToString("yyyy-MM-dd");
                                    string FindDay = dt.DayOfWeek.ToString().Trim();
                                    DateTime now1 = dt;
                                    var startDate1 = new DateTime(now1.Year, now1.Month, 1);
                                    var endDate1 = startDate1.AddMonths(1).AddDays(-1);
                                    string Report_Date = endDate1.ToString("yyyy-MM-dd");
                                    string workingDayStatus = "W";


                                    if ((FindDay == "Saturday") || (FindDay == "Sunday"))
                                    {
                                        workingDayStatus = "H";
                                        // Working_Day_Count = WorkingCount.ToString();


                                    }
                                    else
                                    {
                                        // check holiday     
                                        using (MySqlConnection conn2 = new MySqlConnection(Sqlconnstring))
                                        {
                                            conn2.Open();
                                            string Query = "SELECT * FROM holiday WHERE datediff('" + Daily_NAV_Date + "', holiday_date)= 0";
                                            if (FindDay == "Monday")
                                            {

                                                // Check Holiday for Sunday

                                                DateTime Daily_NAV_Date1 = dt.AddDays(-1);
                                                string Daily_NAV_Date2 = Daily_NAV_Date1.ToString("yyyy-MM-dd");
                                                Query = "SELECT * FROM holiday WHERE datediff('" + Daily_NAV_Date + "', holiday_date)= 0 AND datediff('" + Daily_NAV_Date + "', holiday_date)= 0  or datediff('" + Daily_NAV_Date2 + "', holiday_date)= 0";
                                            }

                                            using (MySqlCommand comm1 = new MySqlCommand(Query, conn2))
                                            {
                                                //conn1.Open();
                                                using (MySqlDataReader rdr1 = comm1.ExecuteReader())
                                                {
                                                    if (rdr1.HasRows)
                                                    {
                                                        workingDayStatus = "H";
                                                        //    Working_Day_Count = WorkingCount.ToString();
                                                    }
                                                    else
                                                    {
                                                        workingDayStatus = "W";
                                                        WorkingCount++;

                                                        //  Working_Day_Count = WorkingCount.ToString();
                                                        Sumof_Daily_NAV = Sumof_Daily_NAV + Daily_NAV;
                                                    }


                                                    conn2.Close();
                                                }

                                            }

                                        }

                                    }


                                    if (workingDayStatus == "W")
                                    {
                                        Last_NAV_Working_Date = Daily_NAV_Date;
                                    }


                                    string Query2 =
                                      "INSERT INTO utmc_daily_nav(EPF_IPD_Code, Daily_NAV_Date, Daily_NAV, Service_Charge_Percent, Service_Charge_RM, Working_Day_Status, Working_Day_Count, Report_Date)"
                                    + " VALUES ('" + EPF_IPD_Code + "','" + Daily_NAV_Date + "','" + Daily_NAV + "','" + Service_Charge_Percent + "','" + Service_Charge_RM + "','" + workingDayStatus + "'," + WorkingCount + ",'" + Report_Date + "')";

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query2, mysql_conn))
                                    {
                                        mysql_comm.ExecuteNonQuery();
                                        Log.Write("UTMC_012 : " + EPF_IPD_Code + Daily_NAV + Daily_NAV_Date + "Inserted", End_date);
                                    }


                                }



                                rdr.Close();
                                mysql_conn.Close();
                            }





                            using (MySqlConnection conn3 = new MySqlConnection(Sqlconnstring))
                            {
                                conn3.Open();
                      
                                /* Adjustment to tally with EPF decimal calculation for last day of the month*/
                                /* gordon */
                                string tmpRpt11Query =
                                    "select sum(Market_Price_NAV*Net_Cumulative_Closing_Balance_Units) from utmc_compositional_investment where Report_Date = '" + Daily_NAV_Date + "' and IPD_Fund_Code !='02' ";    // 02 update


                                  /* gordon */
                                using (MySqlCommand Rpt11_Comm = new MySqlCommand(tmpRpt11Query, conn3))
                                {
                                    using (MySqlDataReader rdrRpt11 = Rpt11_Comm.ExecuteReader())
                                    {
                                        rdrRpt11.Read();
                                        Daily_NAV_RPT11 = Convert.ToDecimal(rdrRpt11[0].ToString());
                                    }
                                }

                              

                                // adjustment to last working day

                                //string tmpRpt12LastWorkingDateQuery = "SELECT max(Daily_NAV_Date) FROM eppa_wbr1.utmc_daily_nav "
                                //        + "where Report_Date = '" + Daily_NAV_Date + "' and Working_Day_Status = 'W' group by Report_Date ; ";



                                 

                                // using (MySqlCommand Rpt12_Comm = new MySqlCommand(tmpRpt12LastWorkingDateQuery, conn3))
                                //{
                                //    using (MySqlDataReader rdrRpt12 = Rpt12_Comm.ExecuteReader())
                                //    {
                                //        rdrRpt12.Read();
                                //        Last_NAV_Working_Date = rdrRpt12[0].ToString();
                                //    }
                                //}
                                 //

                                Sumof_Daily_NAV = Sumof_Daily_NAV - Daily_NAV + Daily_NAV_RPT11;

                                /* Adjustment to tally with EPF decimal calculation for last day of the month*/


                                decimal ServiceChargepercantage = 0.125M;

                                decimal ServiceCharge = Sumof_Daily_NAV / Convert.ToDecimal(WorkingCount) * ServiceChargepercantage / 12 / 100;
                                ServiceCharge = decimal.Round(ServiceCharge, 2);
                                
                                string Query3 = "update  utmc_daily_nav set Daily_NAV='" + Daily_NAV_RPT11 + "', Service_Charge_Percent='" + ServiceChargepercantage + "', Service_Charge_RM='" + ServiceCharge + "' "
                                                 + " where Daily_NAV_Date='" + Last_NAV_Working_Date + "' ";
          
                                
                                using (MySqlCommand mysql_comm = new MySqlCommand(Query3, conn3))
                                {

                                    mysql_comm.ExecuteNonQuery();
                                }
                                conn3.Close();

                            }
                            conn.Close();
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
            }

        }

        protected void UPDATEUTMC012()
        {
            try
            {
                string ConnectionString = GetOracleConnection();
                string Sqlconnstring = GetSqlConnection();

                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    string mysql = "SELECT (s1.CUR_UNIT_HLDG),s1.TRANS_DT"

                                   + "  from UTS.HOLDER_LEDGER s1 inner join"
                                   + "  UTS.HOLDER_REG s3 on"
                                   + "  s1.HOLDER_NO = s3.HOLDER_NO"
                                   + "  INNER JOIN"
                                   + "  ("
                                   + "   select max(SORT_SEQ) SORT_SEQ, FUND_ID,"
                                   + "      HOLDER_NO"
                                   + "   from UTS.HOLDER_LEDGER"
                                   + "   WHERE TRANS_DT < to_date('2016/06/01', 'yyyy/mm/dd')"
                                   + "   group by HOLDER_NO,FUND_ID"
                                   + "  ) s2"
                                   + "    on s1.HOLDER_NO = s2.HOLDER_NO"
                                   + "   and s1.SORT_SEQ = s2.SORT_SEQ "
                                   + "   where "
                                   + "   s1.CUR_UNIT_HLDG > 0"
                                   + "   AND s3.HOLDER_CLS IN ('EB','EN','ER','ES','EZ','EP')"
                                   + "   AND s3.BIRTH_DT > to_date('1961/04/01', 'yyyy/mm/dd')"
                                   + "   AND S1.FUND_ID IN ('01','02','03','04','05','06','07')"
                                   + "   AND (s1.trans_dt between to_date('2016/05/01', 'yyyy/mm/dd')"
                                    + "  and to_date('2016/05/31', 'yyyy/mm/dd'))"

                                   + "  ORDER BY S1.TRANS_DT";

                    using (OracleCommand comm = new OracleCommand(mysql, conn))
                    {
                        using (OracleDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();
                                string Working_Day_Count = "0";
                                Int16 WorkingCount = 0;
                                while (rdr.Read())
                                {
                                    string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                                    string Daily_NAV = rdr[0].ToString();
                                    string Service_Charge_Percent = "0.00";
                                    string Service_Charge_RM = "0.00";



                                    DateTime dt = Convert.ToDateTime(rdr[1].ToString());
                                    string Daily_NAV_Date = dt.ToString("yyyy-MM-dd");
                                    string FindDay = dt.DayOfWeek.ToString().Trim();
                                    DateTime now1 = dt;
                                    var startDate1 = new DateTime(now1.Year, now1.Month, 1);
                                    var endDate1 = startDate1.AddMonths(1).AddDays(-1);
                                    string Report_Date = endDate1.ToString("yyyy-MM-dd");
                                    string workingDayStatus = "W";

                                    if ((FindDay == "Saturday") || (FindDay == "Sunday"))
                                    {
                                        workingDayStatus = "H";
                                        Working_Day_Count = WorkingCount.ToString();
                                    }
                                    else
                                    {
                                        workingDayStatus = "W";
                                        WorkingCount++;
                                        Working_Day_Count = WorkingCount.ToString();

                                    }


                                    string Query =
                                      "INSERT INTO utmc_daily_nav(EPF_IPD_Code, Daily_NAV_Date, Daily_NAV, Service_Charge_Percent, Service_Charge_RM, Working_Day_Status, Working_Day_Count, Report_Date)"
                                    + " VALUES ('" + EPF_IPD_Code + "','" + Daily_NAV_Date + "','" + Daily_NAV + "','" + Service_Charge_Percent + "','" + Service_Charge_RM + "','" + workingDayStatus + "','" + Working_Day_Count + "','" + Report_Date + "')";

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        mysql_comm.ExecuteNonQuery();
                                    }
                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }


            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }

        private void Process_UTMC03()
        {
            try
            {
                string ConnectionString = GetOracleConnection();
                string Sqlconnstring = GetSqlConnection();

                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    string mysql = " SELECT A1.HOLDER_NO,  A1.ID_NO AS NRIC,  A1.ID_NO_2 AS EPF_NO, "
                                 + " to_date(A1.BIRTH_DT),   A1.SEX,    A1.HOLDER_CLS,   A1.HOLDER_STATUS, A1.EPF_I_STATUS, A1.EPF_I_EFF_DT FROM  "
                                 + " UTS.HOLDER_REG A1 WHERE A1.HOLDER_NO IN  "
                                 + " ( "
                                 + "SELECT s3.HOLDER_NO   "
                                 + "from UTS.HOLDER_LEDGER s1 inner join UTS.HOLDER_REG s3 on s1.HOLDER_NO = s3.HOLDER_NO   "
                                 + "INNER JOIN  "
                                 + "  (   select max(SORT_SEQ) SORT_SEQ, MAX(TRANS_DT) TRANS_DT, FUND_ID,    HOLDER_NO   "
                                 + "  from UTS.HOLDER_LEDGER  "
                                 + "  WHERE TRANS_DT < to_date('" + TransDateUpdate + "', 'yyyy/mm/dd') "
                                 + "  group by HOLDER_NO,FUND_ID   "
                                 + "  ) s2  "
                                 + "on s1.HOLDER_NO = s2.HOLDER_NO  and s1.SORT_SEQ = s2.SORT_SEQ "  // adjustment .. AND s1.TRANS_DT = s2.TRANS_DT  "
                                 + "where   s1.CUR_UNIT_HLDG > 0 AND s3.HOLDER_CLS IN ('EB','EN','ER','ES','EZ','EP')  "
                                 + "AND S3.BIRTH_DT > to_date('" + BirthDateupdate + "', 'yyyy/mm/dd')  "
                                 + "AND S1.FUND_ID IN ('01','02','03','04','05','06','07') AND s1.TRANS_DT < to_date('" + TransDateUpdate + "', 'yyyy/mm/dd') "
                                  + "  AND NOT S1.HOLDER_NO IN ('9504', '19765')  "  //adjusment
                                 + "GROUP BY S3.HOLDER_NO) ";
                    string Code = Read_IPD_info_for_utmc01();
                    using (OracleCommand comm = new OracleCommand(mysql, conn))
                    {
                        // MessageBox.Show("Oracle Wait for  Execute");
                        using (OracleDataReader rdr = comm.ExecuteReader())
                        {
                            //  MessageBox.Show("Oracle read done");
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();
                                StringBuilder sb = new StringBuilder();
                                while (rdr.Read())
                                {

                                    string Passport_No = rdr[1].ToString();
                                    string EPF_No = rdr[2].ToString();
                                    string NIC = rdr[7].ToString();

                                    string NIC_Date = rdr[8].ToString();

                                    if (NIC_Date == "")
                                    { NIC_Date = "1900-01-01"; }

                                    DateTime dtEFF = Convert.ToDateTime(NIC_Date);
                                    NIC_Date = dtEFF.ToString("yyyy-MM-dd");



                                    DateTime dt = Convert.ToDateTime(rdr[3].ToString());
                                    string BirthDate = dt.ToString("yyyy-MM-dd");
                                    string Gender = rdr[4].ToString();

                                    DateTime now = DateTime.Now;
                                    var startDate = new DateTime(now.Year, now.Month, 1);
                                    var endDate = startDate.AddMonths(1).AddDays(-1);
                                    string EndofMonth = endDate.ToString("yyyy-MM-dd");
                                    // Effective

                                    DateTime dt1 = Convert.ToDateTime(rdr[3].ToString());
                                    string Release_Date = dt1.ToString("yyyy-MM-dd");
                                    string year = DateTime.Now.Year.ToString();
                                    Release_Date = Release_Date.Remove(0, 4).Insert(0, year);

                                    

                                    string Query = "";


                                    if (NIC_Date.Substring(0, 4) == "1990")
                                    {
                                        Query =
                                          "Insert into utmc_member_information(Code,Passport_No,EPF_No,NIC,Birthdate,Gender, Effective_Date_Of_Report,report_date) "
                                         + " values('" + Code + "', '" + Passport_No + "','" + EPF_No + "','" + NIC + "','" + BirthDate + "','" + Gender + "','" + End_date + "','" + End_date + "')";


                                    }
                                    else
                                    {

                                        Query =
                                      "Insert into utmc_member_information(Code,Passport_No,EPF_No,NIC,NIC_Date,Birthdate,Gender, Effective_Date_Of_Report,report_date) "
                                     + " values('" + Code + "', '" + Passport_No + "','" + EPF_No + "','" + NIC + "','" + NIC_Date + "','" + BirthDate + "','" + Gender + "','" + End_date + "','" + End_date + "')";


                                    }

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        mysql_comm.ExecuteNonQuery();
                                        Log.Write("UTMC_03 : " + Passport_No + EPF_No + BirthDate + "Inserted", End_date);
                                    }
                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
            }



        }

        private void Process_UTMC10()
        {

            try
            {
                string ConnectionString = GetOracleConnection();
                string Sqlconnstring = GetSqlConnection();

                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    string mysql = " select  A.FUND_ID, "
                                    + " B.ID_NO_2 AS EPF_NO, "
                                    + " A.HOLDER_NO, "
                                    + " A.TRANS_DT, "
                                    + " A.TRANS_NO, "
                                    + " A.TRANS_TYPE, "
                                    + " A.ACC_TYPE, "
                                    + " A.TRANS_UNITS, "
                                    + " A.TRANS_AMT,  "
                                    + " A.FEE_AMT, "
                                    + " A.GST_AMT,  "
                                    + " A.UNIT_VALUE, "
                                    + " (A.GST_AMT+ A.UNIT_VALUE) AS GAIN, C.RCPT_DT AS SETTLEMENT_DT,A.HLDR_AVE_COST, "
                                     + " A.SORT_SEQ "
                                    + " from UTS.HOLDER_LEDGER A "
                                    + " INNER JOIN UTS.HOLDER_REG B "
                                    + " ON A.HOLDER_NO  = B.HOLDER_NO "
                                    + " LEFT JOIN UTS.UTSTRANS C"
                                    + " ON A.TRANS_NO = C.TRANS_NO "
                                    + " AND A.TRANS_DT = C.TRANS_DT "
                                    + " where "
                                    + "(A.trans_dt between to_date('" + Start_date + "', 'yyyy/mm/dd')"
                                    + " and to_date('" + End_date + "', 'yyyy/mm/dd'))"
                                    + " AND A.FUND_ID IN ('05', '01', '02', '03', '04', '06', '07')"
                                    + " AND B.HOLDER_CLS IN ('EB','EN','ER','ES','EZ','EP')"
                                    + " AND B.BIRTH_DT >= to_date('" + BirthDateupdate + "', 'yyyy/mm/dd') " //adjust only display active member for historical
                                    + " AND NOT A.TRANS_TYPE IN ('UC', 'CO')"
                                    + " ORDER BY A.TRANS_TYPE, ACC_TYPE, A.FUND_ID, A.TRANS_DT ASC ";
                    string EPF_IPD_Code = Read_IPD_info_for_utmc01();


                    using (OracleCommand comm = new OracleCommand(mysql, conn))
                    {
                        using (OracleDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();
                                while (rdr.Read())
                                {
                                    decimal Cost_RM = 0.00M;
                                    string IPD_Fund_Code = rdr[0].ToString();
                                    string EPF_No = rdr[1].ToString();
                                    string IPD_Member_Acc_No = rdr[2].ToString();
                                    DateTime dt = Convert.ToDateTime(rdr[3].ToString());

                                    var startDate = new DateTime(dt.Year, dt.Month, 1);
                                    var endDate = startDate.AddMonths(1).AddDays(-1);
                                    string LastDay = endDate.ToString("yyyy-MM-dd");

                                    string Effective_Date = dt.ToString("yyyy-MM-dd");
                                    //string Date_Of_Transaction = rdr[7].ToString();                                 
                                    string Settlementdate = rdr[13].ToString();
                                    if (Settlementdate == "")
                                    {
                                        Settlementdate = Effective_Date;
                                    }
                                    else
                                    {
                                        DateTime dt1 = Convert.ToDateTime(rdr[13].ToString());
                                        Settlementdate = dt1.ToString("yyyy-MM-dd");
                                    }
                                    // DateTime dt1 = Convert.ToDateTime(rdr[13].ToString());
                                    // string Settlementdate = dt1.ToString("yyyy-MM-dd");

                                    string IPD_Unique_Transaction_ID = rdr[4].ToString();
                                    string Transaction_Code = rdr[5].ToString().Trim();
                                    string AccountType = rdr[6].ToString().Trim();
                                    string Reversed_Transaction_ID = "";
                                    decimal AverageCost = Convert.ToDecimal(rdr[14].ToString());

                                    string Sort_SEQ = rdr[15].ToString();

                                    decimal Units = Math.Abs(Convert.ToDecimal(rdr[7].ToString()));
                                    decimal Gross_Amount_RM = Convert.ToDecimal(rdr[8].ToString());
                                    decimal Net_Amount_RM = Convert.ToDecimal(rdr[11].ToString());
                                    decimal Fees_RM = Convert.ToDecimal(rdr[9].ToString());
                                    decimal GST_RM = Convert.ToDecimal(rdr[10].ToString());
                                    decimal Proceeds_RM = 0.00M;
                                    decimal Realised_Gain_Loss = 0.00M;
                                    AverageCost = decimal.Round(AverageCost, 4);
                                    char Check_reversed = IPD_Unique_Transaction_ID[IPD_Unique_Transaction_ID.Length - 1];

                                    if (Check_reversed == 'X')
                                    {
                                        if (Transaction_Code == "SA")
                                        {

                                            if (AccountType == "SW")
                                            {
                                                Transaction_Code = "XI";
                                                Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                            }
                                            else
                                            {
                                                Transaction_Code = "XS";
                                                Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                            }



                                        }
                                        else if (Transaction_Code == "RD")
                                        {

                                            if (AccountType == "SW")
                                            {
                                                Transaction_Code = "XO";
                                                Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                            }
                                            else
                                            {
                                                Transaction_Code = "XR";
                                                Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                            }
                                        }

                                        else if (Transaction_Code == "DD")
                                        {

                                            if (Units == 0)
                                            {
                                                Transaction_Code = "XD";
                                                Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                            }
                                            else
                                            {
                                                Transaction_Code = "XV";
                                                Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                            }

                                        }
                                    }





                                    if ((Transaction_Code == "SA") || (Transaction_Code == "DD"))
                                    {

                                    }
                                    if ((Transaction_Code == "SA") && (AccountType == "SW"))
                                    {
                                        Transaction_Code = "SI";

                                    }
                                    if ((Transaction_Code == "RD") && (AccountType == "SW"))
                                    {
                                        Transaction_Code = "SO";
                                        //Gross_Amount_RM = 0.00M;                  // as per request SO
                                        Net_Amount_RM = 0.00M;

                                    }


                                    if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO") || (Transaction_Code == "XR") || (Transaction_Code == "XD") || (Transaction_Code == "XO") || (Transaction_Code == "XV"))
                                    {
                                        Proceeds_RM = Gross_Amount_RM;
                                        // Cost_RM = Units * AverageCost;
                                        Realised_Gain_Loss = Proceeds_RM - Cost_RM;
                                        // Cost_RM = decimal.Round(Cost_RM, 2);
                                        Proceeds_RM = decimal.Round(Proceeds_RM, 2);
                                        Realised_Gain_Loss = decimal.Round(Realised_Gain_Loss, 2);


                                    }

                                    //filter by sign requirement 

                                    if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO")
                                        || (Transaction_Code == "XS") || (Transaction_Code == "XC") || (Transaction_Code == "XD")
                                        || (Transaction_Code == "XV") || (Transaction_Code == "UX") || (Transaction_Code == "XI")
                                        || (Transaction_Code == "RO") || (Transaction_Code == "RI") || (Transaction_Code == "HO") || (Transaction_Code == "XO"))
                                    {

                                        if (!(Net_Amount_RM == 0))
                                        {
                                            Net_Amount_RM = -Net_Amount_RM;

                                        }
                                        if (!(Fees_RM == 0))
                                        {
                                            Fees_RM = -Fees_RM;

                                        }
                                        if (!(GST_RM == 0))
                                        {
                                            GST_RM = -GST_RM;

                                        }
                                        Gross_Amount_RM = -Gross_Amount_RM;

                                        if (!(Transaction_Code == "XO"))
                                        {
                                            Units = -Units;
                                        }
                                        if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO"))
                                        {
                                            Proceeds_RM = -Proceeds_RM;
                                        }


                                    }
                                    ////////// hard code  adjustment Start


                                    if (Transaction_Code == "SA")
                                    {
                                        Transaction_Code = "NS";
                                        // final  aaa ---
                                        Cost_RM = Gross_Amount_RM;
                                        // final  aaa ---

                                    }
                                    if (Transaction_Code == "SI")
                                    {
                                        // final  aaa ---
                                        Cost_RM = Gross_Amount_RM;
                                        // final  aaa ---


                                    }
                                    if (Transaction_Code == "DD")
                                    {
                                        Transaction_Code = "DV";
                                        Realised_Gain_Loss = 0.00M;
                                    }




                                    if (Transaction_Code == "XD")
                                    {
                                        Realised_Gain_Loss = 0.00M;
                                        // Net_Amount_RM = Gross_Amount_RM;
                                        Proceeds_RM = 0.00M;

                                    }

                                    if (Transaction_Code == "XV")
                                    {
                                        Realised_Gain_Loss = 0.00M;

                                       // Net_Amount_RM = Gross_Amount_RM;
                                        Proceeds_RM = 0.00M;
                                    }




                                    if (Transaction_Code == "RD")
                                    {
                                        Transaction_Code = "RE";
                                        Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ);


                                        Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);

                                        // final  aaa ---
                                        Gross_Amount_RM = Proceeds_RM;
                                        Net_Amount_RM = Proceeds_RM;
                                        // final  aaa ---

                                        Cost_RM = -Cost_RM;
                                    }
                                    if (Transaction_Code == "TR")
                                    {
                                        Transaction_Code = "TO";
                                        Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ);
                                        Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);
                                        // final  aaa ---
                                        Gross_Amount_RM = Proceeds_RM;
                                        Net_Amount_RM = Proceeds_RM;
                                        // final  aaa ---
                                    }


                                    if (Transaction_Code == "SO" || Transaction_Code == "XO")
                                    {
                                        Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ);
                                        Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);
                                        // final  aaa ---
                                        Gross_Amount_RM = Proceeds_RM;
                                        Net_Amount_RM = Proceeds_RM;
                                        // final  aaa ---
                                        Cost_RM = -Cost_RM;
                                    }

                                    if (Transaction_Code == "XO")
                                    {
                                        Cost_RM = -Cost_RM;
                                        Realised_Gain_Loss = -Realised_Gain_Loss;
                                        //Units = Units;

                                    }


                                    // FILTERED 0 UNIT DIVIDEND 
                                    Boolean bolFILTER = true;
                                    if (Transaction_Code == "DV")
                                    {
                                        if (Units == 0)
                                        {
                                            bolFILTER = false;
                                        }

                                    }
                                    
                                    if (Transaction_Code == "XS")
                                    
                                    {
                                        Cost_RM = Gross_Amount_RM;
                                       
                                    }


                                    if (Transaction_Code == "XI" || Transaction_Code == "XR")
                                    {
                                        Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ);

                                        if (Transaction_Code == "XI")
                                        {
                                            Cost_RM = Gross_Amount_RM;
                                        }

                                        if (Transaction_Code == "XR")
                                        {
                                            Realised_Gain_Loss = -(Proceeds_RM - Cost_RM);
                                            Realised_Gain_Loss = decimal.Round(Realised_Gain_Loss, 2);

                                        }

                                    }

                                    if (Transaction_Code == "DV" || Transaction_Code == "XV")
                                    {
                                        Net_Amount_RM = Gross_Amount_RM;
                                    }

                                       



                                    //  Log.Write(ex.ToString(), End_date);

                                    //////////////////////////////////////////////////////////////////////////////////////////// hard code  adjustment end
                                    string Query =
                                          "INSERT INTO utmc_compositional_transactions "
                                          + " (EPF_IPD_Code, IPD_Fund_Code, Member_EPF_No, IPD_Member_Acc_No, Effective_Date, Date_Of_Transaction, Date_Of_Settlement, IPD_Unique_Transaction_ID, Transaction_Code, Reversed_Transaction_ID, Units, "
                                          + " Gross_Amount_RM, Net_Amount_RM, Fees_RM, GST_RM, Cost_RM, Proceeds_RM, Realised_Gain_Loss,report_date) VALUES "
                                          + "('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + EPF_No + "','" + IPD_Member_Acc_No + "','" + LastDay + "','" + Effective_Date + "','" + Settlementdate + "','" + IPD_Unique_Transaction_ID + "','" + Transaction_Code + "','" + Reversed_Transaction_ID + "','" + Units + "','" + Gross_Amount_RM + "','" + Net_Amount_RM + "','" + Fees_RM + "','" + GST_RM + "','" + Cost_RM + "','" + Proceeds_RM + "','" + Realised_Gain_Loss + "','" + End_date + "')";
                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        Log.Write("UTMC_010 : " + IPD_Fund_Code + IPD_Fund_Code + IPD_Member_Acc_No + "Inserted", End_date);

                                        if (bolFILTER == true)
                                        {
                                            mysql_comm.ExecuteNonQuery();
                                        }
                                    }
                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                Log.Write(ex.ToString(), End_date);
            }

            // Reversal TO --Murali


            string ReversalQuery= " Update utmc_compositional_transactions set "
+" Reversed_Transaction_ID=IPD_Unique_Transaction_ID,Transaction_Code='RO', units=ABS(units), "
+" Gross_Amount_RM=abs(Gross_Amount_RM),Net_Amount_RM=abs(Net_Amount_RM),Proceeds_RM=abs(Proceeds_RM),Cost_RM=abs(Cost_RM), "
+" Realised_Gain_Loss=abs(Cost_RM)-abs(Proceeds_RM) where  Transaction_Code ='TO' and IPD_Unique_Transaction_ID like '%X' and report_date='"+End_date+"' " ;

            SQL_Execute_NonQuery(ReversalQuery);

        }




        protected decimal Get_Redumption_Cost_UTMC10(string Holder_No, string Fund_ID, string Sort_Seq)
        {

            string Sqlconnstring = GetOracleConnection();
            try
            {

                decimal Actual_transfer_value = 0.0000M;
                decimal Book_value = 0.0000M;
                decimal Unit_Holding_value = 0.0000M;
                string Trans_Type = "";
                decimal Total_Redemption_cost = 0.0000M;
                decimal UnitCostRD = 0.0000M;
                decimal UnitCostRDfinal = 0.0000M;

                string Mysql = "SELECT TRANS_NO, TRANS_TYPE, ACC_TYPE, ENTRY_DT, TRANS_DT,"
                                   + " TRANS_UNITS, TRANS_AMT AS BIG,"
                                   + " (TRANS_UNITS * TRANS_PR) AS SMALL, CUR_UNIT_HLDG, CUM_VALUE_HLDG, SORT_SEQ"
                                   + " FROM UTS.HOLDER_LEDGER "
                                   + " WHERE "
                                   + " HOLDER_NO='" + Holder_No + "' AND "
                                   + " FUND_ID='" + Fund_ID + "' AND "
                                   + " trans_type IN ('SA','RD','DD','BI','UC', 'TR') "
                    // + " trans_type IN ('SA','RD','DD','TR') "
                                   + " AND  TRANS_DT <= to_date('" + End_date + "', 'yyyy/mm/dd') "
                                   + " ORDER BY TRANS_DT ASC,SORT_SEQ ASC, TRANS_TYPE DESC ";



                using (OracleConnection mysql_conn = new OracleConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (OracleCommand comm = new OracleCommand(Mysql, mysql_conn))
                    {
                        int Count = 0; string[] numb;
                        using (OracleDataReader rdr1 = comm.ExecuteReader())
                        {

                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr1);

                            numb = new string[dataTable.Rows.Count];

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                numb[Count++] = row[0].ToString();
                            }
                            Array.Resize(ref numb, Count);

                            Count = 0;
                            string Trans_String;

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                try
                                {

                                    Trans_String = row[0].ToString();
                                    bool TTT = false;


                                    // check reversal
                                    for (int i = Count; i < numb.Length; i++)
                                    {
                                        if (Trans_String + "X" == numb[i].ToString())
                                        {
                                            // rdr.NextResult();
                                            TTT = true;
                                            break;

                                        }

                                        char Check_reversed = Trans_String[Trans_String.Length - 1];

                                        if (Check_reversed == 'X')
                                        {
                                            TTT = true;

                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                                            break;
                                        }

                                    }

                                    Count++;

                                    if (TTT == false)
                                    {


                                        Trans_Type = row[1].ToString().Trim();

                                        if (Trans_Type == "SA")
                                        {
                                            string Trans_no = row[0].ToString();
                                            char Check_reversed = Trans_no[Trans_no.Length - 1];

                                            if (Check_reversed == 'X')
                                            {
                                                Trans_no = Trans_no.TrimEnd(Trans_no[Trans_no.Length - 1]);
                                                Actual_transfer_value -= Convert.ToDecimal(row[6].ToString());
                                                Book_value -= Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                            }
                                            else
                                            {
                                                Actual_transfer_value += Convert.ToDecimal(row[6].ToString());
                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                            }

                                        }
                                        if ((Trans_Type == "DD") || (Trans_Type == "BI") || (Trans_Type == "SA") || (Trans_Type == "UC"))
                                        {
                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                        }
                                        if (Trans_Type == "RD" || Trans_Type == "TR")
                                        {



                                            Total_Redemption_cost = Convert.ToDecimal(row[8].ToString());
                                            Total_Redemption_cost = decimal.Round(Total_Redemption_cost, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);


                                            /* FINAL AAAA OLD COST
                                            UnitCostRDfinal = (Book_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redemption_cost);
                                                                * UnitCostRD);
                                            /* FINAL AAAA OLD COST */
                                             
                                            UnitCostRDfinal = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redemption_cost);
                                                                * UnitCostRD);


                                            UnitCostRDfinal = decimal.Round(UnitCostRDfinal, 2);


                                            if (Convert.ToDecimal(Total_Redemption_cost) > 0)
                                            {



                                                Actual_transfer_value = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                                           * Total_Redemption_cost);
                                                Book_value = (Book_value / Convert.ToDecimal(Unit_Holding_value)
                                                                          * Total_Redemption_cost);



                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                            }
                                            else
                                            {
                                                Actual_transfer_value = 0.0000M;
                                                Book_value = 0.0000M;
                                                Unit_Holding_value = 0.0000M;
                                            }

                                            // if (Trans_Type == "TR")
                                            // {
                                            //   UnitCostRDfinal = 0.00M;
                                            // }

                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            if (Sort_Seq == row[10].ToString())
                                            {
                                                break;
                                            }

                                        }

                                    }

                                }
                                catch (Exception ex)
                                {
                                    Log.Write(ex.ToString(), End_date);
                                }

                            }

                            return UnitCostRDfinal;
                            /*
                      
                            ////////////////////////////////////////////////////////////////////////////////////////////// hard code  adjustment end

                            Cost_update_for_UTMC10(Holder_No, Fund_ID, UnitCostRDfinal, Realised_Gain_Loss);
                            // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Actual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                            //rdr.Close();
                            mysql_conn.Close();
                            */

                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
                return 0.00M;

            }
        }

        protected decimal Get_Redumption_NEW_Cost_UTMC10(string Holder_No, string Fund_ID, string Sort_Seq)
        {

            string Sqlconnstring = GetOracleConnection();
            try
            {

                decimal Actual_transfer_value = 0.0000M;
                decimal Book_value = 0.0000M;
                decimal Unit_Holding_value = 0.0000M;
                string Trans_Type = "";
                decimal Total_Redemption_cost = 0.0000M;
                decimal UnitCostRD = 0.0000M;
                decimal UnitCostRDfinal = 0.0000M;

                string Mysql = "SELECT TRANS_NO, TRANS_TYPE, ACC_TYPE, ENTRY_DT, TRANS_DT,"
                                   + " TRANS_UNITS, TRANS_AMT AS BIG,"
                                   + " (TRANS_UNITS * TRANS_PR) AS SMALL, CUR_UNIT_HLDG, CUM_VALUE_HLDG, SORT_SEQ"
                                   + " FROM UTS.HOLDER_LEDGER "
                                   + " WHERE "
                                   + " HOLDER_NO='" + Holder_No + "' AND "
                                   + " FUND_ID='" + Fund_ID + "' AND "
                                   + " trans_type IN ('SA','RD','DD','BI','UC', 'TR') "
                                   // + " trans_type IN ('SA','RD','DD','TR') "
                                   + " AND  TRANS_DT <= to_date('" + End_date + "', 'yyyy/mm/dd') "
                                   + " ORDER BY TRANS_DT ASC,SORT_SEQ ASC, TRANS_TYPE DESC ";



                using (OracleConnection mysql_conn = new OracleConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (OracleCommand comm = new OracleCommand(Mysql, mysql_conn))
                    {
                        int Count = 0; string[] numb;
                        using (OracleDataReader rdr1 = comm.ExecuteReader())
                        {

                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr1);

                            numb = new string[dataTable.Rows.Count];

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                numb[Count++] = row[0].ToString();
                            }
                            Array.Resize(ref numb, Count);

                            Count = 0;
                            string Trans_String;

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                try
                                {

                                    Trans_String = row[0].ToString();
                                    bool TTT = false;


                                    // check reversal
                                    for (int i = Count; i < numb.Length; i++)
                                    {
                                        if (Trans_String + "X" == numb[i].ToString())
                                        {
                                            // rdr.NextResult();
                                            TTT = true;
                                            break;

                                        }

                                        char Check_reversed = Trans_String[Trans_String.Length - 1];

                                        if (Check_reversed == 'X')
                                        {
                                            TTT = true;

                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                                            break;
                                        }

                                    }

                                    Count++;

                                    if (TTT == false)
                                    {


                                        Trans_Type = row[1].ToString().Trim();

                                        if (Trans_Type == "SA")
                                        {
                                            string Trans_no = row[0].ToString();
                                            char Check_reversed = Trans_no[Trans_no.Length - 1];

                                            if (Check_reversed == 'X')
                                            {
                                                Trans_no = Trans_no.TrimEnd(Trans_no[Trans_no.Length - 1]);
                                                Actual_transfer_value -= Convert.ToDecimal(row[6].ToString());
                                                Book_value -= Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                            }
                                            else
                                            {
                                                Actual_transfer_value += Convert.ToDecimal(row[6].ToString());
                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                            }

                                        }
                            
                                        if (Trans_Type == "RD" || Trans_Type == "TR")
                                        {



                                            Total_Redemption_cost = Convert.ToDecimal(row[8].ToString());
                                            Total_Redemption_cost = decimal.Round(Total_Redemption_cost, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);



                                           // UnitCostRDfinal = (Book_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redemption_cost);
                                            //                    * UnitCostRD);

                                            UnitCostRDfinal = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                                         * UnitCostRD);
                                              
                                            UnitCostRDfinal = decimal.Round(UnitCostRDfinal, 2);


                                            if (Convert.ToDecimal(Total_Redemption_cost) > 0)
                                            {



                                                Actual_transfer_value = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                                           * Total_Redemption_cost);
                                                Book_value = (Book_value / Convert.ToDecimal(Unit_Holding_value)
                                                                          * Total_Redemption_cost);



                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                            }
                                            else
                                            {
                                                Actual_transfer_value = 0.0000M;
                                                Book_value = 0.0000M;
                                                Unit_Holding_value = 0.0000M;
                                            }

                                            // if (Trans_Type == "TR")
                                            // {
                                            //   UnitCostRDfinal = 0.00M;
                                            // }

                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            if (Sort_Seq == row[10].ToString())
                                            {
                                                break;
                                            }

                                        }

                                    }

                                }
                                catch (Exception ex)
                                {
                                    Log.Write(ex.ToString(), End_date);
                                }

                            }

                            return UnitCostRDfinal;
                            /*
                      
                            ////////////////////////////////////////////////////////////////////////////////////////////// hard code  adjustment end

                            Cost_update_for_UTMC10(Holder_No, Fund_ID, UnitCostRDfinal, Realised_Gain_Loss);
                            // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Actual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                            //rdr.Close();
                            mysql_conn.Close();
                            */

                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
                return 0.00M;

            }
        }

        private void Process_UTMC11()
        {
            DateTime dateTime = Convert.ToDateTime(Start_date);

            dateTime = dateTime.AddDays(-1);
            string PrevMonthDate = dateTime.ToString("yyyy-MM-dd");

            string ConnectionString = GetSqlConnection();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(ConnectionString))
                {
                    conn.Open();


                    string mysql = "select IPD_Fund_Code, Member_EPF_No , IPD_Member_Acc_No,max(Report_Date) FROM utmc_member_investment where "
                                      + " (Report_Date='" + End_date + "' "
                                      + " or  Report_Date ='" + PrevMonthDate + "') AND Units > 0"
                                      + " group by IPD_Member_Acc_No,IPD_Fund_Code order by IPD_Member_Acc_No";

                    using (MySqlCommand comm = new MySqlCommand(mysql, conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(ConnectionString))
                            {
                                string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                                mysql_conn.Open();
                                while (rdr.Read())
                                {
                                    string IPD_Fund_Code = rdr[0].ToString();
                                    string EPF_No = rdr[1].ToString();
                                    string IPD_Member_Acc_No = rdr[2].ToString();
                                    // Next
                                    string Next__Opening_Balance_Units = "0.00";          //1
                                    string Next_Opening_Balance_Cost_RM = "0.00";         //2                        

                                    // Current
                                    decimal Curr_Net_Cumulative_Closing_Balance_Units = 0.00M;    //1
                                    decimal Curr_Net_Cumulative_Closing_Balance_Cost_RM = 0.00M;   //2                                  
                                    decimal Curr_Market_Price_NAV = 0.00M;                     //
                                    decimal Curr_Unrealised_Gain_Loss_RM = 0.00M;              //


                                    /* gordon */

                                    /* gordon */

                                    string Query =
                                    "INSERT INTO utmc_compositional_investment(EPF_IPD_Code, IPD_Fund_Code, Member_EPF_No, IPD_Member_Acc_No, Effective_Date, Opening_Balance_Units, Opening_Balance_Cost_RM, Opening_Balance_Date, Net_Cumulative_Closing_Balance_Units,Net_Cumulative_Closing_Balance_Cost_RM,Net_Cumulative_Closing_Balance_Date,Market_Price_NAV,Market_Price_Effective_Date,Unrealised_Gain_Loss_RM,Report_Date,Report_Key)"
                                   + " VALUES ('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + EPF_No + "','" + IPD_Member_Acc_No + "','" + End_date + "','" + Next__Opening_Balance_Units + "','" + Next_Opening_Balance_Cost_RM + "','" + Start_date + "','" + Curr_Net_Cumulative_Closing_Balance_Units + "','" + Curr_Net_Cumulative_Closing_Balance_Cost_RM + "','" + End_date + "','" + Curr_Market_Price_NAV + "','" + End_date + "','" + Curr_Unrealised_Gain_Loss_RM + "','" + End_date + "','" + End_date + "');";


                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        try
                                        {
                                            Log.Write("UTMC_011 : " + IPD_Fund_Code + " " + IPD_Member_Acc_No + "Inserted", End_date);
                                            mysql_comm.ExecuteNonQuery();
                                        }
                                        catch
                                        {
                                            continue;
                                        }
                                    }

                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                        conn.Close();
                    }
                }

                System.Threading.Thread.Sleep(2000);
                ReadHolderNoforutmc11();
            }

            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        protected void ReadHolderNoforutmc11()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "SELECT IPD_Member_Acc_No,IPD_Fund_Code  FROM utmc_compositional_investment where Report_Date='" + End_date + "'";
                string ReportDt = End_date;

                DateTime dateTime = Convert.ToDateTime(Start_date);
                dateTime = dateTime.AddDays(-1);
                string OpeningDate = dateTime.ToString("yyyy-MM-dd");

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {


                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr);

                            rdr.Close();
                            mysql_conn.Close();

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                string EPF_No = row[0].ToString();
                                string IPD_fund_Code = row[1].ToString();
                                UpdateBalance(EPF_No, IPD_fund_Code, OpeningDate, false);
                                UpdateBalance(EPF_No, IPD_fund_Code, ReportDt, true);
                            }


                            //while (rdr.Read())
                            //{
                            //    string EPF_No = rdr[0].ToString();
                            //    string IPD_fund_Code = rdr[1].ToString();
                            //    UpdateBalance(EPF_No, IPD_fund_Code, OpeningDate, false);
                            //    UpdateBalance(EPF_No, IPD_fund_Code, ReportDt, true);
                            //}
                            //rdr.Close();
                        }

                       // mysql_conn.Close();
                    }
                }
                Log.Write("UTMC 11 for " + Start_date + "to" + End_date + "import from oracle", End_date);
            }
            catch (Exception ex)
            {
                ex.ToString();
                Log.Write("UTMC 11 for " + Start_date + "to" + End_date + "import from oracle - failed", End_date);
        
            }

        }

        protected void UpdateBalance(string HolderNo, string IPD_fund_Code, string ReportDt, bool IsClosingUpdate)
        {
            string ConnectionString = GetOracleConnection();
            string Sqlconnstring = GetSqlConnection();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(Sqlconnstring))
                {
                    conn.Open();
                    string mysql = "select Actual_Transferred_From_EPF_RM,Units,Market_Value,Report_Date from utmc_member_investment where Report_Date='" + ReportDt + "' AND  IPD_Member_Acc_No='" + HolderNo + "' and IPD_Fund_Code='" + IPD_fund_Code + "' ";

                    using (MySqlCommand comm = new MySqlCommand(mysql, conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();
                                string Query = "";
                                bool det = false;

                                if (!IsClosingUpdate)
                                {
                                    det = true;
                                }

                                while (rdr.Read())
                                {
                                    det = true;

                                    if (IsClosingUpdate)
                                    {

                                        
                                        string Balance = rdr[0].ToString();
                                        string Net_Cumulative_Closing_Balance_Units = rdr[1].ToString();
                                        string MarketPrice = rdr[2].ToString();
                                        string Gain = (Convert.ToDecimal(MarketPrice) - Convert.ToDecimal(Balance)).ToString();
                                        
                                        /* gordon */
                                        string MarketPricePU = decimal.Round((Convert.ToDecimal(MarketPrice) / Convert.ToDecimal(Net_Cumulative_Closing_Balance_Units)), 4).ToString();

                                        Query = "UPDATE utmc_compositional_investment SET Net_Cumulative_Closing_Balance_Units='" + Net_Cumulative_Closing_Balance_Units + "',Net_Cumulative_Closing_Balance_Cost_RM='" + Balance + "', Market_Price_NAV='" + MarketPricePU + "', Unrealised_Gain_Loss_RM='" + Gain + "' where IPD_Member_Acc_No='" + HolderNo + "' AND IPD_Fund_Code= '" + IPD_fund_Code + "' and Report_Date='" + End_date + "'";
                                        /* gordon */

                                    }
                                    else
                                    {
                                        string OpeningBalance = rdr[0].ToString();
                                        string OpeningBalanceUnit = rdr[1].ToString();

                                        Query = "UPDATE utmc_compositional_investment SET Opening_Balance_Units='" + OpeningBalanceUnit + "',Opening_Balance_Cost_RM='" + OpeningBalance + "'  where IPD_Member_Acc_No='" + HolderNo + "' AND IPD_Fund_Code= '" + IPD_fund_Code + "' and Report_Date='" + End_date + "'";

                                    }

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        try
                                        {
                                            mysql_comm.ExecuteNonQuery();
                                        }
                                        catch
                                        {
                                            continue;
                                        }
                                    }



                                }

                                if (!det)
                                {
                                    // UpdateReprot15_RD(HolderNo, IPD_fund_Code, ReportDt);


                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                        conn.Close();
                    }
                }
            }


            catch (Exception ex)
            {
                ex.ToString();
                Log.Write("UTMC 11 - UpdateBalance " + HolderNo + " import from oracle - failed", End_date);
       
            }

        }


        //murali update 11
        protected void UpdateMarketPerNaV()
        {       
            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "select IPD_Fund_Code,Market_Price_Effective_Date "
                                            +" from utmc_compositional_investment where report_date='"+End_date+"' and "
                                             +" Market_Price_NAV!=0.00 group by IPD_Fund_Code ";

         

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            while (rdr.Read())
                            { 
                                string IPD_fund_Code = rdr[0].ToString();
                                DateTime dt = Convert.ToDateTime(rdr[1].ToString());

                                string LastWorkingDate = dt.ToString("yyyy-MM-dd");

                                string ReadValue=GetLastWorkingDayValue("select Daily_Unit_Price from utmc_daily_nav_fund where  Daily_NAV_Date='"+LastWorkingDate+"' and IPD_Fund_Code='"+IPD_fund_Code+"'");

                                string UpdateQuery = "Update utmc_compositional_investment set Market_Price_NAV='" + ReadValue + "'  where report_date='" + End_date + "' and  Market_Price_NAV!=0.00 and IPD_Fund_Code='" + IPD_fund_Code + "';";                               
                                
                                string UpdateQuery1="  UPDATE  utmc_member_investment SET Market_Value=(UNITS* "+ReadValue+")  WHERE report_date='" + End_date + "'"
                                                                        +" AND  IPD_Fund_Code='"+IPD_fund_Code+"'  AND UNITS !=0; ";

                                SQL_Execute_NonQuery(UpdateQuery+UpdateQuery1);
                                Log.Write("Update MarketPriceByNAV : " + UpdateQuery +"-----"+UpdateQuery1, End_date);
                            }
                            rdr.Close();
                        }

                         mysql_conn.Close();
                    }
                }
                Log.Write("UTMC 11 for " + Start_date + "to" + End_date + "Updated Market Price NAV", End_date);
            }
            catch (Exception ex)
            {
                ex.ToString();
                Log.Write("UTMC 11 for " + Start_date + "to" + End_date + "Failed Market Price NAV", End_date);

            }

        }

        protected void UpdateReport12Final()
        {
            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql =  " select sum(market_value) from utmc_member_investment  where Report_Date = '" + End_date + "'  and IPD_Fund_Code !='02' ";    // 02 update

              

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                string NAV_Value = rdr[0].ToString();

                                string UpdateQuery = " update utmc_daily_nav set  Daily_NAV='" + NAV_Value + "' where report_date='" + End_date + "' and Service_Charge_Percent=0.125 ";

                                SQL_Execute_NonQuery(UpdateQuery);
                                Log.Write("Update rpt 12 NAV : " + UpdateQuery , End_date);
                            }
                            rdr.Close();
                        }

                        mysql_conn.Close();
                    }
                }
                Log.Write("UTMC 12 for " + Start_date + "to" + End_date + "Updated Daily NAV ", End_date);
            }
            catch (Exception ex)
            {
                ex.ToString();
                Log.Write("UTMC 12 for " + Start_date + "to" + End_date + "Failed  Daily NAV", End_date);

            }

        }

        protected void UpdateReport18Final()
        {
            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = " select sum(market_value), IPD_Fund_Code, (select Daily_NAV_Date from utmc_daily_nav   WHERE "
                + "report_date='"+End_date+ "' and Service_Charge_Percent=0.125) from utmc_member_investment  where Report_Date = '" + End_date + "'  and IPD_Fund_Code !='02'  group by  IPD_Fund_Code  ";    // 02 update



                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                string NAV_Value = rdr[0].ToString();
                                string IPDfundCode = rdr[1].ToString();
                                DateTime dt = Convert.ToDateTime(rdr[2].ToString());
                                string NAVDate = dt.ToString("yyyy-MM-dd");

                                string UpdateQuery = " update  utmc_daily_nav_fund set  Daily_NAV_EPF='" + NAV_Value + "' where Daily_NAV_Date='" + NAVDate + "' and Report_Date='" + End_date + "' and IPD_Fund_Code='"+IPDfundCode+"'";

                                SQL_Execute_NonQuery(UpdateQuery);
                                Log.Write("Update rpt 18 NAV : " + UpdateQuery, End_date);
                            }
                            rdr.Close();
                        }

                        mysql_conn.Close();
                    }
                }
                Log.Write("UTMC 18 for " + Start_date + "to" + End_date + "Updated Daily NAV ", End_date);
            }
            catch (Exception ex)
            {
                ex.ToString();
                Log.Write("UTMC 18 for " + Start_date + "to" + End_date + "Failed  Daily NAV", End_date);

            }

        }

        protected string GetLastWorkingDayValue(string Query)
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string IPD_no = "";
                string Mysql = Query;
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                IPD_no = rdr[0].ToString();
                            }
                            rdr.Close();
                            mysql_conn.Close();
                        }
                    }
                }
                return IPD_no;
            }
            catch (Exception ex)
            {

                ex.ToString();
                return "";
            }

        }



        private void UpdateReprot15_RD(string HolderNo, string IPD_fund_Code, string ReportDt)
        {
            //  string Sqlconnstring = GetSqlConnection();
            try
            {
                Get_Redumptiondate(HolderNo, IPD_fund_Code, true);
                // UpdateReprot15_RD("11945", "04");            
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
            }

        }
     
        protected void ReadReportMeta_data(string Query, string ReportName, int Number_of_Columns)
        {
            string Sqlconnstring = GetSqlConnection();
            try
            {
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (var cmd = new MySqlCommand(Query, mysql_conn))
                    {
                        int count = Convert.ToInt32(cmd.ExecuteScalar());
                        UTMC02_Update(ReportName, count+1, Number_of_Columns);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }

        protected void UTMC02_Update(string Report_Number, int No_of_Rows, int No_of_Columns)
        {
            string Sqlconnstring = GetSqlConnection();
            try
            {
                string End_date_format = End_date;
                End_date_format = End_date_format.Replace("/", "-");

                



                //string Query = "INSERT INTO utmc_report_metadata"
                //      + " (EPF_IPD_Code, Report_Number, Number_Of_Rows, number_of_columns, Prepared_By_Date, Verified_By_Date, Report_Date)"
                //      + " VALUES ('" + EPF_IPD_Code + "','" + Report_Number + "','" + No_of_Rows + "','" + No_of_Columns + "','" + End_date_format + "','" + End_date_format + "','" + End_date_format + "')";


                string Query = "UPDATE utmc_report_metadata SET Number_Of_Rows = '" + No_of_Rows + "', number_of_columns = '" + No_of_Columns + "', Prepared_By_Date = '" + End_date_format + "', " +
                               " Verified_By_Date = '" + End_date_format + "', Report_Date = '" + End_date_format + "' WHERE Report_Number = '" + Report_Number + "'";
                
                

                //string Query = "INSERT INTO utmc_fund_asset_class_composition"
                 //     + " (File_Name, EPF_IPD_Code, IPD_Fund_Code, Effective_Date, Asset_Class_Code, Percent_Exposure, Report_Date,Plan_Type)"
                  //    + " VALUES ('" + str + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Effective_Date + "','" + Asset_Class_Code + "','" + Percent_Exposure + "','" + End_date + "','" + Plan_type + "')";



                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();
                    using (MySqlCommand comm = new MySqlCommand(Query, mysql_conn))
                    {

                        comm.ExecuteNonQuery();
                    }

                    mysql_conn.Close();
                }

            }

            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
            }
        }

        protected void SQL_Execute_NonQuery(string strQuery)
        {
            string Sqlconnstring = GetSqlConnection();
            try
            {
                
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();
                    using (MySqlCommand comm = new MySqlCommand(strQuery, mysql_conn))
                    {

                        comm.ExecuteNonQuery();
                    }

                    mysql_conn.Close();
                }

            }

            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
            }
        }


        private void UTMC_02_Click(object sender, EventArgs e)
        {
            /*

            //UTMC001
            string UTMC001_Query = "SELECT count(*) FROM utmc_ipd_info";
            ReadReportMeta_data(UTMC001_Query, "UTMC001", 2);

            //UTMC002
            string UTMC002_Query = "SELECT count(*) FROM utmc_report_metadata;";
            ReadReportMeta_data(UTMC002_Query, "UTMC002", 10);

            //UTMC003
            string UTMC003_Query = "SELECT count(*) FROM utmc_member_information where Report_Date='" + End_date + "'";
            ReadReportMeta_data(UTMC003_Query, "UTMC003", 08);

            //UTMC004
            string UTMC004_Query = "SELECT count(*) FROM utmc_fund_information";
            ReadReportMeta_data(UTMC004_Query, "UTMC004", 09);

            //UTMC005
            string UTMC005_Query = "SELECT count(*) FROM utmc_fund_asset_class_composition";
            ReadReportMeta_data(UTMC005_Query, "UTMC005", 05);
            //UTMC006
            string UTMC006_Query = "SELECT count(*) FROM utmc_fund_sector_composition";
            ReadReportMeta_data(UTMC006_Query, "UTMC006", 05);
            //UTMC007
            string UTMC007_Query = "SELECT count(*) FROM utmc_fund_sector_composition";
            ReadReportMeta_data(UTMC007_Query, "UTMC007", 05);
            //UTMC008
            string UTMC008_Query = "SELECT count(*) FROM utmc_fund_currency_composition";
            ReadReportMeta_data(UTMC008_Query, "UTMC008", 06);
            //UTMC009
            string UTMC009_Query = "SELECT count(*) FROM utmc_fund_country_composition";
            ReadReportMeta_data(UTMC009_Query, "UTMC009", 05);

            //UTMC010
            string UTMC010_Query = "SELECT count(*) FROM utmc_compositional_transactions where Report_Date='" + End_date + "'";
            ReadReportMeta_data(UTMC010_Query, "UTMC010", 18);

            //UTMC011
            string UTMC011_Query = "SELECT count(*) FROM utmc_compositional_investment where Report_Date='" + End_date + "'";
            ReadReportMeta_data(UTMC011_Query, "UTMC011", 14);

            //UTMC012
            string UTMC012_Query = "SELECT count(*) FROM utmc_daily_nav where Report_Date='" + End_date + "'";
            ReadReportMeta_data(UTMC012_Query, "UTMC012", 07);

            //UTMC013
            string UTMC013_Query = "SELECT count(*) FROM utmc_transferred where Report_Date='" + End_date + "'";
            ReadReportMeta_data(UTMC013_Query, "UTMC013", 15);
            //UTMC014
            string UTMC014_Query = "SELECT count(*) FROM utmc_release_control where Report_Date='" + End_date + "'";
            ReadReportMeta_data(UTMC014_Query, "UTMC014", 11);
            //UTMC015
            string UTMC015_Query = "SELECT count(*) FROM utmc_member_investment where Report_Date='" + End_date + "'";
            ReadReportMeta_data(UTMC015_Query, "UTMC015", 09);
            //UTMC016
            string UTMC016_Query = "SELECT count(*) FROM utmc_fund_daily_nav where Report_Date='" + End_date + "'";
            ReadReportMeta_data(UTMC016_Query, "UTMC016", 05);
            //UTMC017
            string UTMC017_Query = "SELECT count(*) FROM utmc_fund_corporate_actions where Report_Date='" + End_date + "'";
            ReadReportMeta_data(UTMC017_Query, "UTMC017", 05);

            //UTMC018
            string UTMC018_Query = "SELECT count(*) FROM utmc_daily_nav_fund where Report_Date='" + End_date + "'";
            ReadReportMeta_data(UTMC018_Query, "UTMC018", 04);

             * 
             * */
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Start_date = StartDate.Text;
            End_date = EndDate.Text;
            DateSelection.Text = "Import record between " + Start_date + " to " + End_date + "";

            //End_date = "1900/01/01";
            DateTime date = Convert.ToDateTime(End_date);
            var nextMonth = new DateTime(date.Year, date.Month, 1).AddMonths(1);
            
           // string year = DateTime.Now.Year.ToString();
            // year = (Convert.ToInt16(year) - 55).ToString();


            string[] datePartsDOB = Start_date.Split('/');

            int upper = datePartsDOB.GetUpperBound(0);
            if (upper == 2)
            {
                int tmpYear = Convert.ToInt32(datePartsDOB[0]) - 55;
                int tmpMonth = Convert.ToInt32(datePartsDOB[1]) - 1;
                if (tmpMonth == 0) 
                    { 
                        tmpMonth = 12; 
                        tmpYear = tmpYear - 1; 
                    }
                if (tmpMonth == 13)
                {
                    tmpMonth = 1;
                    tmpYear = tmpYear + 1;
                }
                int tmpDay = 1;

                DateTime dtLastTemp = new
                    DateTime(tmpYear, tmpMonth, tmpDay);
                BirthDateupdate = GetLastDayOfMonth(dtLastTemp).ToString("yyyy-MM-dd");

            }
            
            
            if (Convert.ToInt32(BirthDateupdate.Replace("-", "")) < 19610831)
            {
                BirthDateupdate = "1961-08-31";
            }

            
            //BirthDateupdate = End_date.Remove(0, 4).Insert(0, year);


            TransDateUpdate = nextMonth.ToString("yyyy-MM-dd");
        }

        protected string Read_IPD_info_for_utmc01()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string IPD_no = "";
                string Mysql = "SELECT EPF_IPD_Code,IPD_Name FROM utmc_ipd_info";
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                IPD_no = rdr[0].ToString();
                            }
                            rdr.Close();
                            mysql_conn.Close();
                        }
                    }
                }
                return IPD_no;
            }
            catch (Exception ex)
            {

                ex.ToString();
                return "";
            }

        }

        private void Process_UTMC03_AdjustDecease()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "update  utmc_member_information "
                   + "set Adjustment = 'S' "
                   + "where Report_Date >= '2010-09-01' and EPF_No in "
                   + "("
                   + "'10402564', '61272095', '17532738', '15451436', '05428762', '13581257', '16028600', '04159001', '10680005', '15286233', '13621082', '14901511', '10142244', '13825195', '10209020',"
                   + "'13430327', '10116499', '13644892', '10510752', '13938394', '12180650', '05265633', '10933506', '13267496', '14486547', '11399640', '16837490', '11040896', '10420784', '16080296',"
                   + "'05249109', '11690534', '11105589', '11587384', '10855509', '11656335', '11400044', '10630037', '11327632', '17583581', '10055192', '10641766', '10430586', '13195752')";


                //string ReportDt = End_date;

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();


                    using (MySqlCommand mysql_comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        mysql_comm.ExecuteNonQuery();
                    }

                    mysql_conn.Close();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            
        }


        private void Process_UTMC18_NAV_Adjust_OLD()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                /* gordon */
                string Mysql = "SELECT A1.EPF_IPD_Code, A1.IPD_Fund_Code, A1.Daily_NAV_Date, A1.Daily_NAV_EPF, "
                            + "A1.Daily_Unit_Created_EPF, A1.Daily_Unit_Price, A1.Daily_Unit_Created_EPF_Adj, A1.Report_Date, "
                            + "IFNULL( "
                            + "( "
                            + "  SELECT SUM(Units) as B_Factor FROM eppa_wbr1.utmc_compositional_TRANSACTIONS "
                            + "  WHERE  Report_Date = A1.Report_Date "
                            + "  AND Date_Of_Transaction > A1.Daily_NAV_Date "
                            + "  AND IPD_Fund_Code = A1.IPD_Fund_Code "
                            + "  GROUP BY IPD_Fund_Code "
                            + "), 0) as B_Factor, "
                            + "IFNULL( "
                            + "( "
                            + "SELECT SUM(Net_Cumulative_Closing_Balance_Units) as A_Factor FROM eppa_wbr1.utmc_compositional_investment "
                            + "WHERE  Report_Date = A1.Report_Date "
                            + "AND IPD_Fund_Code = A1.IPD_Fund_Code "
                            + "GROUP BY IPD_Fund_Code "
                            + "), 0) as A_Factor "
                            + "FROM utmc_daily_nav_fund A1 "
                            + "WHERE  A1.Report_Date = '" + End_date + "' "
                            + "ORDER BY A1.Daily_NAV_Date desc ";

                /* gordon */

                string ReportDt = End_date;
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {


                            while (rdr.Read())
                            {
                                string IPD_Fund_Code = rdr[1].ToString();
                                string Daily_NAV_Date = rdr[2].ToString();

                                decimal Daily_NAV_EPF = Convert.ToDecimal(rdr[3].ToString());
                                Daily_NAV_EPF = decimal.Round(Daily_NAV_EPF, 2);

                                decimal B_Factor = Convert.ToDecimal(rdr[8].ToString());
                                B_Factor = decimal.Round(B_Factor, 4);

                                decimal A_Factor = Convert.ToDecimal(rdr[9].ToString());
                                A_Factor = decimal.Round(A_Factor, 4);

                                decimal C_Factor = A_Factor - B_Factor;
                                C_Factor = decimal.Round(C_Factor, 4);


                                DateTime newDate = Convert.ToDateTime(Daily_NAV_Date);

                                Daily_NAV_Date = newDate.ToString("yyyy-MM-dd");

                                string Query = "UPDATE utmc_daily_nav_fund SET Daily_Unit_Created_EPF_Adj = '" + C_Factor + "', "
                                        + "Daily_NAV_EPF = (" + C_Factor + " * Daily_Unit_Price) "
                                        + "WHERE  Report_Date = '" + End_date + "' "
                                        + "AND Daily_NAV_Date = '" + Daily_NAV_Date + "' "
                                        + "AND IPD_Fund_Code = '" + IPD_Fund_Code + "' ";


                                string Sub_Sqlconnstring = GetSqlConnection();

                                using (MySqlConnection mysql_sub_conn = new MySqlConnection(Sub_Sqlconnstring))
                                {
                                    mysql_sub_conn.Open();



                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_sub_conn))
                                    {

                                        if (Daily_NAV_Date != End_date)
                                        {
                                            mysql_comm.ExecuteNonQuery();
                                            Log.Write("UTMC_18_Adjust : " + IPD_Fund_Code + " - " + Daily_NAV_Date + " - " + C_Factor + " -- Updated", End_date);

                                        }

                                    }


                                    mysql_sub_conn.Close();
                                }

                            }

                            rdr.Close();
                        }

                        mysql_conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }


        private void Process_UTMC18_NAV_Adjust()
        {

            string ConnectionString = GetSqlConnection();
            try
            {
                /* gordon */


                using (MySqlConnection conn = new MySqlConnection(ConnectionString))
                {
                    conn.Open();


                    // main looping
                    string mysql = "SELECT IPD_Fund_Code, Daily_NAV_Date, Daily_Unit_Price FROM utmc_daily_nav_fund "
                        + "where report_date = '" + End_date + "' "
                        + "order by IPD_Fund_Code, Daily_NAV_Date desc; ";

                    using (MySqlCommand comm = new MySqlCommand(mysql, conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(ConnectionString))
                            {
                                string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                                string IPD_Fund_Code2 = "";
                                decimal Daily_Unit_Created_EPF_Adj2 = 0.00M;
                                decimal Daily_NAV_Created_EPF2 = 0.00M;
                                Daily_Unit_Created_EPF_Adj2 = decimal.Round(Daily_Unit_Created_EPF_Adj2, 4);
                                Daily_NAV_Created_EPF2 = decimal.Round(Daily_NAV_Created_EPF2, 2);

                                int IsLastDay = 1;

                                mysql_conn.Open();
                                while (rdr.Read())
                                {
                                    string IPD_Fund_Code = rdr[0].ToString();
                                    DateTime Daily_NAV_Date = Convert.ToDateTime(rdr[1].ToString());
                                    decimal Daily_Unit_Price = Convert.ToDecimal(rdr[2].ToString());
                                    decimal Daily_Unit_Created_EPF_Adj = Convert.ToDecimal(rdr[2].ToString());


                                    // re-formatting the variable
                                    Daily_Unit_Price = decimal.Round(Daily_Unit_Price, 4);

                                    string strDaily_NAV_Date = Daily_NAV_Date.ToString("yyyy/MM/dd");
                                    string strTmpQuery = "";

                                    // confirm lastday of the month

                                    if (strDaily_NAV_Date.Substring(strDaily_NAV_Date.Length - 2, 2) == End_date.Substring(strDaily_NAV_Date.Length - 2, 2))
                                    {
                                        IsLastDay = 1;

                                    }
                                    else
                                    {
                                        IsLastDay = 0;
                                    }

                                    // check subquery for any transaction exit

                                    if (IsLastDay == 1)
                                    {
                                        strTmpQuery = "select IPD_FUND_CODE, sum(Net_Cumulative_Closing_Balance_Units), "
                                           + "sum(Net_Cumulative_Closing_Balance_Units*Market_Price_NAV) "
                                           + "from utmc_compositional_investment  "
                                           + "where report_date = '" + End_date + "'  AND IPD_FUND_CODE = '" + IPD_Fund_Code + "'  "
                                           + "and Net_Cumulative_Closing_Balance_Units > 0 "
                                           + "And NOT Member_EPF_No in (SELECT EPF_No FROM utmc_member_information WHERE Adjustment='S' AND  Report_Date<='" + End_date + "' GROUP BY EPF_No) "
                                           + "group by IPD_FUND_CODE ";

                                       // MessageBox.Show("hERE!!!");
                                    }
                                    else
                                    {
                                        //Daily_NAV_Date.AddDays(1);
                                        // gggggggggggggg
                                        string strDaily_NAV_Date2 = Daily_NAV_Date.AddDays(1).ToString("yyyy/MM/dd");
                                            
                                        strTmpQuery = "SELECT IPD_FUND_CODE, SUM(Units), SUM(Units* " + Daily_Unit_Price.ToString() + " ) FROM eppa_wbr1.utmc_compositional_transactions "
                                        + " WHERE Report_Date = '" + End_date + "' and Date_Of_Transaction = '" + strDaily_NAV_Date2 + "' AND IPD_FUND_CODE = '" + IPD_Fund_Code + "' "
                                        + " GROUP BY IPD_FUND_CODE, Date_Of_Transaction  "
                                        + " ORDER BY IPD_FUND_CODE, Date_Of_Transaction; ";
                                    }



                                    #region  update_daily_NAV_by_Fund

                                    using (MySqlConnection mysql_conn2 = new MySqlConnection(ConnectionString))
                                    {
                                        mysql_conn2.Open();

                                        using (MySqlCommand comm1 = new MySqlCommand(strTmpQuery, mysql_conn2))
                                        {
                                            //conn1.Open();
                                            using (MySqlDataReader rdr1 = comm1.ExecuteReader())
                                            {
                                                if (rdr1.HasRows)
                                                {
                                                    rdr1.Read();
                                                    // update nav and cur_unit_hldg and Daily_Unit_Price + - 
                                                    IPD_Fund_Code2 = rdr1[0].ToString();
                                                    if (IsLastDay == 1)
                                                    {
                                                        Daily_Unit_Created_EPF_Adj2 = Convert.ToDecimal(rdr1[1].ToString());

                                                    }
                                                    else
                                                    {
                                                        Daily_Unit_Created_EPF_Adj2 = Daily_Unit_Created_EPF_Adj2 - Convert.ToDecimal(rdr1[1].ToString());

                                                    }

                                                    Daily_NAV_Created_EPF2 = Daily_Unit_Price * Daily_Unit_Created_EPF_Adj2;


                                                    // update insert ???????????????

                                                    string Query = "update eppa_wbr1.utmc_daily_nav_fund set Daily_NAV_EPF = '" + Daily_NAV_Created_EPF2.ToString() + "', Daily_Unit_Created_EPF = '" + Daily_Unit_Created_EPF_Adj2 + "' "
                                                            + " where report_date = '" + End_date + "'  AND IPD_FUND_CODE = '" + IPD_Fund_Code2 + "' "
                                                            + " AND Daily_NAV_Date = '" + strDaily_NAV_Date + "'";
                                     
                                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                                    {
                                                        mysql_comm.ExecuteNonQuery();
                                                        Log.Write("UTMC_18 ADJUST UPDATE RPT 10 : " + IPD_Fund_Code2 + " " + Daily_NAV_Date + " " + Daily_NAV_Created_EPF2 + " " + Daily_Unit_Created_EPF_Adj2 + " - SUCCESS", End_date);

                                                    }




                                                }
                                                else
                                                {


                                                    // update insert ???????????????

                                                    string Query = "update utmc_daily_nav_fund set Daily_NAV_EPF = '" + Daily_NAV_Created_EPF2.ToString() + "', Daily_Unit_Created_EPF = '" + Daily_Unit_Created_EPF_Adj2 + "'  "
                                                            + " where report_date = '" + End_date + "'  AND IPD_FUND_CODE = '" + IPD_Fund_Code2 + "' "
                                                            + " AND Daily_NAV_Date = '" + strDaily_NAV_Date + "'";

                                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                                    {
                                                        mysql_comm.ExecuteNonQuery();
                                                        Log.Write("UTMC_18 ADJUST UPDATE RPT 10 : " + IPD_Fund_Code2 + " " + Daily_NAV_Date + " " + Daily_NAV_Created_EPF2 + " " + Daily_Unit_Created_EPF_Adj2 + " - SUCCESS", End_date);

                                                    }




                                                }
                                            }
                                        }

                                        mysql_conn2.Close();


                                    }



                                    /* gggggggggordon */



                                    #endregion

                                }

                                mysql_conn.Close();
                            }
                            
                            
                        }
                    }
                    
                    conn.Close();
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }



         private void Process_UTMC11_AdjustZEROUnit()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "UPDATE eppa_wbr1.utmc_compositional_investment A SET A.Market_Price_NAV = "
+ "	(    "
+ "SELECT B.Nav_Per_Unit FROM eppa_wbr1.utmc_fund_daily_nav B "
+ "WHERE NAV_Date_Daily = '" + End_date + "' AND A.IPD_Fund_Code = B.IPD_Fund_Code "
   + " ), Isactive = '1' "
+ "WHERE Market_Price_NAV = 0 AND Market_Price_Effective_Date = '" + End_date + "'; ";

                //string ReportDt = End_date;

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();







                    // update rpt 03 - YEAR END 58 UPDATE

                    // PATCH BUG FOR NIC_DATE > REPORT DATE 
                    string Query = "";

                    Query = "UPDATE eppa_wbr1.utmc_member_information SET NIC = 'C', NIC_Date = null "
                            + " WHERE  Report_Date='" + End_date + "' AND NIC_Date > '" + End_date + "'";

                    Log.Write(Query, End_date);


                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {
                        mysql_comm.ExecuteNonQuery();
                        Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                        //System.Threading.Thread.Sleep(20);
                    }



                    // PATCH BUG FOR NIC_DATE > REPORT DATE 








                    /// disable update zero unit NAVPU *******

                    //Log.Write(Mysql, End_date);

                    //using (MySqlCommand mysql_comm = new MySqlCommand(Mysql, mysql_conn))
                    //{
                    //    int abc = mysql_comm.ExecuteNonQuery();
                    //    Log.Write("UTMC_11 ADJUST ZERO UNIT : " + abc + " - UPDATED.", End_date);
                                      
                    //}

                    /// disable update zero unit NAVPU ********







                    mysql_conn.Close();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                Log.Write(ex.ToString(), End_date);
            }
            
        }
       



        //good

        private void Process_UTMC03_Adjust()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "SELECT Member_EPF_No"
                            + " FROM utmc_compositional_investment "
                            + " where "
                            + " Report_Date='" + End_date + "' group by Member_EPF_No order by Member_EPF_No";
                

                string ReportDt = End_date;
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                string Holder_No = rdr[0].ToString();



                                
                                string strTmpQuery = "SELECT * FROM utmc_member_information WHERE Report_Date = '" + End_date + "' AND EPF_No = '" + Holder_No + "'";

                                using (MySqlConnection mysql_conn2 = new MySqlConnection(Sqlconnstring))
                                {
                                    mysql_conn2.Open();



                                    using (MySqlCommand comm1 = new MySqlCommand(strTmpQuery, mysql_conn2))
                                    {
                                        //conn1.Open();
                                        using (MySqlDataReader rdr1 = comm1.ExecuteReader())
                                        {
                                            if (rdr1.HasRows)
                                            {
                                               // MessageBox.Show("1");
                                            }
                                            else
                                            {
                                                AdjustMent_Update_Memberinfo(Holder_No);

                                            }
                                        }
                                    }

                                    mysql_conn2.Close();

                                }














                                
                            }
                            rdr.Close();
                        }

                        mysql_conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }


        //GORDON

        private void Process_UTMC03_Adjust08()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "SELECT EPF_No FROM eppa_wbr1.utmc_member_information where "
                    + "Report_Date='" + End_date + "' AND NIC = '' order by EPF_No";


                string ReportDt = End_date;
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                string Holder_No = rdr[0].ToString();




                                string strTmpQuery = "DELETE FROM utmc_member_information WHERE Report_Date = '" + End_date + "' AND EPF_No = '" + Holder_No + "'";

                                using (MySqlConnection mysql_conn2 = new MySqlConnection(Sqlconnstring))
                                {
                                    mysql_conn2.Open();



                                    using (MySqlCommand comm1 = new MySqlCommand(strTmpQuery, mysql_conn2))
                                    {
                                        //conn1.Open();
                                        using (MySqlDataReader rdr1 = comm1.ExecuteReader())
                                        {
                                            if (rdr1.HasRows)
                                            {
                                                AdjustMent_Update_Memberinfo(Holder_No);

                                                Log.Write("UTMC_03 ADJUST 08 : " + Holder_No + " - INSERTED", End_date);
                                      
                                            }
                                            else
                                            {
                                                //AdjustMent_UpdateOnly_Memberinfo(Holder_No);
                                                AdjustMent_Update_Memberinfo(Holder_No);

                                                Log.Write("UTMC_03 ADJUST 08 : " + Holder_No + " - PENDING", End_date);
                                      
                                            }
                                        }
                                    }

                                    mysql_conn2.Close();

                                }















                            }
                            rdr.Close();
                        }

                        mysql_conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }


        //protected void AdjustMent_Update_Memberinfo(string Holder_No)
        //{
        //    string NIC = "";
        //    string NIC_Date = "";

        //    try
        //    {
        //        string ConnectionString = GetOracleConnection();
        //        string Sqlconnstring = GetSqlConnection();
               

        //        using (OracleConnection conn = new OracleConnection(ConnectionString))
        //        {
        //            conn.Open();
        //            string mysql = " SELECT A1.HOLDER_NO,  A1.ID_NO AS NRIC,  A1.ID_NO_2 AS EPF_NO, "
        //                         + " to_date(A1.BIRTH_DT),   A1.SEX,    A1.HOLDER_CLS,   A1.HOLDER_STATUS, A1.EPF_I_STATUS, A1.EPF_I_EFF_DT  FROM  "
        //                         + " UTS.HOLDER_REG A1 WHERE A1.ID_NO_2='" + Holder_No + "' AND rownum < 2 "
        //                        + " AND A1.HOLDER_CLS IN ('EB','EN','ER','ES','EZ','EP'); ";


        //            string Code = Read_IPD_info_for_utmc01();
        //            using (OracleCommand comm = new OracleCommand(mysql, conn))
        //            {
        //                // MessageBox.Show("Oracle Wait for  Execute");
        //                using (OracleDataReader rdr = comm.ExecuteReader())
        //                {
        //                    //  MessageBox.Show("Oracle read done");
        //                    using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
        //                    {
        //                        mysql_conn.Open();
        //                        StringBuilder sb = new StringBuilder();
        //                        while (rdr.Read())
        //                        {

        //                            string Passport_No = rdr[1].ToString();
        //                            string EPF_No = rdr[2].ToString();
        //                            //string NIC = "C";
        //                            // string NIC_Date = "N/A";
        //                            DateTime dt = Convert.ToDateTime(rdr[3].ToString());
        //                            string BirthDate = dt.ToString("yyyy-MM-dd");
        //                            string Gender = rdr[4].ToString();

        //                            DateTime now = DateTime.Now;
        //                            var startDate = new DateTime(now.Year, now.Month, 1);
        //                            var endDate = startDate.AddMonths(1).AddDays(-1);
        //                            string EndofMonth = endDate.ToString("yyyy-MM-dd");
        //                            // Effective

        //                            DateTime dt1 = Convert.ToDateTime(rdr[3].ToString());
        //                            string Release_Date = dt1.ToString("yyyy-MM-dd");
        //                            string year = DateTime.Now.Year.ToString();
        //                            Release_Date = Release_Date.Remove(0, 4).Insert(0, year);
        //                            string AccountNumber = rdr[0].ToString(); 

        //                            //gordon

        //                             NIC = rdr[7].ToString();

        //                             NIC_Date = rdr[8].ToString();

        //                            if (NIC_Date == "")
        //                            { NIC_Date = "1900-01-01"; }

        //                            DateTime dtEFF = Convert.ToDateTime(NIC_Date);
        //                            NIC_Date = dtEFF.ToString("yyyy-MM-dd");



        //                            string Adjustment = "T";



        //                            int IsRelease = 0;

        //                            if (Convert.ToInt32(BirthDate.Replace("-", "")) <= Convert.ToInt32(BirthDateupdate.Replace("-", "")))
        //                            {
        //                                IsRelease = 1;
        //                            }

                                    




        //                            string Query = "";

        //                            if (NIC_Date.Substring(0, 4) == "1990")
        //                            {
        //                                Query =
        //                               "Insert into utmc_member_information(IsRelease,IPD_Member_Acc_No, Code,Passport_No,EPF_No,NIC,Birthdate,Gender, Effective_Date_Of_Report,report_date,Adjustment) "
        //                              + " values('" + IsRelease + "','" + AccountNumber + "','" + Code + "', '" + Passport_No + "','" + EPF_No + "','" + NIC + "','" + BirthDate + "','" + Gender + "','" + End_date + "','" + End_date + "','" + Adjustment + "')";


        //                                Log.Write("UTMC_03 ADJUST UPDATE 08 : " + Holder_No + " " + NIC + " " + NIC_Date + " - SUCCESS", End_date);
        //                            }
        //                            else
        //                            {
        //                                Query =
        //                                 "Insert into utmc_member_information(IsRelease,IPD_Member_Acc_No, Code,Passport_No,EPF_No,NIC,NIC_DATE,Birthdate,Gender, Effective_Date_Of_Report,report_date,Adjustment) "
        //                             + " values('" + IsRelease + "','" + AccountNumber + "','" + Code + "', '" + Passport_No + "','" + EPF_No + "','" + NIC + "','" + NIC_Date + "','" + BirthDate + "','" + Gender + "','" + End_date + "','" + End_date + "','" + Adjustment + "')";

        //                                Log.Write("UTMC_03 ADJUST UPDATE 08 : " + Holder_No + " " + NIC + " " + NIC_Date + " - SUCCESS", End_date);
                                      
        //                            }



        //                            using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
        //                            {
        //                                mysql_comm.ExecuteNonQuery();
        //                            }
        //                        }
        //                        rdr.Close();
        //                        mysql_conn.Close();
        //                    }
        //                }
        //            }
        //            conn.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Write(ex.ToString(), End_date);
        //        Log.Write("UTMC_03 ADJUST Failed 08 : " + Holder_No + " " + NIC + " " + NIC_Date + " - SUCCESS", End_date);
                                  
        //    }


        //}


        protected void AdjustMent_Update_Memberinfo(string Holder_No)
        {

            try
            {
                string ConnectionString = GetOracleConnection();
                string Sqlconnstring = GetSqlConnection();

                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
        //            string mysql = " SELECT A1.HOLDER_NO,  A1.ID_NO AS NRIC,  A1.ID_NO_2 AS EPF_NO, "
         //                        + " to_date(A1.BIRTH_DT),   A1.SEX,    A1.HOLDER_CLS,   A1.HOLDER_STATUS FROM  "
          //                       + " UTS.HOLDER_REG A1 WHERE A1.ID_NO_2='" + Holder_No + "' AND rownum < 2";

                      string mysql = " SELECT A1.HOLDER_NO,  A1.ID_NO AS NRIC,  A1.ID_NO_2 AS EPF_NO, "
                                 + " to_date(A1.BIRTH_DT),   A1.SEX,    A1.HOLDER_CLS,   A1.HOLDER_STATUS, A1.EPF_I_STATUS, A1.EPF_I_EFF_DT  FROM  "
                                 + " UTS.HOLDER_REG A1 WHERE A1.ID_NO_2='" + Holder_No + "' AND rownum < 2 "
                                + " AND A1.HOLDER_CLS IN ('EB','EN','ER','ES','EZ','EP') ";


                    string Code = Read_IPD_info_for_utmc01();
                    using (OracleCommand comm = new OracleCommand(mysql, conn))
                    {
                        // MessageBox.Show("Oracle Wait for  Execute");
                        using (OracleDataReader rdr = comm.ExecuteReader())
                        {
                            //  MessageBox.Show("Oracle read done");
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();
                                StringBuilder sb = new StringBuilder();
                                while (rdr.Read())
                                {

                                    string Passport_No = rdr[1].ToString();
                                    string EPF_No = rdr[2].ToString();
                                    string NIC = "C";
                                    // string NIC_Date = "N/A";
                                    DateTime dt = Convert.ToDateTime(rdr[3].ToString());
                                    string BirthDate = dt.ToString("yyyy-MM-dd");
                                    string Gender = rdr[4].ToString();

                                    //gordon
                                     NIC = rdr[7].ToString();

                                 string NIC_Date = rdr[8].ToString();

                                    if (NIC_Date == "")
                                    { NIC_Date = "1900-01-01"; }

                                    DateTime dtEFF = Convert.ToDateTime(NIC_Date);
                                    NIC_Date = dtEFF.ToString("yyyy-MM-dd");



                                    DateTime now = DateTime.Now;
                                    var startDate = new DateTime(now.Year, now.Month, 1);
                                    var endDate = startDate.AddMonths(1).AddDays(-1);
                                    string EndofMonth = endDate.ToString("yyyy-MM-dd");
                                    // Effective

                                    DateTime dt1 = Convert.ToDateTime(rdr[3].ToString());
                                    string Release_Date = dt1.ToString("yyyy-MM-dd");
                                    string year = DateTime.Now.Year.ToString();
                                    Release_Date = Release_Date.Remove(0, 4).Insert(0, year);
                                    string AccountNumber = rdr[0].ToString();


                                    string Adjustment = "T";



                                    int IsRelease = 0;

                                    if (Convert.ToInt32(BirthDate.Replace("-", "")) <= Convert.ToInt32(BirthDateupdate.Replace("-", "")))
                                    {
                                        IsRelease = 1;
                                    }

                                    string Query =
                                       "Insert into utmc_member_information(IsRelease,IPD_Member_Acc_No, Code,Passport_No,EPF_No,NIC, NIC_DATE,Birthdate,Gender, Effective_Date_Of_Report,report_date,Adjustment) "
                                      + " values('" + IsRelease + "','" + AccountNumber + "','" + Code + "', '" + Passport_No + "','" + EPF_No + "','" + NIC + "','" + NIC_Date + "','" + BirthDate + "','" + Gender + "','" + End_date + "','" + End_date + "','" + Adjustment + "')";

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        mysql_comm.ExecuteNonQuery();
                                        Log.Write("UTMC_03 ADJUST UPDATE 08 : " + Holder_No + " " + NIC + " " + NIC_Date + " - SUCCESS", End_date);
     
                                    }
                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
            }


        }


        // not using
        protected void AdjustMent_UpdateOnly_Memberinfo(string Holder_No)
        {

            try
            {
                string ConnectionString = GetOracleConnection();
                string Sqlconnstring = GetSqlConnection();

                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    string mysql = " SELECT A1.HOLDER_NO,  A1.ID_NO AS NRIC,  A1.ID_NO_2 AS EPF_NO, "
                                 + " to_date(A1.BIRTH_DT),   A1.SEX,    A1.HOLDER_CLS,   A1.HOLDER_STATUS, A1.EPF_I_STATUS, A1.EPF_I_EFF_DT  FROM  "
                                 + " UTS.HOLDER_REG A1 WHERE A1.ID_NO_2='" + Holder_No + "' AND rownum < 2";


                    string Code = Read_IPD_info_for_utmc01();
                    using (OracleCommand comm = new OracleCommand(mysql, conn))
                    {
                        // MessageBox.Show("Oracle Wait for  Execute");
                        using (OracleDataReader rdr = comm.ExecuteReader())
                        {
                            //  MessageBox.Show("Oracle read done");
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();
                                StringBuilder sb = new StringBuilder();
                                while (rdr.Read())
                                {

                                    string Passport_No = rdr[1].ToString();
                                    string EPF_No = rdr[2].ToString();
                                    //string NIC = "C";
                                    // string NIC_Date = "N/A";
                                    DateTime dt = Convert.ToDateTime(rdr[3].ToString());
                                    string BirthDate = dt.ToString("yyyy-MM-dd");
                                    string Gender = rdr[4].ToString();

                                    DateTime now = DateTime.Now;
                                    var startDate = new DateTime(now.Year, now.Month, 1);
                                    var endDate = startDate.AddMonths(1).AddDays(-1);
                                    string EndofMonth = endDate.ToString("yyyy-MM-dd");
                                    // Effective

                                    DateTime dt1 = Convert.ToDateTime(rdr[3].ToString());
                                    string Release_Date = dt1.ToString("yyyy-MM-dd");
                                    string year = DateTime.Now.Year.ToString();
                                    Release_Date = Release_Date.Remove(0, 4).Insert(0, year);
                                    string AccountNumber = rdr[0].ToString();

                                    //gordon

                                    string NIC = rdr[7].ToString();

                                    string NIC_Date = rdr[8].ToString();

                                    if (NIC_Date == "")
                                    { NIC_Date = "1900-01-01"; }

                                    DateTime dtEFF = Convert.ToDateTime(NIC_Date);
                                    NIC_Date = dtEFF.ToString("yyyy-MM-dd");



                                    string Adjustment = "T";



                                    int IsRelease = 0;

                                    if (Convert.ToInt32(BirthDate.Replace("-", "")) <= Convert.ToInt32(BirthDateupdate.Replace("-", "")))
                                    {
                                        IsRelease = 1;
                                    }






                                    string Query = "";

                                    if (NIC_Date.Substring(0, 4) == "1990")
                                    {
                                        Query =
                                       "UPDATE utmc_member_information SET NIC = '" + NIC + "', NIC_DATE = '" + NIC_Date + "' WHERE EPF_NO = '" + Holder_No + "' AND REPORT_DATE = '" + End_date + "'";
                                    }
                                    else
                                    {
                                        Query =
                                       "UPDATE utmc_member_information SET NIC = '" + NIC + "', NIC_DATE = '" + NIC_Date + "' WHERE EPF_NO = '" + Holder_No + "' AND REPORT_DATE = '" + End_date + "'";
                                    

                                    }



                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        mysql_comm.ExecuteNonQuery();
                                    }
                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
            }


        }



        private void btn_import_Click(object sender, EventArgs e)
        {
            btn_import_Click_fn();
        }

        private void btn_import_Click_fn()
        {

            string EndDateforPMS = End_date;
            EndDateforPMS = EndDateforPMS.Replace("/", "");
            EndDateforPMS = EndDateforPMS.Replace("-", "");

            string ImportFilePath = ConfigurationManager.AppSettings["Import_file_path"];

            string ExportFilePath = ConfigurationManager.AppSettings["Export_file_path"];


            string str = "";
            string ReadLine = "";
            string filePath = @"" + ExportFilePath + "";
            int ColumnValue = 0;
            //string Plan_type = "";

            System.IO.DirectoryInfo dr = new System.IO.DirectoryInfo(filePath);
            System.IO.FileInfo[] files = dr.GetFiles("*.txt");

            string destFolder = ExportFilePath + "\\history\\" + EndDateforPMS.Substring(0, 6) + "\\";
            string destFolderFile = "";


            string Query = "UPDATE utmc_report_metadata SET Number_Of_Rows = 0";

            UTMC02_Update_for_Row_and_ColumnValue(Query);


            foreach (System.IO.FileInfo fie in files)
            {
                str = fie.Name;


                Log.Write(DateTime.Now.ToString() + " - start export file: " + str, End_date);

                destFolderFile = destFolder + str;
                string strRptCode = str.Substring(str.IndexOf("UTMC", 0) + 4, 3);

                System.IO.StreamReader sr = new System.IO.StreamReader(filePath + "\\" + str);

                var line = sr.ReadToEnd().Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                int Rownumber = line.Length;


                #region  update_column_Count

                for (int i = 0; i < line.Length; i++)
                {
                    ReadLine = line[i];
                    string[] columm1 = ReadLine.Split('|');
                    ColumnValue = columm1.Length;
                    break;
                }
                #endregion

                # region UpdateRow and Column

                if (strRptCode == "001")
                    UTMC02_Update("UTMC001", Rownumber, ColumnValue);

                if (strRptCode == "003")
                    UTMC02_Update("UTMC003", Rownumber, ColumnValue);

                if (strRptCode == "004")
                    UTMC02_Update("UTMC004", Rownumber, ColumnValue);

                if (strRptCode == "005")
                    UTMC02_Update("UTMC005", Rownumber, ColumnValue);

                if (strRptCode == "006")
                    UTMC02_Update("UTMC006", Rownumber, ColumnValue);

                if (strRptCode == "007")
                    UTMC02_Update("UTMC007", Rownumber, ColumnValue);

                if (strRptCode == "008")
                    UTMC02_Update("UTMC008", Rownumber, ColumnValue);

                if (strRptCode == "009")
                    UTMC02_Update("UTMC009", Rownumber, ColumnValue);

                if (strRptCode == "010")
                    UTMC02_Update("UTMC010", Rownumber, ColumnValue);

                if (strRptCode == "011")
                    UTMC02_Update("UTMC011", Rownumber, ColumnValue);

                if (strRptCode == "012")
                    UTMC02_Update("UTMC012", Rownumber, ColumnValue);

                if (strRptCode == "013")
                    UTMC02_Update("UTMC013", Rownumber, ColumnValue);

                if (strRptCode == "014")
                    UTMC02_Update("UTMC014", Rownumber, ColumnValue);

                if (strRptCode == "015")
                    UTMC02_Update("UTMC015", Rownumber, ColumnValue);

                if (strRptCode == "016")
                    UTMC02_Update("UTMC016", Rownumber, ColumnValue);

                if (strRptCode == "017")
                    UTMC02_Update("UTMC017", Rownumber, ColumnValue);

                if (strRptCode == "018")
                    UTMC02_Update("UTMC018", Rownumber, ColumnValue);
                # endregion


                //GetData(End_date_format, Report_Number);

               // # region GENDATA RPT2 UpdateRow and Column
                //string UTMC02_query = "SELECT EPF_IPD_Code,Report_Number,Number_Of_Rows,Number_Of_Columns,Prepared_By_Name,Prepared_By_Designation,Prepared_By_Date,Verified_By_Name,Verified_By_Designation,Verified_By_Date FROM utmc_report_metadata where Number_Of_Rows > 0 ORDER BY Report_Number ";
                //Read_export_data(UTMC02_query, "UTMC002");
                //# endregion


                sr.Close();
                sr = null;
            }
            # region GENDATA RPT2 UpdateRow and Column

           // UTMC02_Update("UTMC002", 0, 10);

            string UTMC002_Query = "SELECT count(*) FROM utmc_report_metadata where Number_Of_Rows > 0;";
            ReadReportMeta_data(UTMC002_Query, "UTMC002", 10);


            
            string UTMC02_query = "SELECT EPF_IPD_Code,Report_Number,Number_Of_Rows,Number_Of_Columns,Prepared_By_Name,Prepared_By_Designation,Prepared_By_Date,Verified_By_Name,Verified_By_Designation,Verified_By_Date FROM utmc_report_metadata where Number_Of_Rows > 0 ORDER BY Report_Number ";
            Write_Export_Data(UTMC02_query, "UTMC002");
            # endregion

            bool exists = System.IO.Directory.Exists(destFolder);

            if (!exists)
                System.IO.Directory.CreateDirectory(destFolder);


            foreach (System.IO.FileInfo fie in files)
            {
                destFolderFile = destFolder + fie;

                fie.CopyTo(destFolderFile, true);

                Log.Write(DateTime.Now.ToString() + " - copy export file to history folder: " + destFolderFile, End_date);


                fie.Delete();

            }



        }
            



        protected void UTMC02_Update_for_Row_and_ColumnValue(string Mysql)
        {
            string Sqlconnstring = GetSqlConnection();
            try
            {
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();
                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {

                        comm.ExecuteNonQuery();
                    }

                    mysql_conn.Close();
                }
            }

            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
            }
        }

        private void btn_export_Click(object sender, EventArgs e)
        {
            btn_export_Click_fn();
        }
        private void btn_export_Click_fn()
        {
            DateTime dateTime = Convert.ToDateTime(Start_date);

            dateTime = dateTime.AddDays(-1);
            string PrevMonthDate = dateTime.ToString("yyyy-MM-dd");




            string UTMC01_query = "SELECT EPF_IPD_Code,IPD_Name FROM utmc_ipd_info";
//            Read_export_data(UTMC01_query, "UTMC001");
            Write_Export_Data(UTMC01_query, "UTMC001");


            string UTMC02_query = "SELECT EPF_IPD_Code,Report_Number,Number_Of_Rows,Number_Of_Columns,Prepared_By_Name,Prepared_By_Designation,Prepared_By_Date,Verified_By_Name,Verified_By_Designation,Verified_By_Date FROM utmc_report_metadata where Number_Of_Rows >0 ";
            Write_Export_Data(UTMC02_query, "UTMC002");

            /* murali
            string UTMC03_query = " SELECT code,Passport_No,EPF_No,NIC,IFNULL(NIC_Date,\"\"),Birthdate,Gender,Effective_Date_Of_Report"
                                     + " FROM utmc_member_information"
                                     + " where EPF_No in (select Member_EPF_No from utmc_compositional_investment "
                // + " where Net_Cumulative_Closing_Balance_Units>0 and Report_Date='"+End_date+"' group by Member_EPF_No) "   // Without Zero
                                     + " where  Report_Date='" + End_date + "' group by Member_EPF_No) "                 //with Zero
                                    + " and  Adjustment='A' "
                //  + " and Report_Date in ('2016-05-31','2016-06-30','2016-07-31','2016-08-31')"   // 06,07 and 08
                                    + " and Report_Date in ('" + PrevMonthDate + "','" + End_date + "')"
                                    + " group by EPF_No order by EPF_No";

            //Read_export_data(UTMC03_query, "UTMC003");

             */

            string UTMC03_query = " SELECT code,Passport_No,EPF_No,NIC,IF(NIC = 'C','',NIC_Date) AS NIC_Date,Birthdate,Gender,Effective_Date_Of_Report"
                                    + " FROM utmc_member_information "
                                    + " where  Report_Date='" + End_date + "'"                 //with Zero
                                    + " And NOT EPF_No in (SELECT EPF_No FROM utmc_member_information WHERE Adjustment='S'  AND  Report_Date<='" + End_date + "' GROUP BY EPF_No) "
                               + " and  EPF_No in (SELECT Member_EPF_No FROM utmc_member_investment where  Report_Date='" + End_date + "'  and IPD_Fund_Code !='02' group by Member_EPF_No ) "      //02 update
                                    + " group by EPF_No order by EPF_No";

            Write_Export_Data(UTMC03_query, "UTMC003");


            string UTMC04_query = "SELECT EPF_IPD_Code,IPD_Fund_Code,Fund_Name,'" + End_date + "',LIPPER_Category_Of_Fund "
                                    + " ,Conventional,Status,Foreign_Fund,Fund_Base_Currency "
                                    + "  FROM utmc_fund_information "
                                    + "  where IPD_Fund_Code !='02' "    //02 update
                                    + " order by IPD_Fund_Code";

            //Read_export_data(UTMC04_query, "UTMC004");
            Write_Export_Data(UTMC04_query, "UTMC004");

            string UTMC05_query = "select EPF_IPD_Code,IPD_Fund_Code,Effective_Date,Asset_Class_Code,Format(Percent_Exposure,2) "
                                   + " FROM utmc_fund_asset_class_composition "
                                    + " where Plan_Type = 'E' and Report_Date in ('" + End_date + "') "
                                    + " order by Effective_Date,IPD_Fund_Code";

            //Read_export_data(UTMC05_query, "UTMC005");
            Write_Export_Data(UTMC05_query, "UTMC005");

            string UTMC06_query = "SELECT EPF_IPD_Code,IPD_Fund_Code,Effective_Date,GICS_Sector_Code,Format(Percent_Exposure,2)"
                                   + " from utmc_fund_sector_composition "
                                   + " where Plan_Type = 'E' and Report_Date in ('" + End_date + "') "
                                   + " order by Effective_Date,IPD_Fund_Code";
            //Read_export_data(UTMC06_query, "UTMC006");
            Write_Export_Data(UTMC06_query, "UTMC006");

            string UTMC07_query = "SELECT EPF_IPD_Code,IPD_Fund_Code,Effective_Date,Shariah_Conventional,Format(Percent_Exposure,2)"
                                  + " from utmc_fund_shariah_composition "
                                    + " where Plan_Type = 'E' and Report_Date in ('" + End_date + "') "
                                    + "order by Effective_Date,IPD_Fund_Code";
            //Read_export_data(UTMC07_query, "UTMC007");
            Write_Export_Data(UTMC07_query, "UTMC007");

            string UTMC08_query = "SELECT EPF_IPD_Code,IPD_Fund_Code,Effective_Date,ISO_Country_Code,ISO_Listed_Trade_Country_Code "
                                    + " ,Format(Percent_Exposure,2) from utmc_fund_country_composition "
                                    + " where Plan_Type = 'E' and Report_Date in ('" + End_date + "') "
                                    + " order by Effective_Date,IPD_Fund_Code";
            //Read_export_data(UTMC08_query, "UTMC008");
            Write_Export_Data(UTMC08_query, "UTMC008");

            string UTMC09_query = "SELECT EPF_IPD_Code,IPD_Fund_Code,Effective_Date,ISO_Currency_Code,Format(Percent_Exposure,2) "
                                  + " from utmc_fund_currency_composition "
                                  + " where Plan_Type = 'E' and Report_Date in ('" + End_date + "') "
                                  + " order by Effective_Date,IPD_Fund_Code";
            //Read_export_data(UTMC09_query, "UTMC009");
            Write_Export_Data(UTMC09_query, "UTMC009");

            string UTMC10_query = "SELECT A.EPF_IPD_Code,A.IPD_Fund_Code,A.Member_EPF_No, A.IPD_Member_Acc_No, A.Effective_Date,A.Date_Of_Transaction,A.Date_Of_Settlement"
                                    + " ,A.IPD_Unique_Transaction_ID,A.Transaction_Code,A.Reversed_Transaction_ID,A.Units,A.Gross_Amount_RM,"
                                    + " A.Net_Amount_RM,A.Fees_RM,"
                                    + " A.GST_RM,A.Cost_RM,A.Proceeds_RM,A.Realised_Gain_Loss FROM utmc_compositional_transactions A"
                                    + " where A.Report_Date in ('" + End_date + "')"
                                    + " And NOT Member_EPF_No in (SELECT EPF_No FROM utmc_member_information WHERE Adjustment='S' GROUP BY EPF_No) "
                //  + " where A.Report_Date in ('2016-06-30','2016-07-31','2016-08-31') and B.Adjustment='A'   order by "   // 06,07,08
                                    + "  order by  A.Transaction_Code, A.IPD_Fund_Code, A.Date_Of_Transaction";

            //Read_export_data(UTMC10_query, "UTMC010");
            Write_Export_Data(UTMC10_query, "UTMC010");

            string UTMC11_query = "SELECT A.EPF_IPD_Code,A.IPD_Fund_Code,A.Member_EPF_No,A.IPD_Member_Acc_No,"
                                    + " A.Effective_Date,A.Opening_Balance_Units,A.Opening_Balance_Cost_RM,A.Opening_Balance_Date,"
                                    + " A.Net_Cumulative_Closing_Balance_Units,A.Net_Cumulative_Closing_Balance_Cost_RM,A.Net_Cumulative_Closing_Balance_Date,"
                                    + " A.Market_Price_NAV,A.Market_Price_Effective_Date,A.Unrealised_Gain_Loss_RM"
                                    + " FROM utmc_compositional_investment A where A.Report_Date in ('" + End_date + "')"
                                    + " And NOT Member_EPF_No in (SELECT EPF_No FROM utmc_member_information WHERE Adjustment='S' AND  Report_Date<='" + End_date + "' GROUP BY EPF_No) "
                                    + " and A.IPD_Fund_Code !='02' "                                                //02 update
                                    + " ORDER BY A.IPD_Fund_Code,A.IPD_Member_Acc_No";
            Write_Export_Data(UTMC11_query, "UTMC011");


            string UTMC12_query = "SELECT EPF_IPD_Code,Daily_NAV_Date,Daily_NAV,Working_Day_Status,Working_Day_Count,Service_Charge_Percent,"
                                   + " Service_Charge_RM FROM utmc_daily_nav where Report_Date='" + End_date + "'";
            //Read_export_data(UTMC12_query, "UTMC012");
            Write_Export_Data(UTMC12_query, "UTMC012");

            string UTMC13_query = "select distinct A.Member_EPF_No,A.EPF_IPD_Code,A.IPD_Fund_Code,"
                                    + " A.IPD_Member_Acc_No,A.Transfer_Date,A.Transaction_Code,"
                                    + " A.IPD_Unique_Transaction_ID,A.Reversed_Transaction_ID,A.Actual_Transferred_From_EPF_RM,A.Service_Charge_RM,"
                                    + " A.GST_RM,A.Actual_Amount_Invested_RM,A.Transaction_Price_RM,A.Units_Created,A.Effective_Date"
                                    + " from utmc_transferred A where A.Report_Date in ('" + End_date + "')"
                                    + " And NOT Member_EPF_No in (SELECT EPF_No FROM utmc_member_information WHERE Adjustment='S' GROUP BY EPF_No) "
                                     + " and A.IPD_Fund_Code !='02' "                                                //02 update
                                    + " order by A.Transfer_Date, A.IPD_Fund_Code, A.IPD_Member_Acc_No";

            Write_Export_Data(UTMC13_query, "UTMC013");

            string UTMC14_query = "SELECT Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,"
                                  + " Release_Date,Transaction_Code,IPD_Unique_Transaction_ID,Reversed_Transaction_ID,Units,Cost_RM,Report_Date "
                                  + " FROM eppa_wbr1.utmc_release_control where Report_Date='" + End_date + "'";
           // Read_export_data(UTMC14_query, "UTMC014");
            Write_Export_Data(UTMC14_query, "UTMC014");

            
            string UTMC15_query = "SELECT A.Member_EPF_No,A.EPF_IPD_Code,A.IPD_Fund_Code,A.IPD_Member_Acc_No,A.Actual_Cost,"
                                + " A.Units,A.Book_Value,A.Market_Value,A.Effective_Date"
                                + " FROM utmc_member_investment A where A.Report_Date in ('" + End_date + "')"
                                + " And NOT Member_EPF_No in (SELECT EPF_No FROM utmc_member_information WHERE Adjustment='S' AND  Report_Date<='" + End_date + "' GROUP BY EPF_No) "
                                + " and A.IPD_Fund_Code !='02' "                                                //02 update 
                                + " ORDER BY A.IPD_Fund_Code,A.IPD_Member_Acc_No";

            Write_Export_Data(UTMC15_query, "UTMC015");



            string UTMC16_query = "SELECT EPF_IPD_Code,IPD_Fund_Code,NAV_Date_Daily,Nav_Per_Unit,Total_Units_Circulation"
                                 + " FROM utmc_fund_daily_nav "
                // + " where Report_Date in ('2016-06-30','2016-07-31','2016-08-31') order by IPD_Fund_Code, NAV_Date_Daily";
                                  + " where Report_Date in ('" + End_date + "') "
                                      + " and IPD_Fund_Code !='02' "                                                //02 update 
            +" order by IPD_Fund_Code, NAV_Date_Daily";
            //Read_export_data(UTMC16_query, "UTMC016");
            Write_Export_Data(UTMC16_query, "UTMC016");

            string UTMC17_query = "select EPF_IPD_Code, IPD_Fund_Code,Corporate_Action_Date,Distributions,Unit_Splits "
                                   + "FROM utmc_fund_corporate_actions where Report_Date='" + End_date + "' "
                                      + " and IPD_Fund_Code !='02' "                                                //02 update 
            +" order by Corporate_Action_Date      ";
            //Read_export_data(UTMC17_query, "UTMC017");
            Write_Export_Data(UTMC17_query, "UTMC017");
                                    
          
            string UTMC18_query = "SELECT EPF_IPD_Code,IPD_Fund_Code,Daily_NAV_Date,Daily_NAV_EPF FROM "
                                  + " utmc_daily_nav_fund  where Report_Date='" + End_date + "'"
                                  + " and IPD_Fund_Code !='02' " // 02 update
                                  +" order by Daily_NAV_Date,IPD_Fund_Code";
            //Read_export_data(UTMC18_query, "UTMC018");
            Write_Export_Data(UTMC18_query, "UTMC018");
        }

        protected void Read_export_data(string strScript, string ReportName)
        {
            string Sqlconnstring = GetSqlConnection();
            DataTable dt = new DataTable();

            
            try
            {

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(strScript, mysql_conn))
                    {

                        IDataReader dr = comm.ExecuteReader();
                         dt.Load(dr);
                    }

                    mysql_conn.Close();
                }
                GetData(dt, ReportName);
            }
            catch (Exception)
            {

                throw;
            }    
        }

        protected void Write_Export_Data(string strScript, string ReportName)
        {

            string ExportGRtextFile = "";

            string path = ConfigurationManager.AppSettings["Export_file_path"];
            string EndDateforPMS = End_date;

            EndDateforPMS = EndDateforPMS.Replace("-", "");
            EndDateforPMS = EndDateforPMS.Replace("/", "");
            string RunningNo = "001";
            
            ExportGRtextFile = path + "IPD033" + ReportName  + EndDateforPMS + RunningNo + ".txt";

            ExportGRtextFile = ExportGRtextFile.Replace("\\", "/");
            
            if (File.Exists(ExportGRtextFile))
            {
                File.Delete(ExportGRtextFile);
            }


            string Sqlconnstring = GetSqlConnection();
            //DataTable dt = new DataTable();

            strScript = strScript  + 
                            " INTO OUTFILE '" + ExportGRtextFile + "'  " +
                            " FIELDS TERMINATED BY '|'  " +
                            " ENCLOSED BY '\"'  " +
                            " LINES TERMINATED BY '\r\n'  ";

            // strScript = "SELECT EPF_IPD_Code,IPD_Fund_Code,Daily_NAV_Date,Daily_NAV_EPF, ID FROM  utmc_daily_nav_fund   where   Report_Date>='2016/09/01' and Report_Date<='2016/09/30' order by Daily_NAV_Date,IPD_Fund_Code";

            try
            {

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(strScript, mysql_conn))
                    {

                         comm.ExecuteReader(); ;

                        //IDataReader dr = comm.ExecuteReader();
                        //dt.Load(dr);
                    }

                    mysql_conn.Close();
                }
              //  GetData(dt, ReportName);
            }
            catch (Exception)
            {

                throw;
            }


        }


        protected string GetData(DataTable dt, string ReportName)
        {
            StreamWriter ExportGRtextFile = null;

            //string path = @"C:\\";
            //if (!Directory.Exists(path + "\\EXPORT\\"))
            //{
            //    Directory.CreateDirectory(path + "\\EXPORT\\");
            //}
            //path = path + "\\EXPORT\\";

            string path = ConfigurationManager.AppSettings["Export_file_path"];

            string EndDateforPMS = End_date;

            EndDateforPMS = EndDateforPMS.Replace("/", "");
            string StrFullValue = "";
            string RunningNo = "001";



            foreach (DataRow row in dt.Rows)
            {
                if (ReportName == "UTMC001")
                {
                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC001" + EndDateforPMS + RunningNo + ".txt");
                    
                    ExportGRtextFile.WriteLine(StrFullValue);
                //    Log.Write(StrFullValue, End_date);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();

                }
                if (ReportName == "UTMC002")
                {
                    DateTime dt1 = Convert.ToDateTime(row[6].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");
                    DateTime dt2 = Convert.ToDateTime(row[9].ToString());
                    string SecondDate = dt2.ToString("yyyy-MM-dd");


                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + row[2].ToString() + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + row[4].ToString() + "\"" + "|" + "\"" + row[5].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[7].ToString() + "\"" + "|" + "\"" + row[8].ToString() + "\"" + "|" + "\"" + SecondDate + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC002" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                  //  Log.Write(StrFullValue, End_date);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }
                if (ReportName == "UTMC003")
                {

                    DateTime dt1 = Convert.ToDateTime(row[5].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");
                    DateTime dt2 = Convert.ToDateTime(row[7].ToString());
                    string SecondDate = dt2.ToString("yyyy-MM-dd");

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + row[2].ToString() + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + row[4].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[6].ToString() + "\"" + "|" + "\"" + End_date + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC003" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }
                if (ReportName == "UTMC004")
                {
                    DateTime dt1 = Convert.ToDateTime(row[3].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + row[2].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[4].ToString() + "\"" + "|" + "\"" + row[5].ToString() + "\"" + "|" + "\"" + row[6].ToString() + "\"" + "|" + "\"" + row[7].ToString() + "\"" + "|" + "\"" + row[8].ToString() + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC004" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                   // Log.Write(StrFullValue, End_date);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }
                if (ReportName == "UTMC005")
                {
                    DateTime dt1 = Convert.ToDateTime(row[2].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + row[4].ToString() + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC005" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                   // Log.Write(StrFullValue, End_date);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }
                if (ReportName == "UTMC006")
                {
                    DateTime dt1 = Convert.ToDateTime(row[2].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + row[4].ToString() + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC006" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                  //  Log.Write(StrFullValue, End_date);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }
                if (ReportName == "UTMC007")
                {
                    DateTime dt1 = Convert.ToDateTime(row[2].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + row[4].ToString() + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC007" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }

                if (ReportName == "UTMC008")
                {
                    DateTime dt1 = Convert.ToDateTime(row[2].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");
                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + row[4].ToString() + "\"" + "|" + "\"" + row[5].ToString() + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC008" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                  //  Log.Write(StrFullValue, End_date);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }

                if (ReportName == "UTMC009")
                {
                    DateTime dt1 = Convert.ToDateTime(row[2].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + row[4].ToString() + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC009" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }

                if (ReportName == "UTMC010")
                {

                    DateTime dt1 = Convert.ToDateTime(row[4].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");
                    DateTime dt2 = Convert.ToDateTime(row[5].ToString());
                    string SecondDate = dt2.ToString("yyyy-MM-dd");
                    DateTime dt3 = Convert.ToDateTime(row[6].ToString());
                    string ThirdDate = dt3.ToString("yyyy-MM-dd");

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + row[2].ToString() + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + SecondDate + "\"" + "|" + "\"" + ThirdDate + "\"" + "|" + "\"" + row[7].ToString() + "\"" + "|" + "\"" + row[8].ToString() + "\"" + "|" + "\"" + row[9].ToString() + "\"" + "|" + "\"" + row[10].ToString() + "\"" + "|" + "\"" + row[11].ToString() + "\"" + "|" + "\"" + row[12].ToString() + "\"" + "|" + "\"" + row[13].ToString() + "\"" + "|" + "\"" + row[14].ToString() + "|" + "\"" + row[15].ToString() + "\"" + "|" + "\"" + row[16].ToString() + "\"" + "|" + "\"" + row[17].ToString() + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC010" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                 //   Log.Write(StrFullValue, End_date);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }

                if (ReportName == "UTMC011")
                {

                    DateTime dt1 = Convert.ToDateTime(row[4].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");
                    DateTime dt2 = Convert.ToDateTime(row[7].ToString());
                    string SecondDate = dt2.ToString("yyyy-MM-dd");
                    DateTime dt3 = Convert.ToDateTime(row[10].ToString());
                    string ThirdDate = dt3.ToString("yyyy-MM-dd");
                    DateTime dt4 = Convert.ToDateTime(row[12].ToString());
                    string fourtdate = dt4.ToString("yyyy-MM-dd");

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + row[2].ToString() + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[5].ToString() + "\"" + "|" + "\"" + row[6].ToString() + "\"" + "|" + "\"" + SecondDate + "\"" + "|" + "\"" + row[8].ToString() + "\"" + "|" + "\"" + row[9].ToString() + "\"" + "|" + "\"" + ThirdDate + "\"" + "|" + "\"" + row[11].ToString() + "\"" + "|" + "\"" + fourtdate + "\"" + "|" + "\"" + row[13].ToString() + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC011" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                  //  Log.Write(StrFullValue, End_date);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }

                if (ReportName == "UTMC012")
                {
                    DateTime dt1 = Convert.ToDateTime(row[1].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[2].ToString() + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + row[4].ToString() + "\"" + "|" + "\"" + row[5].ToString() + "\"" + "|" + "\"" + row[6].ToString() + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC012" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                  //  Log.Write(StrFullValue, End_date);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }

                if (ReportName == "UTMC013")
                {

                    DateTime dt1 = Convert.ToDateTime(row[4].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");
                    DateTime dt2 = Convert.ToDateTime(row[14].ToString());
                    string SecondDate = dt2.ToString("yyyy-MM-dd");
                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + row[2].ToString() + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[5].ToString() + "\"" + "|" + "\"" + row[6].ToString() + "\"" + "|" + "\"" + row[7].ToString() + "\"" + "|" + "\"" + row[8].ToString() + "|" + "\"" + row[9].ToString() + "\"" + "|" + "\"" + row[10].ToString() + "\"" + "|" + "\"" + row[11].ToString() + "\"" + "|" + "\"" + row[12].ToString() + "\"" + "|" + "\"" + row[13].ToString() + "\"" + "|" + "\"" + SecondDate + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC013" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                //    Log.Write(StrFullValue, End_date);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }

                if (ReportName == "UTMC014")
                {
                    DateTime dt1 = Convert.ToDateTime(row[4].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");
                    DateTime dt2 = Convert.ToDateTime(row[10].ToString());
                    string SecondDate = dt2.ToString("yyyy-MM-dd");

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + row[2].ToString() + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[5].ToString() + "\"" + "|" + "\"" + row[6].ToString() + "\"" + "|" + "\"" + row[7].ToString() + "\"" + "|" + "\"" + row[8].ToString() +"\"" + "|" + "\"" + row[9].ToString() + "\"" + "|" + "\"" + SecondDate + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC014" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                  //  Log.Write(StrFullValue, End_date);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }

                if (ReportName == "UTMC015")
                {
                    DateTime dt1 = Convert.ToDateTime(row[8].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + row[2].ToString() + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + row[4].ToString() + "\"" + "|" + "\"" + row[5].ToString() + "\"" + "|" + "\"" + row[6].ToString() + "\"" + "|" + "\"" + row[7].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC015" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }

                if (ReportName == "UTMC016")
                {

                    DateTime dt1 = Convert.ToDateTime(row[2].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + row[4].ToString() + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC016" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }

                if (ReportName == "UTMC017")
                {

                    DateTime dt1 = Convert.ToDateTime(row[2].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + row[3].ToString() + "\"" + "|" + "\"" + row[4].ToString() + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC017" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                  //  Log.Write(StrFullValue, End_date);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }

                if (ReportName == "UTMC018")
                {

                    DateTime dt1 = Convert.ToDateTime(row[2].ToString());
                    string Firstdate = dt1.ToString("yyyy-MM-dd");
                    string Value = row[3].ToString();

                    StrFullValue = StrFullValue + "\"" + row[0].ToString() + "\"" + "|" + "\"" + row[1].ToString() + "\"" + "|" + "\"" + Firstdate + "\"" + "|" + "\"" + Value + "\"" + "\r\n";
                    ExportGRtextFile = new StreamWriter(path + "IPD033UTMC018" + EndDateforPMS + RunningNo + ".txt");
                    ExportGRtextFile.WriteLine(StrFullValue);
                //    Log.Write(StrFullValue, End_date);
                    ExportGRtextFile.Flush();
                    ExportGRtextFile.Close();
                    ExportGRtextFile.Dispose();
                }
            }

            return "OK";

        }

        private void Test_Click(object sender, EventArgs e)
        {
               
                                DateTime dtLastCalendarDay = Convert.ToDateTime(End_date);
                                DateTime dtLastWorkingDay = Convert.ToDateTime("2016-12-29");
                                int intLastWorkingDay = dtLastWorkingDay.Day;
                                int intLastCalendarDay = dtLastCalendarDay.Day;

                                int k = 0;
                                for (int i = intLastWorkingDay; i < intLastCalendarDay; i++)
                                {
                                    k = k + 1; 
                                    string strLastWorkingDay = dtLastWorkingDay.AddDays(k).ToString("yyyy-MM-dd");
                                    MessageBox.Show(strLastWorkingDay);
                                }
        }

        private void Process_UTMC15_Adjust()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "select * from utmc_compositional_investment "
                            + " where Net_Cumulative_Closing_Balance_Units=0 "
                            + " and Report_Date='" + End_date + "'";

                string ReportDt = End_date;
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                string Member_EPF_No = rdr[2].ToString();
                                string EPF_IPD_Code = rdr[0].ToString();
                                string IPD_Fund_Code = rdr[1].ToString();
                                string IPD_Member_Acc_No = rdr[3].ToString();
                                string Actual_Transferred_From_EPF_RM = "0.00";
                                string Units = "0.0000";
                                string Book_Value = "0.00";
                                string Market_Value = "0.00";


                                string Query =
                                    "Insert IGNORE into utmc_member_investment(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Actual_Transferred_From_EPF_RM,Units,Book_Value,Market_Value,Effective_Date,Report_Date) "
                                     + " values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "', '" + IPD_Member_Acc_No + "','" + Actual_Transferred_From_EPF_RM + "','" + Units + "','" + Book_Value + "','" + Market_Value + "','" + End_date + "','" + End_date + "')";



                                string Sub_Sqlconnstring = GetSqlConnection();

                                using (MySqlConnection mysql_sub_conn = new MySqlConnection(Sub_Sqlconnstring))
                                {
                                    mysql_sub_conn.Open();



                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_sub_conn))
                                    {
                                        mysql_comm.ExecuteNonQuery();
                                         Log.Write("UTMC_15_Adjust : " + IPD_Fund_Code + Member_EPF_No + IPD_Member_Acc_No + "Inserted", End_date);
                                    }


                                    mysql_sub_conn.Close();
                                }

                            }

                            rdr.Close();
                        }

                        mysql_conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }



        private void Process_UTMC10_Adjust()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "INSERT INTO eppa_wbr1.utmc_member_information (Code, Passport_No, EPF_No, NIC, NIC_Date, Birthdate, Gender, Effective_Date_Of_Report, Report_Date, Adjustment, IPD_Member_Acc_No) "
                                + "SELECT Code, Passport_No, EPF_No, NIC, NIC_Date, Birthdate, Gender, '" + End_date + "', '" + End_date + "', 'T1', IPD_Member_Acc_No  FROM eppa_wbr1.utmc_member_information WHERE EPF_No IN "
                                + "( "
                                + "SELECT B.Member_EPF_No FROM eppa_wbr1.utmc_compositional_transactions B WHERE B.Report_Date = '" + End_date + "' "
                                + "AND NOT B.Member_EPF_No IN "
                                + "(SELECT EPF_No FROM eppa_wbr1.utmc_member_information A WHERE A.Report_Date = '" + End_date + "') "
                                + "GROUP BY B.Member_EPF_No order by ID DESC "
                                + ") "
                                + "group by EPF_No; ";

                string ReportDt = End_date;

                using (MySqlConnection mysql_sub_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_sub_conn.Open();

                    using (MySqlCommand mysql_comm = new MySqlCommand(Mysql, mysql_sub_conn))
                    {
                        int strResult = 0;
                        strResult = mysql_comm.ExecuteNonQuery();
                        Log.Write("UTMC_10_Adjust : " + strResult + "Inserted", End_date);
                    }

                    mysql_sub_conn.Close();
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }


        private void Process_UTMC18_Adjust()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                /* gordon */
                string Mysql = "select IPD_Fund_Code, sum(Market_Price_NAV*Net_Cumulative_Closing_Balance_Units) from utmc_compositional_investment   "
                            + " where  Report_Date= '" + End_date + "'"
                            + " group by IPD_Fund_Code";

                /* gordon */

                string ReportDt = End_date;
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                string IPD_Fund_Code = rdr[0].ToString();
                                //string Market_Value = rdr[1].ToString();

                                

                                decimal Market_Value = Convert.ToDecimal(rdr[1].ToString());
                                Market_Value = decimal.Round(Market_Value, 2);

                               

                                string Query =
                                    "UPDATE utmc_daily_nav_fund set Daily_NAV_EPF = '" + Market_Value + "'"
                                        + " WHERE IPD_Fund_Code = '" + IPD_Fund_Code + "' AND Daily_NAV_Date = '" + End_date + "';";
                                    


                               



                                string Sub_Sqlconnstring = GetSqlConnection();

                                using (MySqlConnection mysql_sub_conn = new MySqlConnection(Sub_Sqlconnstring))
                                {
                                    mysql_sub_conn.Open();



                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_sub_conn))
                                    {
                                        mysql_comm.ExecuteNonQuery();
                                        Log.Write("UTMC_18_Adjust : " + IPD_Fund_Code + " - " + End_date + " - " + Market_Value + " -- Updated", End_date);
                                        Log.Write("UTMC_11_Adjust : " + IPD_Fund_Code + " - " + End_date + " - " + Market_Value + " -- Updated", End_date);
                                   
                                    }


                                    mysql_sub_conn.Close();
                                }

                            }

                            rdr.Close();
                        }

                        mysql_conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }



        private void Process_UTMC_Purge()
        {
            string Mysql = "";
            string Sqlconnstring = GetSqlConnection();
            try
            {
                Mysql = "SET SQL_SAFE_UPDATES = 0; "
+ "DELETE   from  utmc_member_information where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
+ "DELETE   from  utmc_compositional_transactions where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
+ "DELETE   from  utmc_compositional_investment where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
+ "DELETE   from  utmc_transferred where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
+ "DELETE   from  utmc_release_control where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
+ "DELETE   from  utmc_member_investment where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
+ "DELETE   from  utmc_fund_daily_nav where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
+ "DELETE   from  utmc_daily_nav_fund where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
+ "DELETE   from  utmc_daily_nav where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
+ "DELETE   from  utmc_fund_corporate_actions where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "';  "
+ "DELETE   from  utmc_fund_asset_class_composition where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
+ "DELETE   from  utmc_fund_sector_composition where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
+ "DELETE   from  utmc_fund_shariah_composition where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
+ "DELETE   from  utmc_fund_country_composition where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
+ "DELETE   from  utmc_fund_currency_composition where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; ";



                string ReportDt = End_date;
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();


                    using (MySqlCommand mysql_comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        mysql_comm.ExecuteNonQuery();
                        Log.Write("UTMC_Purge : " + Mysql + " - SUCCESSFULLY", End_date);
                    }


                    mysql_conn.Close();
                }
                
            }
            catch (Exception ex)
            {
                Log.Write("UTMC_Purge : " + Mysql + " - UNSUCCESSFULLY", End_date);
                MessageBox.Show(ex.Message);

            }

        }

        //private void Process_UTMC03_Adjust()
        //{

        //    string Sqlconnstring = GetSqlConnection();
        //    try
        //    {
        //        string Mysql = "select IPD_Fund_Code, sum(Market_Price_NAV) from utmc_compositional_investment   "
        //                    + " where  Report_Date= '" + End_date + "'"
        //                    + " group by IPD_Fund_Code";


        //        string ReportDt = End_date;
        //        using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
        //        {
        //            mysql_conn.Open();

        //            using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
        //            {
        //                using (MySqlDataReader rdr = comm.ExecuteReader())
        //                {
        //                    while (rdr.Read())
        //                    {
        //                        string IPD_Fund_Code = rdr[0].ToString();
        //                        //string Market_Value = rdr[1].ToString();

        //                        decimal Market_Value = Convert.ToDecimal(rdr[1].ToString());
        //                        Market_Value = decimal.Round(Market_Value, 2);



        //                        string Query =
        //                            "UPDATE utmc_daily_nav_fund set Daily_NAV_EPF = '" + Market_Value + "'"
        //                                + " WHERE IPD_Fund_Code = '" + IPD_Fund_Code + "' AND Daily_NAV_Date = '" + End_date + "'";



        //                        string Sub_Sqlconnstring = GetSqlConnection();

        //                        using (MySqlConnection mysql_sub_conn = new MySqlConnection(Sub_Sqlconnstring))
        //                        {
        //                            mysql_sub_conn.Open();



        //                            using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_sub_conn))
        //                            {
        //                                mysql_comm.ExecuteNonQuery();
        //                                Log.Write("UTMC_03_Adjust : " + IPD_Fund_Code + " - " + End_date + " - " + Market_Value + " -- Updated", End_date);
        //                            }


        //                            mysql_sub_conn.Close();
        //                        }

        //                    }

        //                    rdr.Close();
        //                }

        //                mysql_conn.Close();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }

        //}


        private void  Process_UTMC05_09()
        {
        
            string EndDateforPMS = End_date;
            EndDateforPMS = EndDateforPMS.Replace("-", "");

            string ImportFilePath = ConfigurationManager.AppSettings["Import_file_path"];

            string str = "";
            string ReadLine = "";
            string filePath = @""+ImportFilePath+"";
            int ColumnValue = 0;
            string Plan_type = "";

            System.IO.DirectoryInfo dr = new System.IO.DirectoryInfo(filePath);
            System.IO.FileInfo[] files = dr.GetFiles("*.txt");

            string destFolder = ImportFilePath + "\\history\\";
            string destFolderFile = "";

            try {

                foreach (System.IO.FileInfo fie in files)
                {

                    
                    str = fie.Name;



                    Log.Write(DateTime.Now.ToString() + " - start import file: " + str, End_date);

                    destFolderFile = destFolder + str;
                    string strRptCode = str.Substring(str.IndexOf("UTMC", 0) + 4, 3);
                    string strRptDate = str.Substring(str.IndexOf("UTMC", 0) + 7, 8);

                    if (strRptDate == EndDateforPMS)

                    {




                        //                  System.IO.StreamReader sr = System.IO.File.OpenText(filePath + "\\" + str);

                        //                    var line = sr.ReadToEnd().Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        System.IO.StreamReader sr = new System.IO.StreamReader(filePath + "\\" + str);

                        var line = sr.ReadToEnd().Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        int Rownumber = line.Length;

                        #region UTMC005

                        if (strRptCode == "005")
                        {

                            for (int i = 0; i < line.Length; i++)
                            {
                                ReadLine = line[i];
                                string[] columm = ReadLine.Split('|');
                                ColumnValue = columm.Length;
                                string EPF_IPD_Code = columm[0];
                                EPF_IPD_Code = EPF_IPD_Code.Replace("\"", "");
                                string IPD_Fund_Code = columm[1];
                                IPD_Fund_Code = IPD_Fund_Code.Replace("\"", "");
                                string Effective_Date = columm[2];
                                Effective_Date = Effective_Date.Replace("\"", "");
                                string Asset_Class_Code = columm[3];
                                Asset_Class_Code = Asset_Class_Code.Replace("\"", "");
                                string Percent_Exposure = columm[4];
                                Percent_Exposure = Percent_Exposure.Replace("\"", "");

                                if (((IPD_Fund_Code == "01")) || ((IPD_Fund_Code == "02")) || ((IPD_Fund_Code == "03")) || ((IPD_Fund_Code == "04")) || ((IPD_Fund_Code == "05")) || ((IPD_Fund_Code == "06")) || ((IPD_Fund_Code == "07")))
                                {
                                    Plan_type = "E";
                                }
                                else
                                {
                                    Plan_type = "C";
                                }

                                //End_date = Effective_Date;

                                string Query = "INSERT INTO utmc_fund_asset_class_composition"
                          + " (File_Name, EPF_IPD_Code, IPD_Fund_Code, Effective_Date, Asset_Class_Code, Percent_Exposure, Report_Date,Plan_Type)"
                          + " VALUES ('" + str + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Effective_Date + "','" + Asset_Class_Code + "','" + Percent_Exposure + "','" + Effective_Date + "','" + Plan_type + "')";

                                UTMC02_Update_for_Row_and_ColumnValue(Query);

                            }
                        }
                        #endregion

                        #region UTMC006
                        if (strRptCode == "006")
                        {

                            for (int i = 0; i < line.Length; i++)
                            {
                                ReadLine = line[i];
                                string[] columm = ReadLine.Split('|');

                                string EPF_IPD_Code = columm[0];
                                EPF_IPD_Code = EPF_IPD_Code.Replace("\"", "");
                                string IPD_Fund_Code = columm[1];
                                IPD_Fund_Code = IPD_Fund_Code.Replace("\"", "");
                                string Effective_Date = columm[2];
                                Effective_Date = Effective_Date.Replace("\"", "");
                                string GICS_Sector_Code = columm[3];
                                GICS_Sector_Code = GICS_Sector_Code.Replace("\"", "");
                                string Percent_Exposure = columm[4];
                                Percent_Exposure = Percent_Exposure.Replace("\"", "");

                                if (((IPD_Fund_Code == "01")) || ((IPD_Fund_Code == "02")) || ((IPD_Fund_Code == "03")) || ((IPD_Fund_Code == "04")) || ((IPD_Fund_Code == "05")) || ((IPD_Fund_Code == "06")) || ((IPD_Fund_Code == "07")))
                                {
                                    Plan_type = "E";
                                }
                                else
                                {
                                    Plan_type = "C";
                                }

                                // End_date = Effective_Date;

                                string Query = "INSERT INTO utmc_fund_sector_composition"
                                              + " (File_Name, EPF_IPD_Code, IPD_Fund_Code, Effective_Date, GICS_Sector_Code, Percent_Exposure, Report_Date,Plan_Type)"
                                              + " VALUES ('" + str + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Effective_Date + "','" + GICS_Sector_Code + "','" + Percent_Exposure + "','" + End_date + "','" + Plan_type + "')";
                                UTMC02_Update_for_Row_and_ColumnValue(Query);

                            }
                        }
                        #endregion

                        #region UTMC007
                        else if (strRptCode == "007")
                        {

                            for (int i = 0; i < line.Length; i++)
                            {
                                ReadLine = line[i];
                                string[] columm = ReadLine.Split('|');

                                string EPF_IPD_Code = columm[0];
                                EPF_IPD_Code = EPF_IPD_Code.Replace("\"", "");
                                string IPD_Fund_Code = columm[1];
                                IPD_Fund_Code = IPD_Fund_Code.Replace("\"", "");
                                string Effective_Date = columm[2];
                                Effective_Date = Effective_Date.Replace("\"", "");
                                string Shariah_Conventional = columm[3];
                                Shariah_Conventional = Shariah_Conventional.Replace("\"", "");
                                string Percent_Exposure = columm[4];
                                Percent_Exposure = Percent_Exposure.Replace("\"", "");

                                if (((IPD_Fund_Code == "01")) || ((IPD_Fund_Code == "02")) || ((IPD_Fund_Code == "03")) || ((IPD_Fund_Code == "04")) || ((IPD_Fund_Code == "05")) || ((IPD_Fund_Code == "06")) || ((IPD_Fund_Code == "07")))
                                {
                                    Plan_type = "E";
                                }
                                else
                                {
                                    Plan_type = "C";
                                }


                                //End_date = Effective_Date;

                                string Qery = "INSERT INTO utmc_fund_shariah_composition"
                                           + " (File_Name, EPF_IPD_Code, IPD_Fund_Code, Effective_Date, Shariah_Conventional, Percent_Exposure, Report_Date,Plan_Type)"

                                           + " VALUES ('" + str + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Effective_Date + "','" + Shariah_Conventional + "','" + Percent_Exposure + "','" + End_date + "','" + Plan_type + "')";

                                UTMC02_Update_for_Row_and_ColumnValue(Qery);
                            }
                        }
                        #endregion

                        #region UTMC008
                        else if (strRptCode == "008")
                        {

                            for (int i = 0; i < line.Length; i++)
                            {
                                ReadLine = line[i];
                                string[] columm = ReadLine.Split('|');
                                string EPF_IPD_Code = columm[0];
                                EPF_IPD_Code = EPF_IPD_Code.Replace("\"", "");
                                string IPD_Fund_Code = columm[1];
                                IPD_Fund_Code = IPD_Fund_Code.Replace("\"", "");
                                string Effective_Date = columm[2];
                                Effective_Date = Effective_Date.Replace("\"", "");
                                string ISO_Country_Code = columm[3];
                                ISO_Country_Code = ISO_Country_Code.Replace("\"", "");
                                string ISO_Listed_Trade_Country_Code = columm[4];
                                ISO_Listed_Trade_Country_Code = ISO_Listed_Trade_Country_Code.Replace("\"", "");
                                string Percent_Exposure = columm[5];
                                Percent_Exposure = Percent_Exposure.Replace("\"", "");

                                if (((IPD_Fund_Code == "01")) || ((IPD_Fund_Code == "02")) || ((IPD_Fund_Code == "03")) || ((IPD_Fund_Code == "04")) || ((IPD_Fund_Code == "05")) || ((IPD_Fund_Code == "06")) || ((IPD_Fund_Code == "07")))
                                {
                                    Plan_type = "E";
                                }
                                else
                                {
                                    Plan_type = "C";
                                }


                                //End_date = Effective_Date;


                                string Query = "INSERT INTO utmc_fund_country_composition "
                              + "(File_Name, EPF_IPD_Code, IPD_Fund_Code, Effective_Date, ISO_Country_Code, ISO_Listed_Trade_Country_Code, Percent_Exposure, Report_Date,Plan_Type)"
                              + " VALUES ('" + str + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Effective_Date + "','" + ISO_Country_Code + "','" + ISO_Listed_Trade_Country_Code + "','" + Percent_Exposure + "','" + End_date + "','" + Plan_type + "')";

                                UTMC02_Update_for_Row_and_ColumnValue(Query);

                            }
                        }

                        #endregion

                        #region UTMC009
                        else if (strRptCode == "009")
                        {

                            for (int i = 0; i < line.Length; i++)
                            {
                                ReadLine = line[i];
                                string[] columm = ReadLine.Split('|');
                                string EPF_IPD_Code = columm[0];
                                EPF_IPD_Code = EPF_IPD_Code.Replace("\"", "");
                                string IPD_Fund_Code = columm[1];
                                IPD_Fund_Code = IPD_Fund_Code.Replace("\"", "");
                                string Effective_Date = columm[2];
                                Effective_Date = Effective_Date.Replace("\"", "");
                                string ISO_Currency_Code = columm[3];
                                ISO_Currency_Code = ISO_Currency_Code.Replace("\"", "");
                                string Percent_Exposure = columm[4];
                                Percent_Exposure = Percent_Exposure.Replace("\"", "");

                                if (((IPD_Fund_Code == "01")) || ((IPD_Fund_Code == "02")) || ((IPD_Fund_Code == "03")) || ((IPD_Fund_Code == "04")) || ((IPD_Fund_Code == "05")) || ((IPD_Fund_Code == "06")) || ((IPD_Fund_Code == "07")))
                                {
                                    Plan_type = "E";
                                }
                                else
                                {
                                    Plan_type = "C";
                                }

                                //End_date = Effective_Date;

                                string Query = "INSERT INTO utmc_fund_currency_composition"
                                             + " (File_Name, EPF_IPD_Code, IPD_Fund_Code, Effective_Date, ISO_Currency_Code, Percent_Exposure, Report_Date,Plan_Type)"

                                             + " VALUES ('" + str + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Effective_Date + "','" + ISO_Currency_Code + "','" + Percent_Exposure + "','" + End_date + "','" + Plan_type + "')";

                                UTMC02_Update_for_Row_and_ColumnValue(Query);


                            }
                        }

                        #endregion



                        fie.CopyTo(destFolderFile, true);

                        Log.Write(DateTime.Now.ToString() + " - copy import file to history folder: " + str, End_date);

                        sr.Close();
                        sr = null;

                        fie.Delete();




                    }


                }



            }

            catch {
                Log.Write(DateTime.Now.ToString() + " - err import file from import folder: " + str, End_date);

                
            }

            finally { 
            
            }


            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                // step 30 - export all data into exportfilepath EXCLUDE 02
                btn_export_Click_fn();
                Log.Write(DateTime.Now.ToString() + " - Export all data into exportfilepath EXCLUDE 02 ", End_date);

                System.Threading.Thread.Sleep(50);
                // step 31 - Update rpt 02 based on the files in the exportfilepath
                btn_import_Click_fn();
                Log.Write(DateTime.Now.ToString() + " - Update rpt 02 based on the files in the exportfilepath: ", End_date);

               // MessageBox.Show("Completed!!!");
            }
            catch (Exception ex)
            {
                Log.Write(DateTime.Now.ToString() + " - Final export err (" + ex.ToString() + ")" , End_date);
              
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                // step 20 - Import fund level RPT 16 data into mysql 
                Process_UTMC16();
                Log.Write(DateTime.Now.ToString() + " - step 20 - Import fund level data into mysql  ", End_date);

                System.Threading.Thread.Sleep(500);
                // step 21 - Import fund level RPT 18 data into MYSQL 
                Process_UTMC18();
                Log.Write(DateTime.Now.ToString() + " - step 21 - Import fund level RPT 18 data into MYSQL ", End_date);

                // step 22 - Import fund level RPT 12 data into MYSQL 
                 Process_UTMC12();
                Log.Write(DateTime.Now.ToString() + " - step 22 - Import fund level RPT 12 data into MYSQL   ", End_date);

                // step 23 - Import fund level RPT 17 data into MYSQL 
                Process_UTMC17();
                Log.Write(DateTime.Now.ToString() + " - step 23 - Import fund level RPT 17 data into MYSQL ", End_date);

                // step 24 - Import fund level RPT 05 - 09 data into MYSQL 
                Process_UTMC05_09();
                Log.Write(DateTime.Now.ToString() + " - step 23 - Import fund level RPT 05 - 09 data into MYSQL ", End_date);



                // step 25 - Import member level RPT 11 Adjust RPT 18 MYSQL 
                Process_UTMC18_Adjust();
                Log.Write(DateTime.Now.ToString() + " - step 25 - Import member level RPT 11 Adjust EPF Daily NAV into RPT 18 last day of the month ", End_date);



              //  MessageBox.Show("Completed!!!");



            }
            catch (Exception ex)
            {
                Log.Write(DateTime.Now.ToString() + " - Final export err (" + ex.ToString() + ")", End_date);

            }

        }

        private void btnUTMC16_Click(object sender, EventArgs e)
        {
            Process_UTMC16();
        }

        private void btnUTMC18_Click(object sender, EventArgs e)
        {
            Process_UTMC18();
        }

        private void btnUTMC12_Click(object sender, EventArgs e)
        {
            Process_UTMC12();
        }

        private void btnUTMC17_Click(object sender, EventArgs e)
        {
            Process_UTMC17();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Process_UTMC05_09();
        }

        private void btnUTMC15_Click(object sender, EventArgs e)
        {
            Process_UTMC15();
        }

        private void btnUTMC11_Click(object sender, EventArgs e)
        {
            Process_UTMC11();
        }

        private void btnUTMC10_Click(object sender, EventArgs e)
        {
            Process_UTMC10();
        }

        private void btnUTMC14_Click(object sender, EventArgs e)
        {
            Process_UTMC14();
        }

        private void btnUTMC13_Click(object sender, EventArgs e)
        {
            Process_UTMC13();
        }

        private void btnUTMC_03_Click(object sender, EventArgs e)
        {
            Process_UTMC03();
        }

        private void btnAdjustment_Click(object sender, EventArgs e)
        {
            Process_UTMC03_Adjust();

            Process_UTMC10_Adjust();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Process_UTMC15_Adjust();

            Process_UTMC18_Adjust();


        }


        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                // step 10 - Import member level RPT 15 data into mysql 
                Process_UTMC15();
                Log.Write(DateTime.Now.ToString() + " - step 20 - Import member level Rpt 15 data into mysql  ", End_date);

                

                System.Threading.Thread.Sleep(500);
                // step 11 - Import fund level RPT 11 data into MYSQL 
                Process_UTMC11();
                Log.Write(DateTime.Now.ToString() + " - step 11 - Import member level RPT 11 data into MYSQL ", End_date);


                if ((checkBox1.Checked))
                {
                    // step 10.1 - Import 0 unit member level for RPT 15 data into mysql 
                    Process_UTMC15_ZeroUnit();
                    Log.Write(DateTime.Now.ToString() + " - step 21 - Import 0 unit member level for RPT 15 data into mysql  ", End_date);

                    Process_UTMC11_ZeroUnit();
                    Log.Write(DateTime.Now.ToString() + " - step 11.5 - Import 0 UNIT member level RPT 11 data into MYSQL ", End_date);

                    // step 17 - Import member level RPT 15 Adjust 0 Unit into MYSQL 
                    Process_UTMC15_Adjust();
                    Log.Write(DateTime.Now.ToString() + " - step 17 - Import member level RPT 15 Adjust 0 Unit data into MYSQL ", End_date);

                }


                
                System.Threading.Thread.Sleep(500);
                
                // step 12 - Import member level RPT 10 data into MYSQL 
                Process_UTMC10();
                Log.Write(DateTime.Now.ToString() + " - step 12 - Import member level RPT 10 data into MYSQL   ", End_date);

                // step 13 - Import member level RPT 14 data into MYSQL 
                Process_UTMC14();
                Log.Write(DateTime.Now.ToString() + " - step 13 - Import member level RPT 14 data into MYSQL ", End_date);

                // step 14 - Import member level RPT 13 into MYSQL 
                Process_UTMC13();
                Log.Write(DateTime.Now.ToString() + " - step 14 - Import member level RPT 13 data into MYSQL ", End_date);


                // step 15 - Import member level RPT 03 data into MYSQL 
                Process_UTMC03();
                Log.Write(DateTime.Now.ToString() + " - step 15 - Import member level RPT 03 data into MYSQL ", End_date);

                System.Threading.Thread.Sleep(500);
                
                // step 16 - Import member level RPT 13 Adjust 0 Unit into MYSQL 
                Process_UTMC03_Adjust();
                Log.Write(DateTime.Now.ToString() + " - step 16 - Import member level RPT 13 Adjust 0 Unit data into MYSQL ", End_date);

                
                Process_UTMC10_Adjust();
                Log.Write(DateTime.Now.ToString() + " - step 18 - Import member level RPT 10 reversal transaction data into MYSQL ", End_date);



                // hardcode adjustment
                string strQuery = "";

                strQuery = "Update utmc_member_information set Adjustment = 'S' where EPF_No = '11667355'";

                SQL_Execute_NonQuery(strQuery);

                // hardcode adjustment


             //   MessageBox.Show("Completed!!!");



            }
            catch (Exception ex)
            {
                Log.Write(DateTime.Now.ToString() + " - Final export err (" + ex.ToString() + ")", End_date);

            }

        }

        /// <summary> 
        /// Get the last day of the month for any 
        /// full date 
        /// </summary> 
        /// <param name="dtDate"></param> 
        /// <returns></returns> 
        private DateTime GetLastDayOfMonth(DateTime dtDate)
        {
            // set return value to the last day of the month 
            // for any date passed in to the method 

            // create a datetime variable set to the passed in date 
            DateTime dtTo = dtDate;

            // overshoot the date by a month 
            dtTo = dtTo.AddMonths(1);

            // remove all of the days in the next month 
            // to get bumped down to the last day of the 
            // previous month 
            dtTo = dtTo.AddDays(-(dtTo.Day));

            // return the last day of the month 
            return dtTo;
        }

        private void Process_UTMC11_ZeroUnit()
        {
            string IPD_Fund_Code = "";
            string IPD_Member_Acc_No = "";
            string ConnectionString = GetSqlConnection();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(ConnectionString))
                {
                    conn.Open();

                   

                    string mysql = "SELECT * FROM eppa_wbr1.utmc_member_investment "
                                    +"where Report_Date = '"+End_date+"' and units = 0 ";




                    string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                    using (MySqlCommand comm = new MySqlCommand(mysql, conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(ConnectionString))
                            {
                                mysql_conn.Open();

                                while (rdr.Read())
                                {
                                    string Member_EPF_No = rdr[0].ToString();

                                    IPD_Fund_Code = rdr[2].ToString();
                                    IPD_Member_Acc_No = rdr[3].ToString();
                                    
                                    string Query = "INSERT IGNORE INTO utmc_compositional_investment(EPF_IPD_Code, IPD_Fund_Code, Member_EPF_No, IPD_Member_Acc_No, Effective_Date, Opening_Balance_Units, Opening_Balance_Cost_RM, Opening_Balance_Date, Net_Cumulative_Closing_Balance_Units,Net_Cumulative_Closing_Balance_Cost_RM,Net_Cumulative_Closing_Balance_Date,Market_Price_NAV,Market_Price_Effective_Date,Unrealised_Gain_Loss_RM,Report_Date,Report_Key)"
                                  + " VALUES ('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Member_EPF_No + "','" + IPD_Member_Acc_No + "','" + End_date + "',0,0,'" + Start_date + "',0,0,'" + End_date + "',0,'" + End_date + "',0,'" + End_date + "','" + End_date + "');";

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        mysql_comm.ExecuteNonQuery();
                                        Log.Write("UTMC_011 (0 unit) : " + IPD_Fund_Code + " " + IPD_Member_Acc_No + "Inserted", End_date);

                                        //System.Threading.Thread.Sleep(20);
                                    }
                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }

                System.Threading.Thread.Sleep(2000);

                // ReadHolderNoforutmc15();
            }
            catch (Exception ex)
            {
                Log.Write("UTMC_011 (0 unit) : " + IPD_Fund_Code + " " + IPD_Member_Acc_No + " - Insert Failed", End_date);

                ex.ToString();
            }
        }


        private void Process_UTMC_Final_Patch()
        {
            string ConnectionString = GetSqlConnection();
            string Query = "";
            try
            {
                using (MySqlConnection conn = new MySqlConnection(ConnectionString))
                {
                    conn.Open();

                    string mysql = "SELECT Daily_NAV_Date FROM eppa_wbr1.utmc_daily_nav "
                                    +" WHERE Report_Date = '"+End_date+"' "
                                    +" and Service_Charge_RM > 0; ";


                    string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                    DateTime dt = Convert.ToDateTime(End_date);
                    using (MySqlCommand comm = new MySqlCommand(mysql, conn))
                    {



                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(ConnectionString))
                            {
                                mysql_conn.Open();

                                while (rdr.Read())
                                {
                                    string strLastDaily_NAV_Date = End_date;

                                    if (rdr.HasRows)
                                    {

                                        //  update rpt 11 - Last BUSINESS Day FOR RPT 11 Net_Cumulative_Closing_Balance_Date &  Market_Price_Effective_Date                                       
                                        dt = Convert.ToDateTime(rdr[0].ToString());
                                        strLastDaily_NAV_Date = dt.ToString("yyyy-MM-dd");

                                        Query = "update eppa_wbr1.utmc_compositional_investment set Market_Price_Effective_Date = '" + strLastDaily_NAV_Date + "', "
                                                    + " Net_Cumulative_Closing_Balance_Date = '" + strLastDaily_NAV_Date + "' "
                                                    +" where Report_Date = '"+End_date+"' ";
                                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                        {
                                            mysql_comm.ExecuteNonQuery();
                                            Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                            //System.Threading.Thread.Sleep(20);
                                        }

                                            // PATCH RPT 15 MARKET VALUE AS RPT 11 MARKET VALUE

                                        string subQuery = "UPDATE utmc_member_investment A "
                                            + "INNER JOIN utmc_compositional_investment B "
                                            + "ON A.Report_Date = B.Report_Date "
                                            + "AND A.IPD_FUND_CODE = B.IPD_FUND_CODE "
                                            + "AND A.IPD_MEMBER_ACC_NO = B.IPD_MEMBER_ACC_NO "
                                            + "AND A.Report_Date = '" + End_date + "' "
                                            + "SET A.Market_Value = IFNULL(round(Market_PRICE_NAV * B.Net_Cumulative_Closing_Balance_Units,2), 0); ";
    

                                 using (MySqlCommand mysql_comm = new MySqlCommand(subQuery, mysql_conn))
                                {
                                    mysql_comm.ExecuteNonQuery();
                                    Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                    //System.Threading.Thread.Sleep(20);
                                }



                                        //  update rpt 12 - Last Calendar Day DAIRY_NAV based on level 15 sum market value 
                                        Query = "UPDATE utmc_daily_nav SET Daily_NAV = "
                                                        + "( "
                                                        + "SELECT  sum(Market_Value) FROM utmc_member_investment "
                                                        + "where Report_Date = '" + End_date + "' "
                                                        + "group by Report_Date "
                                                        + ") "
                                                        + "WHERE  Report_Date = '" + End_date + "' "
                                                        + "AND Daily_NAV_Date >= '" + strLastDaily_NAV_Date + "' ";



                                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                        {
                                            mysql_comm.ExecuteNonQuery();
                                            Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                            //System.Threading.Thread.Sleep(20);
                                        }


                                        // PATCH RPT 18 DAILY_NAV AS RPT 15 MARKET VALUE


                                        Query = "update utmc_daily_nav_fund A "
                                                    + "    SET A.Daily_NAV_EPF = "
                                                    + "( "
                                                    + "SELECT sum(B.Market_Value) FROM utmc_member_investment B "
                                                    + "where B.Report_Date = '" + End_date + "' "
                                                    + "and A.IPD_Fund_Code = B.IPD_Fund_Code "
                                                    + "group by B.IPD_Fund_Code "
                                                    + " ) "
                                                    + "where A.Report_Date = '" + End_date + "' "
                                                    + "and A.Daily_NAV_Date = '" + strLastDaily_NAV_Date + "' ";

     

                                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                        {
                                            mysql_comm.ExecuteNonQuery();
                                            Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                            //System.Threading.Thread.Sleep(20);
                                        }


                                        Query = "update utmc_daily_nav_fund A "
                                                + "SET  A.Daily_Unit_Created_EPF = ROUND(A.Daily_NAV_EPF / A.Daily_Unit_Price, 4) "
                                            + "where A.Report_Date = '" + End_date + "' "
                                            + "and A.Daily_NAV_Date = '" + strLastDaily_NAV_Date + "' ";

                                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                        {
                                            mysql_comm.ExecuteNonQuery();
                                            Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                            //System.Threading.Thread.Sleep(20);
                                        }

                                   



                                    }
                                   
                                }
                                rdr.Close();

                                // update rpt 11 0 unit 
                                Query = "update eppa_wbr1.utmc_compositional_investment set Market_Price_NAV = 0 "
                                                + "where Report_Date = '" + End_date + "' and Net_Cumulative_Closing_Balance_Units = 0 ";
                                using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                {
                                    mysql_comm.ExecuteNonQuery();
                                    Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                    //System.Threading.Thread.Sleep(20);
                                }

                                // update rpt 03 before 2017 = 'C'
                                if (Convert.ToInt16(End_date.Substring(0, 4)) < 2017)
                                {
                                      Query = "update eppa_wbr1.utmc_member_information set NIC = 'C', "
                                        +"NIC_Date = '1900-01-01' "
                                        +"where Report_Date = '"+End_date+"'";

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        mysql_comm.ExecuteNonQuery();
                                        Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                        //System.Threading.Thread.Sleep(20);
                                    }

                                }
                                






                                // update rpt 18 - Last Calendar Day EPF_NAV based on fund level 15 sum market value

                                // PATCH LAST CALENDAR DAY

                                DateTime dtLastCalendarDay = Convert.ToDateTime(End_date);
                                DateTime dtLastWorkingDay = Convert.ToDateTime(dt);
                                int intLastWorkingDay = dtLastWorkingDay.Day;
                                int intLastCalendarDay = dtLastCalendarDay.Day;

                                int k = 0;
                                

                                for (int i = intLastWorkingDay; i < intLastCalendarDay+1; i++)
                                {
                                    k = k + 1; 

                                    string strLastWorkingDay = dtLastWorkingDay.AddDays(k).ToString("yyyy-MM-dd");

                                    Query = "update utmc_daily_nav set Daily_NAV = "
                                        + "(select sum(Daily_NAV_EPF) from utmc_daily_nav_fund where Daily_NAV_Date = '" + strLastWorkingDay + "' group by Daily_NAV_Date ) "
                                        + "where Report_Date = '" + End_date + "' "
                                        + "AND Daily_NAV_Date = '"+strLastWorkingDay+"'; ";

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        mysql_comm.ExecuteNonQuery();
                                        Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                        //System.Threading.Thread.Sleep(20);
                                    }

                                }



                                    // PATCH LAST CALENDAR DAY END
                                


                              


                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }

                System.Threading.Thread.Sleep(200);

                ReadActualCostUTMC15();
            }
            catch (Exception ex)
            {
                Log.Write("UTMC_Final_PATCH : " + Query  + " - Update Failed", End_date);

                ex.ToString();
            }
        }

        private void Process_UTMC_Final_Patch2()
        {
            string ConnectionString = GetSqlConnection();
            string Query = "";
            try
            {
                using (MySqlConnection conn = new MySqlConnection(ConnectionString))
                {
                    conn.Open();

                    string mysql = "SELECT Daily_NAV_Date FROM eppa_wbr1.utmc_daily_nav "
                                    + " WHERE Report_Date = '" + End_date + "' "
                                    + " and Service_Charge_RM > 0; ";


                    string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                    DateTime dt = Convert.ToDateTime(End_date);
                    using (MySqlCommand comm = new MySqlCommand(mysql, conn))
                    {



                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(ConnectionString))
                            {
                                mysql_conn.Open();

                                while (rdr.Read())
                                {
                                    string strLastDaily_NAV_Date = End_date;

                                    if (rdr.HasRows)
                                    {

                                        //  update rpt 11 - Last BUSINESS Day FOR RPT 11 Net_Cumulative_Closing_Balance_Date &  Market_Price_Effective_Date                                       
                                        dt = Convert.ToDateTime(rdr[0].ToString());
                                        strLastDaily_NAV_Date = dt.ToString("yyyy-MM-dd");

                                        Query = "update eppa_wbr1.utmc_compositional_investment set Market_Price_Effective_Date = '" + strLastDaily_NAV_Date + "', "
                                                    + " Net_Cumulative_Closing_Balance_Date = '" + strLastDaily_NAV_Date + "' "
                                                    + " where Report_Date = '" + End_date + "' ";
                                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                        {
                                            mysql_comm.ExecuteNonQuery();
                                            Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                            //System.Threading.Thread.Sleep(20);
                                        }

                                        // PATCH RPT 15 MARKET VALUE AS RPT 11 MARKET VALUE

                                        string subQuery = "UPDATE utmc_member_investment A "
                                            + "INNER JOIN utmc_compositional_investment B "
                                            + "ON A.Report_Date = B.Report_Date "
                                            + "AND A.IPD_FUND_CODE = B.IPD_FUND_CODE "
                                            + "AND A.IPD_MEMBER_ACC_NO = B.IPD_MEMBER_ACC_NO "
                                            + "AND A.Report_Date = '" + End_date + "' "
                                            + "SET A.Market_Value = IFNULL(round(Market_PRICE_NAV * B.Net_Cumulative_Closing_Balance_Units,2), 0); ";


                                        using (MySqlCommand mysql_comm = new MySqlCommand(subQuery, mysql_conn))
                                        {
                                            mysql_comm.ExecuteNonQuery();
                                            Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                            //System.Threading.Thread.Sleep(20);
                                        }



                                        //  update rpt 12 - Last Calendar Day DAIRY_NAV based on level 15 sum market value 
                                        Query = "UPDATE utmc_daily_nav SET Daily_NAV = "
                                                        + "( "
                                                        + "SELECT  sum(Market_Value) FROM utmc_member_investment "
                                                        + "where Report_Date = '" + End_date + "' "
                                                        + "group by Report_Date "
                                                        + ") "
                                                        + "WHERE  Report_Date = '" + End_date + "' "
                                                        + "AND Daily_NAV_Date >= '" + strLastDaily_NAV_Date + "' ";



                                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                        {
                                            mysql_comm.ExecuteNonQuery();
                                            Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                            //System.Threading.Thread.Sleep(20);
                                        }


                                        // PATCH RPT 18 DAILY_NAV AS RPT 15 MARKET VALUE


                                        Query = "update utmc_daily_nav_fund A "
                                                    + "    SET A.Daily_NAV_EPF = "
                                                    + "( "
                                                    + "SELECT sum(B.Market_Value) FROM utmc_member_investment B "
                                                    + "where B.Report_Date = '" + End_date + "' "
                                                    + "and A.IPD_Fund_Code = B.IPD_Fund_Code "
                                                    + "group by B.IPD_Fund_Code "
                                                    + " ) "
                                                    + "where A.Report_Date = '" + End_date + "' "
                                                    + "and A.Daily_NAV_Date = '" + strLastDaily_NAV_Date + "' ";



                                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                        {
                                            mysql_comm.ExecuteNonQuery();
                                            Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                            //System.Threading.Thread.Sleep(20);
                                        }


                                        Query = "update utmc_daily_nav_fund A "
                                                + "SET  A.Daily_Unit_Created_EPF = ROUND(A.Daily_NAV_EPF / A.Daily_Unit_Price, 4) "
                                            + "where A.Report_Date = '" + End_date + "' "
                                            + "and A.Daily_NAV_Date = '" + strLastDaily_NAV_Date + "' ";

                                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                        {
                                            mysql_comm.ExecuteNonQuery();
                                            Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                            //System.Threading.Thread.Sleep(20);
                                        }





                                    }

                                }
                                rdr.Close();

                                // update rpt 11 0 unit 
                                Query = "update eppa_wbr1.utmc_compositional_investment set Market_Price_NAV = 0 "
                                                + "where Report_Date = '" + End_date + "' and Net_Cumulative_Closing_Balance_Units = 0 ";
                                using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                {
                                    mysql_comm.ExecuteNonQuery();
                                    Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                    //System.Threading.Thread.Sleep(20);
                                }

                                // update rpt 03 before 2017 = 'C'
                                if (Convert.ToInt16(End_date.Substring(0, 4)) < 2017)
                                {
                                    Query = "update eppa_wbr1.utmc_member_information set NIC = 'C', "
                                      + "NIC_Date = '1900-01-01' "
                                      + "where Report_Date = '" + End_date + "'";

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        mysql_comm.ExecuteNonQuery();
                                        Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                        //System.Threading.Thread.Sleep(20);
                                    }

                                }







                                // update rpt 18 - Last Calendar Day EPF_NAV based on fund level 15 sum market value

                                // PATCH LAST CALENDAR DAY

                                DateTime dtLastCalendarDay = Convert.ToDateTime(End_date);
                                DateTime dtLastWorkingDay = Convert.ToDateTime(dt);
                                int intLastWorkingDay = dtLastWorkingDay.Day;
                                int intLastCalendarDay = dtLastCalendarDay.Day;

                                int k = 0;


                                for (int i = intLastWorkingDay; i < intLastCalendarDay + 1; i++)
                                {
                                    k = k + 1;

                                    string strLastWorkingDay = dtLastWorkingDay.AddDays(k).ToString("yyyy-MM-dd");

                                    Query = "update utmc_daily_nav set Daily_NAV = "
                                        + "(select sum(Daily_NAV_EPF) from utmc_daily_nav_fund where Daily_NAV_Date = '" + strLastWorkingDay + "' group by Daily_NAV_Date ) "
                                        + "where Report_Date = '" + End_date + "' "
                                        + "AND Daily_NAV_Date = '" + strLastWorkingDay + "'; ";

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        mysql_comm.ExecuteNonQuery();
                                        Log.Write("UTMC_Final_PATCH : " + Query + " - Update Successed", End_date);

                                        //System.Threading.Thread.Sleep(20);
                                    }

                                }



                                // PATCH LAST CALENDAR DAY END



                                




                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }

                System.Threading.Thread.Sleep(200);

                ReadActualCostUTMC15();
            }
            catch (Exception ex)
            {
                Log.Write("UTMC_Final_PATCH : " + Query + " - Update Failed", End_date);

                ex.ToString();
            }
        }



        private void Process_UTMC15_ZeroUnit()
        {
            string IPD_Fund_Code = "";
            string IPD_Member_Acc_No = "";

            try
            {
                string ConnectionString = GetOracleConnection();
                string Sqlconnstring = GetSqlConnection();


                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    string NAV_PricePU_Dt = End_date;
                    NAV_PricePU_Dt = NAV_PricePU_Dt.Replace("/", "-");
                    BirthDateupdate = BirthDateupdate.Replace("/", "-");

                    //NAV_PricePU_Dt = "2016-08-30";

                    string mysql = "select S3.ID_NO_2 AS EPF_NO, S1.FUND_ID,s1.HOLDER_NO, S1.TRANS_UNITS,S1.TRANS_AMT, s1.TRANS_DT, s1.CUR_UNIT_HLDG, S3.ID_NO, s3.BIRTH_DT, S1.TRANS_TYPE,S1.TRANS_PR  from UTS.HOLDER_LEDGER s1 "
                        +"inner join  UTS.HOLDER_REG s3 on   s1.HOLDER_NO = s3.HOLDER_NO  "
                        +"inner join (   select max(SORT_SEQ) SORT_SEQ, MAX(TRANS_DT) TRANS_DT, FUND_ID,    HOLDER_NO   from UTS.HOLDER_LEDGER   "
                        +"WHERE TRANS_DT <= to_date('"+End_date+"', 'yyyy/mm/dd')  group by HOLDER_NO,FUND_ID    ) s2     on s1.HOLDER_NO = s2.HOLDER_NO  and s1.SORT_SEQ = s2.SORT_SEQ  "
                        +"where   s1.CUR_UNIT_HLDG = 0 "
                        +"AND NOT S1.HOLDER_NO IN ('9504', '19765')  "
                        + "AND s3.HOLDER_CLS IN ('EB','EN','ER','ES','EZ','EP')    AND S3.BIRTH_DT > to_date('" + BirthDateupdate + "', 'yyyy/mm/dd')   "
                        + "AND S1.FUND_ID IN ('01','02','03','04','05','06','07')  AND s1.TRANS_DT <= to_date('" + End_date + "', 'yyyy/mm/dd')  "
                        +"AND S3.HOLDER_STATUS = 'A' "
                        +"ORDER BY S1.TRANS_DT, S1.FUND_ID, S1.HOLDER_NO ";


                    //string mysql = "select S3.ID_NO_2 AS EPF_NO, S1.FUND_ID,s1.HOLDER_NO,"
                    //                 + " S1.TRANS_UNITS,S1.TRANS_AMT, s1.TRANS_DT, s1.CUR_UNIT_HLDG, S3.ID_NO,"
                    //                 + " s3.BIRTH_DT, S1.TRANS_TYPE,S1.TRANS_PR "
                    //                    + " from UTS.HOLDER_LEDGER s1 inner join  UTS.HOLDER_REG s3 on "
                    //                  + "  s1.HOLDER_NO = s3.HOLDER_NO inner join "
                    //                 + " (   select max(SORT_SEQ) SORT_SEQ, MAX(TRANS_DT) TRANS_DT, FUND_ID,    HOLDER_NO "
                    //                 + "  from UTS.HOLDER_LEDGER "
                    //                 + "  WHERE TRANS_DT < to_date('" + TransDateUpdate + "', 'yyyy/mm/dd')"
                    //                 + "  group by HOLDER_NO,FUND_ID  "
                    //                 + "  ) s2    "
                    //                 + " on s1.HOLDER_NO = s2.HOLDER_NO  and s1.SORT_SEQ = s2.SORT_SEQ "  // adjustment .. AND s1.TRANS_DT = s2.TRANS_DT"
                    //                 + " where   s1.CUR_UNIT_HLDG = 0  AND NOT S1.TRANS_TYPE IN ('TR')  AND s3.HOLDER_CLS IN ('BE')  "
                    //                 + " AND S3.BIRTH_DT > to_date('" + BirthDateupdate + "', 'yyyy/mm/dd') "
                    //                 + " AND S1.FUND_ID IN ('AS') AND s1.TRANS_DT < to_date('" + TransDateUpdate + "', 'yyyy/mm/dd')  "
                    //                 + " ORDER BY S1.FUND_ID, S1.HOLDER_NO";







                    string EPF_IPD_Code = Read_IPD_info_for_utmc01();
                    using (OracleCommand comm = new OracleCommand(mysql, conn))
                    {
                        using (OracleDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();

                                while (rdr.Read())
                                {
                                    string Member_EPF_No = rdr[0].ToString();

                                    IPD_Fund_Code = rdr[1].ToString();
                                    IPD_Member_Acc_No = rdr[2].ToString();
                                    string Actual_Transferred_From_EPF_RM = "0";
                                    string Units = rdr[6].ToString();
                                    string Book_Value = "0";
                                    string Market_Value = "0";


                                    DateTime dt = Convert.ToDateTime(rdr[5].ToString());
                                    string TransDate = dt.ToString("yyyy-MM-dd");

                                    DateTime now = Convert.ToDateTime(rdr[5].ToString());
                                    var startDate = new DateTime(now.Year, now.Month, 1);
                                    var endDate = startDate.AddMonths(1).AddDays(-1);
                                    string Effective_Date = End_date;

                                    string Query = "Insert ignore into utmc_member_investment(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Actual_Transferred_From_EPF_RM,Units,Book_Value,Market_Value,Effective_Date,Report_Date) "
                                   + " values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "', '" + IPD_Member_Acc_No + "','" + Actual_Transferred_From_EPF_RM + "','" + Units + "','" + Book_Value + "','" + Market_Value + "','" + End_date + "','" + End_date + "')";

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        mysql_comm.ExecuteNonQuery();
                                        Log.Write("UTMC_015 (0 unit) : " + IPD_Fund_Code + " " + IPD_Member_Acc_No + "Inserted", End_date);
                                        
                                        //System.Threading.Thread.Sleep(20);
                                    }
                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                    }
                    conn.Close();
                }

                System.Threading.Thread.Sleep(2000);

                // ReadHolderNoforutmc15();
            }
            catch (Exception ex)
            {
                Log.Write("UTMC_015 (0 unit) : " + IPD_Fund_Code + " " + IPD_Member_Acc_No + " - Insert Failed", End_date);

                ex.ToString();
            }
        }

        private DateTime GetFirstDayOfMonth(DateTime dtDate)
        {
            // set return value to the last day of the month 
            // for any date passed in to the method 

            // create a datetime variable set to the passed in date 
            DateTime dtFrom = dtDate;

            // remove all of the days in the month 
            // except the first day and set the 
            // variable to hold that date 
            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1));

            // return the first day of the month 
            return dtFrom;
        }

        public struct DateTimeSpan
        {
            private readonly int years;
            private readonly int months;
            private readonly int days;
            private readonly int hours;
            private readonly int minutes;
            private readonly int seconds;
            private readonly int milliseconds;

            public DateTimeSpan(int years, int months, int days, int hours, int minutes, int seconds, int milliseconds)
            {
                this.years = years;
                this.months = months;
                this.days = days;
                this.hours = hours;
                this.minutes = minutes;
                this.seconds = seconds;
                this.milliseconds = milliseconds;
            }

            public int Years { get { return years; } }
            public int Months { get { return months; } }
            public int Days { get { return days; } }
            public int Hours { get { return hours; } }
            public int Minutes { get { return minutes; } }
            public int Seconds { get { return seconds; } }
            public int Milliseconds { get { return milliseconds; } }

            enum Phase { Years, Months, Days, Done }

            public static DateTimeSpan CompareDates(DateTime date1, DateTime date2)
            {
                if (date2 < date1)
                {
                    var sub = date1;
                    date1 = date2;
                    date2 = sub;
                }

                DateTime current = date1;
                int years = 0;
                int months = 0;
                int days = 0;

                Phase phase = Phase.Years;
                DateTimeSpan span = new DateTimeSpan();
                int officialDay = current.Day;

                while (phase != Phase.Done)
                {
                    switch (phase)
                    {
                        case Phase.Years:
                            if (current.AddYears(years + 1) > date2)
                            {
                                phase = Phase.Months;
                                current = current.AddYears(years);
                            }
                            else
                            {
                                years++;
                            }
                            break;
                        case Phase.Months:
                            if (current.AddMonths(months + 1) > date2)
                            {
                                phase = Phase.Days;
                                current = current.AddMonths(months);
                                if (current.Day < officialDay && officialDay <= DateTime.DaysInMonth(current.Year, current.Month))
                                    current = current.AddDays(officialDay - current.Day);
                            }
                            else
                            {
                                months++;
                            }
                            break;
                        case Phase.Days:
                            if (current.AddDays(days + 1) > date2)
                            {
                                current = current.AddDays(days);
                                var timespan = date2 - current;
                                span = new DateTimeSpan(years, months, days, timespan.Hours, timespan.Minutes, timespan.Seconds, timespan.Milliseconds);
                                phase = Phase.Done;
                            }
                            else
                            {
                                days++;
                            }
                            break;
                    }
                }

                return span;
            }
        }
   

        private void button7_Click(object sender, EventArgs e)
        {

            try
            {

                Start_date = StartDate.Text;
                End_date = EndDate.Text;


                string[] dateParts = StartDate.Text.Split('/');
                DateTime dtFirstTemp = new
                    DateTime(Convert.ToInt32(dateParts[0]),
                    Convert.ToInt32(dateParts[1]),
                    Convert.ToInt32(dateParts[2]));

                string[] dateParts2 = EndDate.Text.Split('/');
                DateTime dtLastTemp = new
                    DateTime(Convert.ToInt32(dateParts2[0]),
                    Convert.ToInt32(dateParts2[1]),
                    Convert.ToInt32(dateParts2[2]));

                Start_date = GetFirstDayOfMonth(dtFirstTemp).ToString("yyyy-MM-dd");
                End_date = GetLastDayOfMonth(dtLastTemp).ToString("yyyy-MM-dd");

                DateSelection.Text = "Import record between " + Start_date + " to " + End_date + "";

                var dateSpan = DateTimeSpan.CompareDates(dtFirstTemp, dtLastTemp);
                int NoOfMonthSpan = (Convert.ToInt32(dateSpan.Years) * 12) + Convert.ToInt32(dateSpan.Months);

                for (int i = 0; i <= NoOfMonthSpan; i++)
                {

                    DateTime dtProcessTemp;
                    dtProcessTemp = dtFirstTemp.AddMonths(i);

                    Start_date = GetFirstDayOfMonth(dtProcessTemp).ToString("yyyy-MM-dd");
                    End_date = GetLastDayOfMonth(dtProcessTemp).ToString("yyyy-MM-dd");

                    //MessageBox.Show(Start_date + " - " + i);

                    // DateSelection.Text = "Import record between " + Start_date + " to " + End_date + "";

                    DateTime date = Convert.ToDateTime(End_date);
                    var nextMonth = new DateTime(date.Year, date.Month, 1).AddMonths(1);




                    string[] datePartsDOB = dtProcessTemp.ToString("yyyy-MM-dd").Split('-');

                    int upper = datePartsDOB.GetUpperBound(0);
                    if (upper == 2)
                    {
                        int tmpYear = Convert.ToInt32(datePartsDOB[0]) - 55;
                        int tmpMonth = Convert.ToInt32(datePartsDOB[1]) - 1;
                        if (tmpMonth == 0)
                        {
                            tmpMonth = 12;
                            tmpYear = tmpYear - 1;
                        }
                        if (tmpMonth == 13)
                        {
                            tmpMonth = 1;
                            tmpYear = tmpYear + 1;
                        }
                        int tmpDay = 1;

                        DateTime dtLastTemp2 = new
                            DateTime(tmpYear, tmpMonth, tmpDay);
                        BirthDateupdate = GetLastDayOfMonth(dtLastTemp2).ToString("yyyy-MM-dd");

                    }


                    if (Convert.ToInt32(BirthDateupdate.Replace("-", "")) < 19610831)
                    //if (Convert.ToInt32(BirthDateupdate.Replace("-", "")) < 19600831)
                    {
                        BirthDateupdate = "1961-08-31";
                    }


                    //MessageBox.Show(End_date);
                    //MessageBox.Show(Start_date);


                    TransDateUpdate = nextMonth.ToString("yyyy-MM-dd");


                    //Process_UTMC18_NAV_Adjust();


                    button6_Click(sender, e);
                    button5_Click(sender, e);

                    // hardcode for decease case
                    Process_UTMC03_AdjustDecease();

                    //GORDON - 08  FIXING REPORT 03 INVALID NIC_DATE VS REPORT DATE
                    Process_UTMC03_Adjust08();

                    //GORDON - 11 AdjustZEROUnit  ... ... fix rpt 03 NIC date > report date
                    Log.Write("Process_UTMC11_AdjustZEROUnit - Start", End_date);
            
                    Process_UTMC11_AdjustZEROUnit();

                    // ADJUST 12, 18 BASED ON 15... EFFECTIVE DATE AND SO ON
                    Process_UTMC_Final_Patch();

                    // ADJUST 18 BASED ON 10 DAILY MOVEMENT
                    Process_UTMC_Final_Patch2();

                    // only for hisotrical rpt 18 based on rpt 10
                    if (Convert.ToInt32(End_date.Replace("-", "").ToString()) < 20160901)
                    {

                        Process_UTMC18_NAV_Adjust_OLD();
                    }

                    if ((checkBox2.Checked))
                    {
                       // MessageBox.Show("HAHA!!!");

                        Process_UTMC18_NAV_Adjust();


                    }



                   // update 11 based on report working  murali // 2018-03-31

                    if ((checkBox3.Checked))
                    {
                        UpdateMarketPerNaV();
                        UpdateReport12Final();
                        UpdateReport18Final();
                    }


                    SQL_Execute_NonQuery(@"update utmc_compositional_investment set 
                                         Unrealised_Gain_Loss_RM = round(((Net_Cumulative_Closing_Balance_Units * Market_Price_NAV) - Net_Cumulative_Closing_Balance_Cost_RM),2)
                                         where Report_Date = '" + End_date+"';");

                    // BEFORE EXPORT



                    //patching
                    SQL_Execute_NonQuery(@"delete from utmc_compositional_investment where IPD_Member_Acc_No='21448' and Report_Date >='2021-01-31';");
                    SQL_Execute_NonQuery(@"delete from utmc_member_information where IPD_Member_Acc_No='21448' and Report_Date >='2021-01-31';");
                    SQL_Execute_NonQuery(@"delete from utmc_member_investment where IPD_Member_Acc_No='21448' and Report_Date >='2021-01-31';");

                   

                   //patching 23022022

                     SQL_Execute_NonQuery(@"update utmc_compositional_transactions set Reversed_Transaction_ID=replace(Reversed_Transaction_ID,'X','') where Reversed_Transaction_ID !='' and Report_Date >='2022-02-28';");
                     SQL_Execute_NonQuery(@"update utmc_release_control set Reversed_Transaction_ID=replace(Reversed_Transaction_ID,'X','') where Reversed_Transaction_ID !='' and Report_Date >='2022-02-28';");
                     SQL_Execute_NonQuery(@"update utmc_transferred set Reversed_Transaction_ID=replace(Reversed_Transaction_ID,'X','') where Reversed_Transaction_ID !='' and Report_Date >='2022-02-28';");
                     SQL_Execute_NonQuery(@"update utmc_compositional_transactions set Date_Of_Settlement =Date_Of_Transaction where  Report_Date >='2022-02-28';");


                    button4_Click(sender, e);
                
                }

              


              //  button5_Click(sender, e);
             //   button6_Click(sender, e);
             //   button4_Click(sender, e);
                
                MessageBox.Show("Completed!!!");



            }
            catch (Exception ex)
            {
                Log.Write(DateTime.Now.ToString() + " - Final Main err (" + ex.ToString() + ")", End_date);

            }

        }

        private void button8_Click(object sender, EventArgs e)
        {
            Process_UTMC18_NAV_Adjust();
            MessageBox.Show("Completed!!!");

        }

        private void button9_Click(object sender, EventArgs e)
        {
            Process_UTMC03_AdjustDecease();
        }

        private void button10_Click(object sender, EventArgs e)
        {

            Start_date = StartDate.Text;
                End_date = EndDate.Text;


            //
            // Dialog box with two buttons: yes and no. [3]
            //
                DialogResult result1 = MessageBox.Show("Are you sure you want to purge all UTMC report data between " + Start_date + " and " + End_date + " ?",
    "Critical Warning",
    MessageBoxButtons.OKCancel,
    MessageBoxIcon.Warning,
    MessageBoxDefaultButton.Button2,
    MessageBoxOptions.RightAlign,
    true);

                if (result1 == DialogResult.OK)
                {
                    Process_UTMC_Purge();
                    MessageBox.Show("Successfully purge all UTMC report data between " + Start_date + " and " + End_date + ".", "Purge_UTMC", MessageBoxButtons.OK, MessageBoxIcon.Information);
    
                }
                else
                {

                }




        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                this.textBox1.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {

            DialogResult result1 = MessageBox.Show("Are you sure you want to find all " + this.textBox2.Text + " and replace all with " + this.textBox3.Text + "?",
"Critical Warning",
MessageBoxButtons.OKCancel,
MessageBoxIcon.Warning,
MessageBoxDefaultButton.Button2,
MessageBoxOptions.RightAlign,
true);

            if (result1 == DialogResult.OK)
            {
                FindReplace(this.textBox1.Text);
              
            }
            else
            {

            }

           
        }


        private void FindReplace(string strTxtFileFolderPath)
        {
            string strFind = this.textBox2.Text;
            string strReplace = this.textBox3.Text;

            string End_date = DateTime.Now.ToShortDateString();
            End_date = End_date.Replace("/", "-");
          

            string str = "";
     
            System.IO.DirectoryInfo dr = new System.IO.DirectoryInfo(strTxtFileFolderPath);
            System.IO.FileInfo[] files = dr.GetFiles("*.txt");
            System.IO.DirectoryInfo[] dfolder = dr.GetDirectories();

            //string destFolder = ExportFilePath + "\\history\\" + EndDateforPMS.Substring(0, 6) + "\\";
            string destFolderFile = "";
            string FolderName = "";
            int RowAffected = 0;

            foreach (System.IO.DirectoryInfo subFolder in dfolder)
            {
                FolderName = subFolder.Name;
                System.IO.FileInfo[] subfiles = subFolder.GetFiles("*.txt");

                foreach (System.IO.FileInfo sfie in subfiles)
                {
                    str = sfie.Name;

                    int Row = 0;

                    destFolderFile = strTxtFileFolderPath + "\\" + FolderName + "\\" + str;
                    string bakdestFolderFile = strTxtFileFolderPath + "\\" + FolderName + "\\bak-" + str;

                    // find and replace log
                    Log.Write(" - start checking file: " + destFolderFile, End_date + "_replace");

                    StreamWriter writer = null;
                    System.Collections.Generic.Dictionary<string, string> replacements = new System.Collections.Generic.Dictionary<string, string>();
                    replacements.Add(strFind, strReplace);
                    // ... further replacement entries ...

                    using (writer = File.CreateText(bakdestFolderFile))
                    {
                        foreach (string strline in File.ReadLines(destFolderFile))
                        {
                            bool replacementMade = false;
                            foreach (var replacement in replacements)
                            {
                                if (strline.Contains(replacement.Key))
                                {
                                    // find and replace log
                                    Log.Write(" B - " + strline, End_date + "_replace");
                                    Log.Write(" A - " + strline.Replace(strFind, strReplace), End_date + "_replace");

                                    writer.WriteLine(strline.Replace(strFind, strReplace));
                                    replacementMade = true;
                                    RowAffected = RowAffected + 1;
                                    Row = Row + 1;
                                    break;
                                }
                            }
                            if (!replacementMade)
                            {
                                writer.WriteLine(strline);
                            }
                        }
                    }

                    if (Row > 0)
                    {
                        string backupFileName = destFolderFile.Replace(".txt", ".bak");
                        File.Replace(bakdestFolderFile, destFolderFile, backupFileName);
                    }
                    else
                    {
                        File.Delete(bakdestFolderFile);
                    }
                }
            }




            foreach (System.IO.FileInfo fie in files)
            {
                str = fie.Name;


                int Row = 0;

                destFolderFile = strTxtFileFolderPath + "\\" + str;
                string bakdestFolderFile = strTxtFileFolderPath + "\\bak-" + str;

                // find and replace log
                Log.Write(" - start checking file: " + destFolderFile, End_date + "_replace");


                StreamWriter writer = null;
                System.Collections.Generic.Dictionary<string, string> replacements = new System.Collections.Generic.Dictionary<string, string>();
                replacements.Add(strFind, strReplace);
                // ... further replacement entries ...

                using (writer = File.CreateText(bakdestFolderFile))
                {
                    foreach (string strline in File.ReadLines(destFolderFile))
                    {
                        bool replacementMade = false;
                        foreach (var replacement in replacements)
                        {
                            if (strline.Contains(replacement.Key))
                            {
                                // find and replace log
                                Log.Write(" B - " + strline, End_date + "_replace");
                                Log.Write(" A - " + strline.Replace(strFind, strReplace), End_date + "_replace");

                                writer.WriteLine(strline.Replace(strFind, strReplace));
                                replacementMade = true;
                                RowAffected = RowAffected + 1;
                                Row = +1;
                                break;
                            }
                        }
                        if (!replacementMade)
                        {
                            writer.WriteLine(strline);
                        }
                    }
                }

                if (Row > 0)
                {
                    string backupFileName = destFolderFile.Replace(".txt", ".bak");
                    File.Replace(bakdestFolderFile, destFolderFile, backupFileName);
                }
                else
                {
                    File.Delete(bakdestFolderFile);
                }
            }
         



           // MessageBox.Show("Number of row " + RowAffected + " affected.");
            MessageBox.Show("Total " + RowAffected + " number of row(s) updated.", "Folder Find and Replace", MessageBoxButtons.OK, MessageBoxIcon.Information);

         
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            decimal Cost_RM = 0.00M;

            Cost_RM = Get_Redumption_NEW_Cost_UTMC10("12019", "04", "3104753");

            MessageBox.Show(System.Convert.ToString(Cost_RM));

        }

        private void button14_Click(object sender, EventArgs e)
        {
            Process_UTMC11_ZeroUnit();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            decimal Cost_RM = 0.00M;

            Cost_RM = Get_Redumption_Cost_UTMC10("12019", "04", "3104753");

            MessageBox.Show(System.Convert.ToString(Cost_RM));

        }

        

    }
}


